package com.acats.core.xray;

import java.io.Serializable;



import com.google.gson.annotations.SerializedName;


public class TransitionReqType implements Serializable {
	private static final long serialVersionUID = 4L; 
	@SerializedName("id")
	String id;

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

}
