package com.acats.core.xray;

import com.acats.core.ReadProperty;

public interface JiraConstants {
	public static String issueStatusBacklog="11";
	public static String passedExecution="PASSED";
	public static String failedExecution="FAILED";
	public static String screenshotPath=ReadProperty.get_propValue("screenShotspath");
	public static String extentReportPath=ReadProperty.get_propValue("extentReportPath");
}
