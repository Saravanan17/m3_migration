package com.acats.core.xray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.acats.core.BaseUtil;
import com.acats.core.FileReaderManager;
import com.acats.core.ReadProperty;
import com.google.gson.Gson;


public class JiraAPIImpl implements JiraConstants,JiraXrayAPIConstants  {

	final static Logger logger = Logger.getLogger(JiraAPIImpl.class);
	/**
	 * To Update Issue Status in JIRA
	 * @throws Throwable 
	 */

	public String updateIssueStatusJira(String issueStatusId,String issueKey) throws Throwable {
		StringBuffer result = new StringBuffer();
		try {
			HttpClient client = HttpClientBuilder.create().build();
			String url = jiraUpdateIssueStatusApi;
			url= url.replace("Key", issueKey);
			HttpPost post = new HttpPost(url);
			post.addHeader("Accept", "application/json");
			post.addHeader("Content-type", "application/json");
			String authStringEnc  =ReadProperty.get_propValue("jiraEncodedId");
			post.addHeader("Authorization", "Basic " + authStringEnc);
			IssueStatusReqType issueStatusReqType= new IssueStatusReqType();
			TransitionReqType transitionReqType = new TransitionReqType();
			transitionReqType.setId(issueStatusId);
			issueStatusReqType.setTransition(transitionReqType);
			Gson gson = new Gson();
			String requestBody = gson.toJson(issueStatusReqType);
			StringEntity params = new StringEntity(requestBody);
			post.setEntity(params);
			HttpResponse response = client.execute(post);
			logger.info("JIRA UPDATE RESPONSE CODE"+response.getStatusLine());
		}catch(Exception e) {
			logger.error(e.getMessage());    
		}
		return "********************Jira Status Updated*************";

	}
	
	
	
	/**
	 * To retrieve Test ID
	 * @throws Throwable
	 */
	public static void retrieveIssueStatusId() throws Throwable {
		try {
			Map<String, String> jiraIssueStatusMap = new HashMap<>();
			String api =jiraUpdateIssueStatusApi;
			api= api.replace("Key",getIssueId());
			URL url = new URL(api);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			String jiraEncodedId  =ReadProperty.get_propValue("jiraEncodedId");
			conn.setRequestProperty  ("Authorization", "Basic " + jiraEncodedId);
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(output);
				JSONArray  issueList = (JSONArray ) json.get("transitions"); 
				for (int i=0;i<issueList.size();i++) {
					JSONObject  list = (JSONObject) issueList.get(i);
					String issueId = (String) list.get("id");
					String issueStatus = (String) list.get("name");
					jiraIssueStatusMap.put(issueStatus,issueId);
				}
				BaseUtil.setJiraIssueStatusMap(jiraIssueStatusMap);
			}
		}catch(Exception e) {
			logger.error(e.getMessage());    
		}
	}
	
	
	public static String getIssueId() throws Throwable {
		String  key = null;
		String issueReqJsonPath = ReadProperty.get_propValue("issueReqJsonPath").trim();
		Object obj = new JSONParser().parse(new FileReader(issueReqJsonPath)); 
		JSONObject issueObject = (JSONObject) obj; 
		JSONArray  issueList = (JSONArray ) issueObject.get("Issue"); 
		for(int i=0;i<issueList.size();i++) {
			JSONObject issueKey = (JSONObject) issueList.get(i);
			JSONArray  keyList = (JSONArray ) issueKey.get("testSet");
			for(int j=0;j<keyList.size();j++) {
				 key = (String ) keyList.get(j);
				break;
			}
			break;
		}
		return key;
	}

	
	
	

}