package com.acats.core.xray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

import com.acats.core.FileReaderManager;
import com.acats.core.ReadProperty;

public class JiraXrayUpdateBitbucket implements JiraConstants,JiraXrayAPIConstants {

	private static final String INPUT_FOLDER = "src/test/resources/kilo/Sprint1";
	final static Logger logger = Logger.getLogger(JiraXrayUpdateBitbucket.class);
	/**
	 * Will update into X-ray only if the switch is turned on in Config file
	 * xrayBitBucket=true
	 * @throws Throwable
	 */
	public static void updateXray() throws Throwable {
		try {
			String ZIPPED_FOLDER = ReadProperty.get_propValue("importXrayPath").trim();
			if(ReadProperty.get_propValue("xrayUpadate")
					.equalsIgnoreCase("true")) {
				zip( INPUT_FOLDER, ZIPPED_FOLDER);
				XrayApiImpl.importBddXray();;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void zip(String inputFolder,String targetZippedFolder)  throws IOException {

		FileOutputStream fileOutputStream = null;

		fileOutputStream = new FileOutputStream(targetZippedFolder);
		ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

		File inputFile = new File(inputFolder);

		if (inputFile.isFile())
			zipFile(inputFile,"",zipOutputStream);
		else if (inputFile.isDirectory())
			zipFolder(zipOutputStream,inputFile,"");

		zipOutputStream.close();
	}

	public static void zipFolder(ZipOutputStream zipOutputStream,File inputFolder, String parentName)  throws IOException {
		String myname = parentName +inputFolder.getName()+"\\";
		ZipEntry folderZipEntry = new ZipEntry(myname);
		zipOutputStream.putNextEntry(folderZipEntry);

		File[] contents = inputFolder.listFiles();

		for (File f : contents){
			if (f.isFile())
				zipFile(f,myname,zipOutputStream);
			else if(f.isDirectory())
				zipFolder(zipOutputStream,f, myname);
		}
		zipOutputStream.closeEntry();
	}

	public static void zipFile(File inputFile,String parentName,ZipOutputStream zipOutputStream){
		try {
			String ZIPPED_FOLDER = ReadProperty.get_propValue("importXrayPath").trim();
			ZipEntry zipEntry = new ZipEntry(parentName+inputFile.getName());
			zipOutputStream.putNextEntry(zipEntry);
			FileInputStream fileInputStream = new FileInputStream(inputFile);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = fileInputStream.read(buf)) > 0) {
				zipOutputStream.write(buf, 0, bytesRead);
			}
			zipOutputStream.closeEntry();
			logger.info("Regular file :" + parentName+inputFile.getName() +" is zipped to archive :"+ZIPPED_FOLDER);
		}catch(Throwable e) {

		}
	}
}


