package com.acats.core.xray;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TestsReqType implements Serializable {
	
	private static final long serialVersionUID = 4L; 

	@SerializedName("testKey")
	private String testKey;
	
	@SerializedName("status")
	private String status;
	
	@SerializedName("evidences")
	private List<EvidencesReqType> evidencesReqType;
	
	@SerializedName("comment")
	private String comment;
	
	
	public List<EvidencesReqType> getEvidencesReqType() {
		return evidencesReqType;
	}
	public void setEvidencesReqType(List<EvidencesReqType> evidencesReqType) {
		this.evidencesReqType = evidencesReqType;
	}
	public String getTestKey() {
		return testKey;
	}
	public void setTestKey(String testKey) {
		this.testKey = testKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	
}
