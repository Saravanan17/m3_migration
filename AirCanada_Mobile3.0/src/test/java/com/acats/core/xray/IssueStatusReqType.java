package com.acats.core.xray;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class IssueStatusReqType implements Serializable {

	private static final long serialVersionUID = 4L; 
	@SerializedName("transition")
	public TransitionReqType Transition;

	public TransitionReqType getTransition() {
		return Transition;
	}

	
	public void setTransition(TransitionReqType transition) {
		Transition = transition;
	}
}
