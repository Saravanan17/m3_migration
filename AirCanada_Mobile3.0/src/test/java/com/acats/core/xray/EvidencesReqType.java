package com.acats.core.xray;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class EvidencesReqType implements Serializable {
	
	private static final long serialVersionUID = 4L; 
	
	
	@SerializedName("data")
	private String data;
	
	@SerializedName("filename")
	private String filename;
	
	@SerializedName("contentType")
	private String contentType;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
