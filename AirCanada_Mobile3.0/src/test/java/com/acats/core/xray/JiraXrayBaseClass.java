package com.acats.core.xray;

import org.apache.log4j.Logger;

import com.acats.core.FileReaderManager;
import com.acats.core.ReadProperty;

public class JiraXrayBaseClass {
	final static Logger logger = Logger.getLogger(FileReaderManager.class);
	/**
	 * Establish JIRA Connectivity, Download BDD from JIRA and Update Back to JIRA 
	 * @throws Throwable 
	 */
		public void connectToJira()  
		{
			try {
				XrayApiImpl xrayApiImplementation = new XrayApiImpl();
				if(!ReadProperty.get_propValue("xrayUpadate")
	    				.equalsIgnoreCase("true")) {
				xrayApiImplementation.getBDDFromXrayJira();
				}
				JiraAPIImpl.retrieveIssueStatusId();
			}catch(Throwable e) {
				logger.info(e.getMessage());
			}
		}	
}
