package com.acats.core.xray;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class InfoReqType {

	@SerializedName("testEnvironments")
	private List<String> testEnvironments;

	public List<String> getTestEnvironments() {
		return testEnvironments;
	}

	public void setTestEnvironments(List<String> testEnvironments) {
		this.testEnvironments = testEnvironments;
	}

	
	
	
}
