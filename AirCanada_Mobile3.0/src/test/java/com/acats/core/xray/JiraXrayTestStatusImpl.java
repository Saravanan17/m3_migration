package com.acats.core.xray;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.ITestResult;

import com.acats.core.BaseUtil;

import cucumber.api.Scenario;
import cucumber.api.testng.PickleEventWrapper;
import gherkin.pickles.PickleTag;

public class JiraXrayTestStatusImpl implements JiraConstants {
	JiraAPIImpl jiraAPIImpl;
	String keys[];
	XrayApiImpl xrayApiImpl;
	final static Logger logger = Logger.getLogger(JiraXrayTestStatusImpl.class);
	/**
	 * Update JIRA test status
	 * @param result 
	 * @throws Throwable 
	 */

	public void updateJiraStatus(ITestResult result,String failedScenarioScreenshot) throws Throwable {
		jiraAPIImpl = new JiraAPIImpl();
		xrayApiImpl= new XrayApiImpl();
		String issueKey = null;
		
		Object[] featureTestName =result.getParameters();
		for (Object inputArg : featureTestName) {
			if(inputArg instanceof PickleEventWrapper) {
				PickleEventWrapper event = (PickleEventWrapper) inputArg;
				List<PickleTag> tags = event.getPickleEvent().pickle.getTags();
				for (PickleTag tag : tags) {
					if(tag.getName().contains("TEST_")) {
						String[] key = tag.getName().split("_");
						issueKey= key[1];
						break;
					}
				}
			}
		}
		try {
			if(issueKey!=null) {
				if(result.getStatus() == ITestResult.FAILURE) {
					jiraAPIImpl.updateIssueStatusJira(issueStatusBacklog,issueKey);
					xrayApiImpl.updateXrayExecutionResult(issueKey,failedScenarioScreenshot ,failedExecution,result.getThrowable().toString());

				}else {
					String id =renderStatusId();
					jiraAPIImpl.updateIssueStatusJira(id,issueKey);
					xrayApiImpl.updateXrayExecutionResult(issueKey,null,passedExecution,null);
				}

			}
		}catch(Exception e) {
			logger.info(e.getMessage());
		}
	}
	private String renderStatusId() {
		String statusId;
		Map<String, String> issueStatusMap = BaseUtil.getJiraIssueStatusMap();
			return statusId = issueStatusMap.get("Done");
		}
	}



