package com.acats.core.xray;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class IssueExecutionReqType implements Serializable{
	
	private static final long serialVersionUID = 4L; 
	
	@SerializedName("testExecutionKey")
	private String testExecutionKey;
	
	@SerializedName("tests")
	public List<TestsReqType> testsReqType;

	
	@SerializedName("info")
	public InfoReqType infoReqType;
	
	public List<TestsReqType> getTestsReqType() {
		return testsReqType;
	}

	public void setTestsReqType(List<TestsReqType> testsReqType) {
		this.testsReqType = testsReqType;
	}

	public String getTestExecutionKey() {
		return testExecutionKey;
	}

	public void setTestExecutionKey(String testExecutionKey) {
		this.testExecutionKey = testExecutionKey;
	}

	public InfoReqType getInfoReqType() {
		return infoReqType;
	}

	public void setInfoReqType(InfoReqType infoReqType) {
		this.infoReqType = infoReqType;
	}
	
	

	

}
