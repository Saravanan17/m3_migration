package com.acats.core.xray;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.acats.core.BaseUtil;
import com.acats.core.NGTestListener;
import com.acats.core.ReadProperty;
import com.google.gson.Gson;


public class XrayApiImpl implements JiraConstants,JiraXrayAPIConstants {
	final static Logger logger = Logger.getLogger(XrayApiImpl.class);
	/**
	 * Genearte Authentication Token for X-Ray
	 */

	public static String retrieveXrayAuthToken() {
		StringBuffer result = new StringBuffer();
		try {
			String clientId  =ReadProperty.get_propValue("clientId");
			String clientKey  =ReadProperty.get_propValue("clientKey");
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(xrayAuthUrl);
			post.setHeader("Accept", "application/json");
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("client_id", clientId));
			urlParameters.add(new BasicNameValuePair("client_secret", clientKey ));
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = client.execute(post);
			logger.info("JIRA Retrieve Token Status Code : " +response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		}catch(Throwable e) {
			logger.error(e.getMessage());
		}
		return result.toString();
	}


	/**
	 * Read request from JSON file
	 * @throws Throwable 
	 */

	public void getBDDFromXrayJira() throws Throwable {
		String issueReqJsonPath = ReadProperty.get_propValue("issueReqJsonPath").trim();
		Object obj = new JSONParser().parse(new FileReader(issueReqJsonPath)); 
		JSONObject issueObject = (JSONObject) obj; 
		JSONArray  issueList = (JSONArray ) issueObject.get("Issue"); 
		int count=0;
		for(int i=0;i<issueList.size();i++) {
			JSONObject issueKey = (JSONObject) issueList.get(i);
			JSONArray  keyList = (JSONArray ) issueKey.get("testSet");
			for(int j=0;j<keyList.size();j++) {
				String  key = (String ) keyList.get(j);
				String zipFileName = "XrayJiraFeatures" + count + ".zip";
				String featureFileName = key+ ".feature";
				getBDD(key,zipFileName,featureFileName);
			}
			count++;
		}
	}

	/**
	 * To Pull BDD From XrayJira and Export it to file 
	 */

	public String getBDD(String key,String zipFileName,String featureFileName) {
		String output = null;
		try {
			String featureZipFolderLocation = ReadProperty.get_propValue("exportXrayPath").trim();
			String accessToken= retrieveXrayAuthToken();
			accessToken = accessToken.replace("\"", " ").trim();
			URL url = new URL(xrayExportUrl+key);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Content-Type", "application/zip");
			conn.setRequestProperty("Authorization","Bearer "+accessToken);
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : "
						+ conn.getResponseCode());
			}
			InputStream inn = conn.getInputStream();
			Path paths=Paths.get(featureZipFolderLocation);
			File outputDir = new File(paths.toUri());
			if (!outputDir.exists()) {
				outputDir.mkdir();
			}
			FileOutputStream out = new FileOutputStream(featureZipFolderLocation+zipFileName);
			downloadZip(inn, out, 1024);
			unzipFileIntoDirectory(key,zipFileName,featureFileName);
			out.close();
			conn.disconnect();	
		}catch(Throwable e) {
			logger.error(e.getMessage());
		}
		return output;

	}

	public void unzipFileIntoDirectory(String issuekey,String zipFileName, String featureFileName) 
			throws Exception {
		try {
			String featureFilePath = ReadProperty.get_propValue("featureFilePath").trim();
			String featureZipFolderLocation = ReadProperty.get_propValue("exportXrayPath").trim();
			File archive= new File(featureZipFolderLocation+zipFileName);
			String path;
			final int BUFFER_SIZE = 1024;
			BufferedOutputStream dest = null;
			FileInputStream fis = new FileInputStream(archive);
			ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
			ZipEntry entry;
			File destFile;
			while ((entry = zis.getNextEntry()) != null) {
				int fileCount=0;
				String fileName = "xray-Gherkin" + fileCount + ".feature";
				path = featureFilePath+"/"+renderFolderName(issuekey)+"/";
				destFile = new File(path+featureFileName);
				if (entry.isDirectory()) {
					destFile.mkdirs();
					continue;
				} else {
					int count;
					byte data[] = new byte[BUFFER_SIZE];
					destFile.getParentFile().mkdirs();
					FileOutputStream fos = new FileOutputStream(destFile);
					dest = new BufferedOutputStream(fos, BUFFER_SIZE);
					while ((count = zis.read(data, 0, BUFFER_SIZE)) != -1) {
						dest.write(data, 0, count);
					}
					dest.flush();
					dest.close();
					fos.close();
					readFileInList(destFile.getPath());
				}
			}
			zis.close();
			fis.close();
		}catch(Throwable e) {
			logger.error(e.getMessage());
		}
	}




	public static void downloadZip(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buf = new byte[bufferSize];
		int n = input.read(buf);
		while (n >= 0) {
			output.write(buf, 0, n);
			n = input.read(buf);
		}
		output.flush();
	}

	/**
	 * Convert Image to base64
	 * @throws IOException 
	 */

	public String renderImageBase64(String filePath) throws IOException {
		byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		return encodedString;
	}


	/**
	 * 
	 * Upload Failed screenshort in JIRA
	 * @param error 
	 * @throws Throwable 
	 */

	public void updateXrayExecutionResult(String issueKey,String filePath,String testStatus, String error) throws Throwable {
		try {
			String accessToken= retrieveXrayAuthToken();
			accessToken = accessToken.replace("\"", " ").trim();
			HttpClient client = new DefaultHttpClient();
			String url = xrayExecutionUrl;
			HttpPost post = new HttpPost(url);
			post.addHeader("Accept", "application/json");
			post.addHeader("Authorization","Bearer "+accessToken);
			post.addHeader("Content-Type", "application/json");
			String executionKey = retrieveExecutionKey();
			IssueExecutionReqType issueExecutionReqType= new IssueExecutionReqType();
			List<TestsReqType> testsReqTypeList = new ArrayList<>();
			TestsReqType testsReqType= new TestsReqType();
			InfoReqType infoReqType = new InfoReqType();
			testsReqType.setTestKey(issueKey);
			testsReqType.setStatus(testStatus);
			if(NGTestListener.issueKeyMap.get(issueKey)==null||!NGTestListener.issueKeyMap.get(issueKey).equalsIgnoreCase(failedExecution)) {
				if(filePath!=null) {
					String encodedImgString=renderImageBase64(filePath);
					List<EvidencesReqType> evidencesReqTypeList = new ArrayList<>();
					EvidencesReqType evidencesReqType= new EvidencesReqType();
					evidencesReqType.setFilename(filePath);
					evidencesReqType.setContentType("image/jpg");
					evidencesReqType.setData(encodedImgString);
					evidencesReqTypeList.add(evidencesReqType);
					testsReqType.setEvidencesReqType(evidencesReqTypeList);

				}
				testsReqType.setComment(buildErrorResponseDetails(testStatus,error));
				testsReqTypeList.add(testsReqType);
				issueExecutionReqType.setTestsReqType(testsReqTypeList);
				issueExecutionReqType.setTestExecutionKey(executionKey);
				Gson gson = new Gson();
				String requestBody = gson.toJson(issueExecutionReqType);
				StringEntity params = new StringEntity(requestBody);
				post.setEntity(params);
				HttpResponse response = client.execute(post);
				logger.info("JIRA Update Execution Result Status Code"+response.getStatusLine());
			}
			NGTestListener.issueKeyMap.put(issueKey,testStatus);
		}catch(Exception e) {
			logger.error(e.getMessage());   
		}

	}

	/**
	 * To Read Data from Excel
	 * @param fileName
	 * @throws IOException
	 */
	public static void readFileInList(String fileName) throws IOException 
	{ 
		Path path = Paths.get(fileName);
		Charset charset = StandardCharsets.UTF_8;
		String content = new String(Files.readAllBytes(path), charset);
		content = content.replaceAll("#@data", "@data");
		Files.write(path, content.getBytes(charset));
	}

	/**
	 * To retrieve Execution Key
	 * @param issue
	 * @return
	 * @throws Throwable 
	 */
	public String retrieveExecutionKey() throws Throwable {
		String executionKey = null;
		String issueReqJsonPath = ReadProperty.get_propValue("issueReqJsonPath").trim();
		String browserProp = ReadProperty.get_propValue("TestExecution");
		Object obj = new JSONParser().parse(new FileReader(issueReqJsonPath)); 
		JSONObject issueObject = (JSONObject) obj; 
		JSONArray  issueList = (JSONArray) issueObject.get("Execution"); 
		for(int i=0;i<issueList.size();i++) {
			JSONObject object = (JSONObject) issueList.get(i);
			String  browser = (String ) object.get("executionName");
			if(browserProp.equalsIgnoreCase(browser)) {
				executionKey = (String ) object.get("execution");	
			}
		}
		return executionKey;
	}


	public static void importBddXray() {
		try {
			String xrayFilePath = ReadProperty.get_propValue("importXrayPath").trim();
			HttpClient client = new DefaultHttpClient();
			String url =xrayImportBddApi;
			url =url+retrieveProjectkey();
			String accessToken= retrieveXrayAuthToken();
			accessToken = accessToken.replace("\"", " ").trim();
			HttpPost post = new HttpPost(url);
			post.addHeader("Authorization","Bearer "+accessToken);
			
			FileBody fileName = new FileBody(new File(xrayFilePath));
			MultipartEntity parms = new MultipartEntity(); 
			parms.addPart("file",fileName);
			post.addHeader("Content-Type", parms.getContentType().getValue()); 
			post.setEntity(parms);
			HttpResponse response = client.execute(post);
			logger.info("JIRA Import BDD Status Code"+response.getStatusLine());
		}catch(Throwable e) {
			logger.error(e.getMessage());
		}
	}


	public static String retrieveProjectkey() throws Throwable {
		String projectKey =null;
		String issueReqJsonPath = ReadProperty.get_propValue("issueReqJsonPath").trim();
		Object obj = new JSONParser().parse(new FileReader(issueReqJsonPath)); 
		JSONObject issueObject = (JSONObject) obj; 
		projectKey = (String) issueObject.get("projectKey");
		return projectKey; 
	}


	private String renderFolderName(String issueKey) throws Throwable {
		String folderName = null;
		String issueReqJsonPath = ReadProperty.get_propValue("issueReqJsonPath").trim();
		Object obj = new JSONParser().parse(new FileReader(issueReqJsonPath)); 
		JSONObject issueObject = (JSONObject) obj; 
		JSONArray  issueList = (JSONArray ) issueObject.get("Issue");
		for(int i=0;i<issueList.size();i++) {
			JSONObject object = (JSONObject) issueList.get(i);
			JSONArray  testList = (JSONArray) object.get("testSet");
			for(int j=0;j<testList.size();j++) {
				String  key = (String ) testList.get(j);
				key=key.replace("\"", " ").trim();
				if(key.equalsIgnoreCase(issueKey)){
					folderName = (String ) object.get("folderName");
				}
			}
		}
		return folderName;
	}

	public String buildErrorResponseDetails(String testStatus, String error) {
		String newLine = System.getProperty("line.separator");
		StringBuilder sb;  
		if(testStatus.equalsIgnoreCase(failedExecution)) {
			sb=new StringBuilder("Failed Execution "+newLine+newLine);  
			sb.append("Step-Failed--" +NGTestListener.failedStepMap.get((int)Thread.currentThread().getId())+newLine);
			sb.append("Details--"+error+newLine+newLine);
		}else {
			sb=new StringBuilder("Execution Passed"+newLine+newLine);  	
		}
		sb.append("SYSTEM DETAILS--");
		sb.append(renderSystemDetails());
		return sb.toString();
	}

	public String renderSystemDetails() {
		InetAddress ip = null;
		try {
			ip = InetAddress.getLocalHost();
		}catch(UnknownHostException e) {
			logger.error(e.getMessage());
		}
		return ip.toString();
	}
}

