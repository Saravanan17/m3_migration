package com.acats.core.xray;

public interface JiraXrayAPIConstants {

	
   //	public static String jiraURL = "https://abi13.atlassian.net/";
	
	public static String jiraURL = "https://aircanada.atlassian.net/";

	//X-Ray API 
	public static String xrayAuthUrl="https://xray.cloud.xpand-it.com/api/v1/authenticate";
	public static String xrayExportUrl="https://xray.cloud.xpand-it.com/api/v1/export/cucumber?keys=";
	public static String xrayExecutionUrl="https://xray.cloud.xpand-it.com/api/v1/import/execution";
	public static String jiraUpdateIssueStatusApi=jiraURL+"rest/api/2/issue/Key/transitions?expand=transitions.fields";
	public static String xrayImportBddApi="https://xray.cloud.xpand-it.com/api/v1/import/feature?projectKey=";
	
	
	//Test Jira API 
	public static String jiraRetrieveIssueApi=jiraURL+"rest/api/3/issue/KQA-477";
	public static String jiraUpdateIssueDescriptionApi=jiraURL+"rest/api/3/issue/AF-4";
	
}