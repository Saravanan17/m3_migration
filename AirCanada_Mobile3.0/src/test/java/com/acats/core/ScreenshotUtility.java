package com.acats.core;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.acats.core.xray.JiraConstants;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

/**
 * To create Screenshot directory at RunTime and to capture screenshot based on
 * Switch at Property File.
 * 
 * @author HX029704
 *
 */
public class ScreenshotUtility implements JiraConstants {

	public static String featurePath;
	public static String scenarioPath;
	private static String encoded;
	final static Logger logger = Logger.getLogger(ScreenshotUtility.class);

	/**
	 * To Create a Execution Folder for all Tests
	 * 
	 * @param count
	 * @return
	 */

	public static String screenshotPathByFeature() {
		final String screenshotDirPath = ReadProperty.get_propValue("screenShotspath") + "screenshot_"
				+ timestampinSeconds();
		Path path = Paths.get(screenshotDirPath);
		if (!Files.exists(path)) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				// fail to create directory
				e.printStackTrace();
			}
		}
		return screenshotDirPath;
	}

	/**
	 * To create Folder for Each Feature
	 * 
	 * @param featureName
	 * @param featureFilePath
	 * @return
	 */
	public static String featureScreenshotFolder(String featureName, String featureFilePath) {
		if (featureName.length() > 50) {
			featureName = "FE-" + featureName.substring(0, 50).replace("\"", " ").trim();
		} else {
			featureName = "FE-" + featureName.replace("\"", " ").trim();
		}
		featurePath = featureFilePath + "/" + featureName;
		File featureFolder = new File(featurePath);
		if (!featureFolder.exists()) {
			featureFolder.mkdir();
		}
		return featurePath;
	}

	/**
	 * To create folder for each Scenario
	 * 
	 * @param featureName
	 * @param featureFilePath
	 * @return
	 */

	public static String scenarioScreenshotFolder(String scenarioName) {
		boolean isDriverOpened = BaseUtil.isDriverOpened();
		if (isDriverOpened) {
			if (scenarioName.length() > 50) {
				scenarioName = "SC-" + scenarioName.substring(0, 50).replace("\"", " ").trim();
			} else {
				scenarioName = "SC-" + scenarioName.replace("\"", " ").trim();
			}
			scenarioPath = featurePath + "/" + scenarioName;
			File featureFolder = new File(scenarioPath);
			if (!featureFolder.exists()) {
				featureFolder.mkdir();
			}
		}
		return featurePath;
	}

	/**
	 * Capturing Screenshots for all Steps in Feature
	 * 
	 * @param featureName
	 * @throws Throwable
	 */
	public static String screenshotForWeb(String stepName) throws Throwable {
		String destPath = null;
		int key = (int) Thread.currentThread().getId();
		try {
			boolean isDriverOpened = BaseUtil.isDriverOpened();
			if (isDriverOpened) {
				stepName = stepName.replaceAll("[^a-zA-Z]+", "_");
				if (stepName.length() > 50) {
					destPath = scenarioPath + "/" + "Step-" + stepName.substring(0, 50) + "_" + String.valueOf(key);
				} else {
					destPath = scenarioPath + "/" + "Step-" + stepName + "_" + String.valueOf(key);
				}
				File scr =null;
				if(ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser")) {
				scr = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
				}else {
				scr = ((TakesScreenshot) DriverManager.getMDriver()).getScreenshotAs(OutputType.FILE);
				}
				FileInputStream fileInputStreamReader = new FileInputStream(scr);
				byte[] bytes = new byte[(int) scr.length()];
				fileInputStreamReader.read(bytes);
				encoded = new String(Base64.encodeBase64(bytes));
				String fileFormat = ReadProperty.get_propValue("screenshotFileFormat");
				Files.copy(scr.toPath(), new File(destPath + "." + fileFormat).toPath());
				destPath = destPath + "." + fileFormat;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return destPath;
	}

	/**
	 * To capture Failed Screenshot based on Switch - CaptureFailedScreenshot
	 * 
	 * @throws Throwable
	 */
	public static String captureFailedScreenshot(String scenarioName) throws Throwable {
		String destPath = null;
		try {
			scenarioScreenshotFolder(scenarioName);
			destPath = screenshotForWeb("FailureStep");
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return destPath;
	}

	/**
	 * Current Time Stamp
	 * 
	 * @return
	 */

	public static String timestampinSeconds() {
		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss-SSS").format(new Date());
	}

	/**
	 * To Handle Extent Report Baased on Switch
	 * 
	 * @param screenshotSteps
	 * @param scenarioDef
	 * @param stepName
	 * @throws IOException
	 */
	public static void handleExtentReport(String screenshotSteps, ExtentTest scenarioDef, String stepname)
			throws IOException {
		stepname = stepname + "<br>";
		boolean isDriverOpened = BaseUtil.isDriverOpened();
		logger.info("-----Executing Step----- >" + stepname);
		switch (screenshotSteps) {
		case "allSteps":
			if (isDriverOpened) {
				scenarioDef.log(Status.INFO, stepname,
						MediaEntityBuilder.createScreenCaptureFromBase64String(encoded.toString()).build());
			} else {
				scenarioDef.log(Status.INFO, stepname);
			}
			break;
		case "failureOnly":
			if (isDriverOpened) {
				scenarioDef.log(Status.ERROR, stepname,
						MediaEntityBuilder.createScreenCaptureFromBase64String(encoded.toString()).build());
			} else {
				scenarioDef.log(Status.ERROR, stepname);
			}
			break;
		case "none":
			scenarioDef.log(Status.INFO, stepname);
			break;
		default:
			break;
		}
	}

	/**
	 * To Delete Folder/Sub folder based on Property Value -
	 * screenshotReportHistoryCount
	 * 
	 * @param path
	 * @param fileName
	 */

	public static void deleteDirectory(String path, String fileName) {
		try {
			File dir = new File(path);
			int numberOfSubfolders = 0;
			File listDir[] = dir.listFiles();
			for (int i = 0; i < listDir.length; i++) {
				if (listDir[i].isDirectory() && listDir[i].getName().contains(fileName)
						|| (listDir[i].isFile() && listDir[i].getName().contains(fileName))) {
					numberOfSubfolders++;
				}
			}
			String historyCount = ReadProperty.get_propValue("screenshotReportHistoryCount");
			int count = Integer.parseInt(historyCount);
			if (numberOfSubfolders >= count) {
				if (path.equalsIgnoreCase(screenshotPath)) {
					for (int i = 0; i < listDir.length; i++) {
						delFolder(listDir[i]);
					}

				} else {
					Arrays.stream(new File(path).listFiles()).forEach(File::delete);
				}
			}
		} catch (Throwable e) {
			logger.error(e.getMessage());
		}
	}

	public static void delFolder(final File folder) {
		if (folder.isDirectory()) {
			File[] list = folder.listFiles();
			if (list != null) {
				for (int i = 0; i < list.length; i++) {
					File tmpF = list[i];
					if (tmpF.isDirectory()) {
						delFolder(tmpF);
					}
					tmpF.delete();
				}
			}
			if (!folder.delete()) {
				logger.error("Unable to Delete Folder");
			}
		}
	}

	public static void delFilesinDir(File dir) {
		for (File file : dir.listFiles())
			if (!file.isDirectory())
				file.delete();
	}

	public static void resizeImageIcon(File scr, Integer width, Integer height) throws Throwable {
		ImageIcon imageIcon = new ImageIcon("");
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
		Graphics2D graphics2D = bufferedImage.createGraphics();
		graphics2D.drawImage(imageIcon.getImage(), 0, 0, width, height, null);
		graphics2D.dispose();
		String fileFormat = ReadProperty.get_propValue("screenshotFileFormat");
		ImageIO.write(bufferedImage, fileFormat, scr);
	}

}
