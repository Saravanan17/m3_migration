package com.acats.core;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import static java.time.Duration.ofMillis;

public class Lib_MobileGeneric {
	// AppiumDriver<MobileElement> driver;
	public static AppiumDriver driver = null;
	private static String homeWindow = null;

	/*
	 * public Lib_MobileGeneric(AppiumDriver<MobileElement> driver) { this.driver =
	 * driver; PageFactory.initElements(new AppiumFieldDecorator(this.driver),
	 * this); }
	 */

	public String MonthDate = "";
	static Logger logger = Logger.getLogger(Lib_MobileGeneric.class);

	public static void accept_Alert() {
		try {
			Alert alert = DriverManager.getDriver().switchTo().alert();
			alert.accept();
		} catch (Exception e) {
		}
	}

	/**
	 * Authenticating the alert
	 * 
	 * @param actions
	 * @param username
	 * @param password
	 * @return
	 */

	public String screenCapture(String imgLocation) {
		File scrFile = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(imgLocation));
		} catch (IOException e) {
		}
		return imgLocation;
	}

	/**
	 * Method to switch to the newly opened window
	 */
	public static void switchToWindow() {
		homeWindow = DriverManager.getDriver().getWindowHandle();
		for (String window : DriverManager.getDriver().getWindowHandles()) {
			DriverManager.getDriver().switchTo().window(window);
		}
	}

	/**
	 * To navigate to the main window from child window
	 */
	public static void switchToMainWindow() {
		for (String window : DriverManager.getDriver().getWindowHandles()) {
			if (!window.equals(homeWindow)) {
				DriverManager.getDriver().switchTo().window(window);
				DriverManager.getDriver().close();
			}
			DriverManager.getDriver().switchTo().window(homeWindow);
		}
	}

	/**
	 * This method returns the no.of windows present
	 * 
	 * @return
	 */
	public static int getWindowCount() {
		return DriverManager.getDriver().getWindowHandles().size();
	}

	/****************** frames *********************/

	public static void frames(MobileElement frameElement) {
		try {
			DriverManager.getDriver().switchTo().frame(frameElement);
		} catch (Exception e) {
		}
	}

	public static void switchToDefaultcontent() {
		try {
			DriverManager.getDriver().switchTo().defaultContent();
		} catch (Exception e) {
		}
	}

	public void selectAirport_from_list(String airportsName, List<MobileElement> elements) {
		for (MobileElement airport : elements) {
			if (airport.getText().equalsIgnoreCase(airportsName)) {
				airport.click();
				break;
			}
		}
	}

	public static void navigateToUrl(String url) {
		try {
			DriverManager.getDriver().navigate().to(url);
		} catch (Exception e) {
		}
	}

	public static void closeBrowser() {
		try {
			DriverManager.getDriver().close();
		} catch (Exception e) {
		}
	}

	public static void setText(MobileElement element, String value) {
		try {
			waitForElementVisibility(element);
			element.clear();
			element.sendKeys(value);
		} catch (Exception e) {
		}
	}

	public static void implicitWait(long time) {
		DriverManager.getDriver().manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	/**
	 * Verifying the visibility of element only for assert conditions
	 */

	public static boolean isElementPresent(MobileElement element) {
		boolean elementPresent = false;
		try {
			waitForElementVisibility(element);
			if (element.isDisplayed()) {
				elementPresent = true;
			}
		} catch (Exception e) {

		}
		return elementPresent;
	}

	public static void click(MobileElement element) {
		try {
			waitForElementVisibility(element);
			element.click();
		} catch (Exception e) {
		}
	}

	/******************
	 * getting the text from non editable field
	 *********************/

	public static String getText(MobileElement element) {
		String text = null;
		try {
			waitForElementVisibility(element);
			if (element.getText() != null) {
				text = element.getText();
			}
		} catch (Exception e) {
		}
		return text;
	}

	/******************
	 * getting the text from editable field
	 *********************/

	public static String getValue(MobileElement element) {
		String value = null;
		try {
			waitForElementVisibility(element);
			if (element.getAttribute("value") != null) {
				value = element.getAttribute("value");
			}
		} catch (NullPointerException e) {
		}
		return value;
	}

	/**
	 * Method to select the option from dropdown by value
	 */
	public static void selectByValue(MobileElement element, String value) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByValue(value);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to select the option from dropdown by visible text
	 */
	public static void selectByText(MobileElement element, String text) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByVisibleText(text);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to select the option from dropdown by index
	 */
	public static void selectByIndex(MobileElement element, int index) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByIndex(index);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to perform mouseover action on required element
	 * 
	 * @param element
	 */
	public void jsMouseOver(MobileElement element) {
		String code = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
		((JavascriptExecutor) driver).executeScript(code, element);
	}

	/**
	 * Method to wait for page load using javascript
	 */
	public static void jsWaitForPageLoad() {
		String pageReadyState = (String) ((JavascriptExecutor) DriverManager.getDriver())
				.executeScript("return document.readyState");
		while (!pageReadyState.equals("complete")) {
			pageReadyState = (String) ((JavascriptExecutor) DriverManager.getDriver())
					.executeScript("return document.readyState");

		}
	}

	/**
	 * To pause execution until get expected elements visibility
	 * 
	 * @param element
	 */
	public static void waitForElementVisibility(MobileElement element) {
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOf(element));

	}

	/**
	 * To pause the execution @throws
	 */
	public static void pause(int milliSeconds) throws InterruptedException {
		Thread.sleep(milliSeconds);
	}

	/**
	 * To get Date Format
	 * 
	 * @param Date1
	 * @param Date2
	 */
	public static void enterDate(String Date1, String Date2) {
		try {
			String dateValue = Date1;
			String[] originDate = dateValue.split("/");
			String ODate = originDate[0];
			int OMonth = Integer.parseInt(originDate[1]) - 1;
			String OYear = originDate[2];
			String Odates = "//td[@data-year='" + OYear + "' and @data-month='" + OMonth
					+ "']//following::span[text()='" + ODate + "']";
			String dateValue1 = Date2;
			String[] DestDate = dateValue1.split("/");
			String DDate = DestDate[0];
			int DMonth = Integer.parseInt(DestDate[1]) - 1;
			String DYear = DestDate[2];
			String Ddates = "//td[@data-year='" + DYear + "' and @data-month='" + DMonth
					+ "']//following::span[text()='" + DDate + "']";
			DriverManager.getDriver().findElement(By.xpath(Odates)).click();
			Thread.sleep(3000);
			DriverManager.getDriver().findElement(By.xpath(Ddates)).click();

		} catch (Exception e) {
			e.getLocalizedMessage();
		}

	}

	public String dateSelection(int value) throws IOException, Throwable {

		String dateValue = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); // Now use today date.
		c.add(Calendar.DATE, value);
		String output = sdf.format(c.getTime());
		String[] date = output.split("/");
		String date1 = date[0];
		String month = date[1];
		String year = date[2];
		MonthDate = month + " " + date1;
		int i = Integer.parseInt(date1);
		if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Android")) {

			WebElement dateLbl2 = null;
			String dateText = getNormalizedDateString(date);
			// logger.info("date : " + dateText);
			for (int l = 0; l <= 100; l++) {

				try {
					WebElement dateSelect = driver.findElementByXPath(
							"//android.widget.TextView[contains(@content-desc,'" + dateText + "')]");
					boolean b = dateSelect.isDisplayed();
					boolean d = dateSelect.isEnabled();
					// logger.info(b + "" + d);
					if (b && d) {
						if (l > 0) {
							verticalSwipeByPercentages(0.7, 0.4, 0.7);
						}
						try {
							driver.findElementById("tip_text_view").click();
						} catch (Throwable e) {
							logger.info("no tip");
						}
						dateSelect.click();
						break;
					}

				} catch (Throwable e) {
					logger.info("scrolling down " + l);
					verticalSwipeByPercentages(0.7, 0.5, 0.7);
				}

			}
		} else if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("IOS")) {

			WebElement dateLbl2 = null;
			String dateText = getNormalizedDateString(date);
			logger.info("date : " + dateText);
			for (int l = 0; l <= 100; l++) {

				try {
					WebElement dateSelect = driver
							.findElementByXPath("//XCUIElementTypeCell[contains(@name,'" + dateText + "')]");
					boolean b = dateSelect.isDisplayed();
					boolean d = dateSelect.isEnabled();
					logger.info(b + " " + d);
					if (b && d) {
						try {
							driver.findElementByXPath(
									"//XCUIElementTypeStaticText[contains(@name,'Tip')] |//XCUIElementTypeStaticText[contains(@name,'Astuce')]")
									.click();
						} catch (Throwable e) {
							logger.info("no tip");
						}
						dateSelect.click();
						break;
					}

				} catch (Throwable e) {

					verticalSwipeByPercentages(0.7, 0.6, 0.7);
				}
			}
		}
		return output;
	}

	private String getNormalizedDateString(String date[]) throws NumberFormatException, Exception, Throwable {
		String Frenchmonth = "";
		String dateText = "";
		if (ReadProperty.get_propValue("Local_Language").equalsIgnoreCase("English")) {
			// dateText = date[1].toString() + " " + Integer.parseInt(date[0]) + ", " +
			// date[2];
			dateText = date[1].toString() + " " + date[0] + ", " + date[2];
			return dateText;
		} else {
			logger.info(date[1]);
			if (date[1].equalsIgnoreCase("January")) {
				Frenchmonth = "janvier";
			} else if (date[1].equalsIgnoreCase("February"))
				Frenchmonth = "février";
			else if (date[1].equalsIgnoreCase("March"))
				Frenchmonth = "mars";
			else if (date[1].equalsIgnoreCase("April"))
				Frenchmonth = "avril";
			else if (date[1].equalsIgnoreCase("May"))
				Frenchmonth = "mai";
			else if (date[1].equalsIgnoreCase("June"))
				Frenchmonth = "juin";
			else if (date[1].equalsIgnoreCase("July"))
				Frenchmonth = "juillet";

			else if (date[1].equalsIgnoreCase("August"))
				Frenchmonth = "août";
			else if (date[1].equalsIgnoreCase("September"))
				Frenchmonth = "septembre";
			else if (date[1].equalsIgnoreCase("October"))
				Frenchmonth = "octobre";
			else if (date[1].equalsIgnoreCase("November"))
				Frenchmonth = "novembre";
			else if (date[1].equalsIgnoreCase("December"))
				Frenchmonth = "décembre";
			dateText = Integer.parseInt(date[0]) + " " + Frenchmonth + ", " + date[2];
			return dateText;
		}
	}

	public void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {
		Dimension size = driver.manage().window().getSize();
		int anchor = (int) (size.width * anchorPercentage);
		int startPoint = (int) (size.height * startPercentage);
		int endPoint = (int) (size.height * endPercentage);

		new TouchAction(driver).press(PointOption.point(anchor, startPoint))
				.waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
				.moveTo(PointOption.point(anchor, endPoint)).release().perform();

	}

	public static void swipeByElements(MobileElement startElement, MobileElement endElement) {
		try {
			int startX = startElement.getLocation().getX() + (startElement.getSize().getWidth() / 2);
			int startY = startElement.getLocation().getY() + (startElement.getSize().getHeight() / 2);

			int endX = endElement.getLocation().getX() + (endElement.getSize().getWidth() / 2);
			int endY = endElement.getLocation().getY() + (endElement.getSize().getHeight() / 2);
			new TouchAction(DriverManager.getMDriver()).press(point(startX, startY))
					.waitAction(waitOptions(ofMillis(1000))).moveTo(point(endX, endY)).release().perform();
			logger.info("ele1 to ele2 swipe success");
		} catch (Exception e) {
			logger.info("ele1 to ele2 swipe failed");
		}
	}

}
