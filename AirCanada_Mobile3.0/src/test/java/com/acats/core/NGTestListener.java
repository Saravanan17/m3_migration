package com.acats.core;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.acats.Runner;
import com.aventstack.extentreports.ExtentTest;
import cucumber.api.testng.PickleEventWrapper;
import gherkin.pickles.PickleTag;




public class NGTestListener implements ITestListener {

	BaseUtil baseUtil;
	final static Logger logger = Logger.getLogger(NGTestListener.class);
	public static Map<String,ExtentTest> featureTestMap = new HashMap<String,ExtentTest>();
	public static Map<Integer,ExtentTest> scenarioTestMap = new HashMap<Integer,ExtentTest>();
	public static Map<Integer,String> browsermap = new HashMap<Integer,String>();
	public static Map<Integer,String> failedStepMap = new HashMap<Integer,String>();
	public static Map<Integer,String> failedStepPath = new HashMap<Integer,String>();
	public static Map<String,String> issueKeyMap = new HashMap<String,String>();
	/**
	 * Create Node for each Feature 
	 */
	public synchronized void onTestStart(ITestResult result) {
		Object[] featureTestName =result.getParameters();
		String scenarioName = featureTestName[0].toString();
		String featureName = featureTestName[1].toString();
		String tag = null;
		for (Object inputArg : featureTestName) {
			if(inputArg instanceof PickleEventWrapper) {
				PickleEventWrapper event = (PickleEventWrapper) inputArg;
				List<PickleTag> tags = event.getPickleEvent().pickle.getTags();
				tag =tags.get(0).getName();
			}
		}
		if(!featureTestMap.containsKey(featureName)){
			ExtentTest features= ExtentReportUtil.extent.createTest(featureName);
			featureTestMap.put(featureName, features);
			ScreenshotUtility.featureScreenshotFolder(featureName, Runner.featureFilePath);
		}
		String scenarioDetails = BaseUtil.renderTagName(tag,scenarioName);
		ExtentTest scenarioDef = featureTestMap.get(featureName).createNode(scenarioDetails);
		int key = (int)Thread.currentThread().getId();
		scenarioTestMap.put(key, scenarioDef);
		logger.info("*********************************************************************************************************************");
		logger.info("*********************************************************************************************************************");
		logger.info("*************************************EXECUTING SCENARIO********************************************");
		logger.info("Scenario Name--------------->"+featureTestName[0].toString());
		logger.info("*********************************************************************************************************************");
		logger.info("*********************************************************************************************************************");
	}
	

	/**
	 * Executed after Each Scenario if it is passed
	 */
	public void onTestSuccess(ITestResult result) {
	
	}

	/**
	 * Executed after each Scenario if test is failed
	 */

	public void onTestFailure(ITestResult result) {
		
	}

	public void onTestSkipped(ITestResult result) {

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	/**
	 * Executed Once when Application starts
	 */
	public void onStart(ITestContext context) {
		try {
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}
	
	/** Executed Once when Application Ends
	 * 
	 */
	public void onFinish(ITestContext context) {
		
	}

	/**
	 * To get Browser Details
	 * @return
	 */
	public static String getBrowser() {
		Capabilities cap = ((RemoteWebDriver) DriverManager.getDriver()).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		return StringUtils.capitalize(browserName);
	}

	/**
	 * To get Browser Version
	 * @return
	 */
	public static String getVersion() {
		Capabilities cap = ((RemoteWebDriver) DriverManager.getDriver()).getCapabilities();
		String version = cap.getVersion().toString();
		return version;
	}

}
