package com.acats.core;

import java.awt.AWTException;
import java.util.Map;

import org.testng.Assert;

public class ValidateDigitalDataAC implements FileConstants {

	/**
	 * To Validate Digital data AC 
	 * @param dataMap 
	 * @param actualValueList 
	 * @param destination 
	 * @param origin 
	 * @param odpair From: Sankar Palanisamy
Sent: Tuesday, November 12, 2019 4:36:20 AM
To: Sankar Palanisamy <SankarP@hexaware.com>
Subject: https://stackoverflow.com/questions/12159874/click-method-will-not-always-work 

	 * @param currencycode 
	 * @param bookingmagent 
	 * @throws AWTException 
	 */

	public static void validateData(Map<String, Object> expectedValueDataMap, String currencycode, String odpair, String origin,String destination, String bookingmarket) throws AWTException {
		Map<String, Object> obj = (Map<String, Object>) expectedValueDataMap.get(FLIGHT_SEARCH);
		String actualCurrencyCode = (String) obj.get(CURRENCY_CODE);
		Object actualOrigin = obj.get(ORIGIN);
		Object actualDestination = obj.get(DESTINATION);
		Object actualOdPair =  obj.get(OD_PAIR);
		Object actualBookingmarket =obj.get(BOOKING_MARKET);
		Assert.assertEquals(actualCurrencyCode, currencycode);
		Assert.assertEquals(actualOdPair,odpair);
		Assert.assertEquals(actualBookingmarket,bookingmarket);
	}
}
