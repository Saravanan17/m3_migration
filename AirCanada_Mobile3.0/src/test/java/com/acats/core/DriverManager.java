package com.acats.core;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DriverManager {

	private static ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<RemoteWebDriver>();
	private static ThreadLocal<AppiumDriver> mobileDriver = new ThreadLocal<AppiumDriver>();
	public static String destPath;
	public static WebDriver currentDriver;

	public static void setDriver(WebDriver Driver) {
		webDriver.set((RemoteWebDriver) Driver);
		NGTestListener.browsermap.put((int) Thread.currentThread().getId(),
				webDriver.get().getCapabilities().getBrowserName());
	}

	public static void setMDriver(AppiumDriver<MobileElement> MDriver) {
		mobileDriver.set(MDriver);
		NGTestListener.browsermap.put((int) Thread.currentThread().getId(),
				mobileDriver.get().getCapabilities().getCapability("platformName").toString());
		
	}

	public synchronized static AppiumDriver<MobileElement> getMDriver() {
		return mobileDriver.get();
	}

	// Get Chrome Options
	public static ChromeOptions getChromeOptions() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");

		return options;

	}

	// Get Firefox Options
	public static FirefoxOptions getFirefoxOptions() {
		FirefoxOptions options = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();
		// Accept Untrusted Certificates
		profile.setAcceptUntrustedCertificates(true);
		profile.setAssumeUntrustedCertificateIssuer(false);
		// Use No Proxy Settings
		profile.setPreference("network.proxy.type", 0);
		// Set Firefox profile to capabilities
		options.setCapability(FirefoxDriver.PROFILE, profile);
		return options;
	}

	public synchronized static void setDriver(String browser) {
		if (browser.equals("firefox")) {
			webDriver = ThreadLocal.withInitial(() -> new FirefoxDriver(getFirefoxOptions()));

		} else if (browser.equals("chrome")) {
			webDriver = ThreadLocal.withInitial(() -> new ChromeDriver(getChromeOptions()));

		}
	}

	/**
	 * To get WebDriver
	 * 
	 * @return
	 * @throws Throwable
	 */
	public synchronized static WebDriver getDriver() {
		try {
			BaseUtil.setIsDriverOpened(true);
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return webDriver.get();
	}

	public static boolean waitForPageLoad(WebDriver driver) {
		final JavascriptExecutor js = (JavascriptExecutor) driver;
		boolean docReadyState = false;
		try {
			docReadyState = (Boolean) js.executeScript(
					"return (function() { if (document.readyState != 'complete') {  return false; } if (window.jQuery != null && window.jQuery != undefined && window.jQuery.active) { return false;} if (window.jQuery != null && window.jQuery != undefined && window.jQuery.ajax != null && window.jQuery.ajax != undefined && window.jQuery.ajax.active) {return false;}  if (window.angular != null && angular.element(document).injector() != null && angular.element(document).injector().get('$http').pendingRequests.length) return false; return true;})();");
		} catch (WebDriverException e) {
			docReadyState = true;
		}

		return docReadyState;
	}

	public static void closeDriver(boolean isDriverOpened) throws Throwable {
		if ((isDriverOpened) && (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser"))) {
			DriverManager.getDriver().quit();
		} else if ((isDriverOpened) && (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile"))) {
			DriverManager.getMDriver().quit();
		}
		BaseUtil.setIsDriverOpened(false);
	}

}
