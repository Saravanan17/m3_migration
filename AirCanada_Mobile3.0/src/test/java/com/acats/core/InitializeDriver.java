package com.acats.core;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.browserstack.local.Local;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class InitializeDriver {
	public static String username;
	public static String accessKey;
	public static WebDriver driver;
	public static AppiumDriver<MobileElement> mDriver;
	public static DesiredCapabilities capabilities;
	final static Logger logger = Logger.getLogger(InitializeDriver.class);

	/**
	 * Start Web/Mobile Browser driver
	 * 
	 * @throws Throwable
	 */
	public static void startDriver(String browser) throws Throwable {

		String environment = System.getProperty("BrowserName");
		String configEnv = ReadProperty.get_propValue("Browser_Name");
		environment = browser != null ? browser : environment == null ? configEnv : environment;

		try {
			if (ReadProperty.get_propValue("TypeofExecution").equalsIgnoreCase("cloud")) {
				initilizeBrowserStackDriver(environment);
			} else if (ReadProperty.get_propValue("TypeofExecution").equalsIgnoreCase("Local")) {

				if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser")) {
					if (environment.equalsIgnoreCase("firefox")) {
						WebDriverManager.firefoxdriver().setup();
						driver = new FirefoxDriver();
					} else if (environment.equalsIgnoreCase("chrome")) {
						ChromeOptions capabilities = new ChromeOptions();
						capabilities.setCapability("ignoreProtectedModeSettings", true);
						WebDriverManager.chromedriver().setup();
						driver = new ChromeDriver(capabilities);
					}
					DriverManager.setDriver(driver);
				} else if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile")) {
					String config_file = BaseUtil.getCurrentConfigFile();
					environment = browser != null ? browser : environment == null ? configEnv : environment;

					capabilities = new DesiredCapabilities();
					JSONParser parser = new JSONParser();
					JSONObject config = (JSONObject) parser
							.parse(new FileReader(ReadProperty.get_propValue("browserstackconfigPath") + config_file));
					JSONObject envs = (JSONObject) config.get("environments");
					Map<String, String> envCapabilities = (Map<String, String>) envs.get(environment);
					Iterator it = envCapabilities.entrySet().iterator();

					while (it.hasNext()) {
						Map.Entry<String, String> pair = (Map.Entry) it.next();
						capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
					}

					if ((ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile"))
							&& (ReadProperty.get_propValue("Platform").equalsIgnoreCase("Android"))) {
						String appPath = ReadProperty.get_propValue("AppPath") + "/Android/";
						capabilities.setCapability("app", getAppPath(appPath, ".apk"));

						mDriver = new AndroidDriver<MobileElement>(
								new URL("http://" + config.get("server") + ":" + config.get("port") + "/wd/hub"),
								capabilities);
						DriverManager.setMDriver(mDriver);
					} else if ((ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile"))
							&& (ReadProperty.get_propValue("Platform").equalsIgnoreCase("iOS"))) {
						String appPath = ReadProperty.get_propValue("AppPath") + "/iOS/";
						capabilities.setCapability("app", getAppPath(appPath, ".ipa"));

						mDriver = new IOSDriver<MobileElement>(
								new URL("http://" + config.get("server") + ":" + config.get("port") + "/wd/hub"),
								capabilities);
						DriverManager.setMDriver(mDriver);
					}
				}

			}
		} catch (Exception e) {
			logger.error("ERROR : Driver Error- Driver not initialized" + e.getMessage());
		}
	}

	public static void initilizeBrowserStackDriver(String browser) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String config_file = BaseUtil.getCurrentConfigFile();
		String environment = System.getProperty("BrowserName");
		String configEnv = ReadProperty.get_propValue("Browser_Name");
		environment = browser != null ? browser : environment == null ? configEnv : environment;

		try {
			capabilities = new DesiredCapabilities();
			JSONParser parser = new JSONParser();
			JSONObject config = (JSONObject) parser
					.parse(new FileReader(ReadProperty.get_propValue("browserstackconfigPath") + config_file));
			JSONObject envs = (JSONObject) config.get("environments");
			Map<String, String> envCapabilities = (Map<String, String>) envs.get(environment);
			Iterator it = envCapabilities.entrySet().iterator();

			while (it.hasNext()) {
				Map.Entry<String, String> pair = (Map.Entry) it.next();
				capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
			}

			capabilities.setCapability("build", "M3_Demo");
			capabilities.setCapability("name", "Demo" + timestamp);
			capabilities.setCapability("Project", "ETS");

			Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
			it = commonCapabilities.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				if (capabilities.getCapability(pair.getKey().toString()) == null) {
					capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
				}
			}

			username = System.getenv("BROWSERSTACK_USERNAME");
			if (username == null) {
				username = (String) config.get("user");
			}
			accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
			if (accessKey == null) {
				accessKey = (String) config.get("key");
			}
			String app = System.getenv("BROWSERSTACK_APP_ID");
			if (app != null && !app.isEmpty()) {
				capabilities.setCapability("app", app);
			}
			if (capabilities.getCapability("browserstack.local") != null
					&& capabilities.getCapability("browserstack.local") == "true") {
				Local l = new Local();
				Map<String, String> options = new HashMap<String, String>();
				options.put("key", accessKey);
				l.start(options);
			}
			
			// Adding capabilites for Locale and Languages
			if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile")) 
			{
				String platform = System.getProperty("Platform");
				String conf_platform = ReadProperty.get_propValue("Platform");

				String language = System.getProperty("Language");
				String conf_language = ReadProperty.get_propValue("Language");

				language = language.equalsIgnoreCase("NoLanguage") ? conf_language : language;

				platform = platform.equalsIgnoreCase("NoPlatform") ? conf_platform : platform;

				String languages_cap = platform + "_" + language;
				JSONObject lang = (JSONObject) config.get("Locale_language");
				Map<String, String> langcapabilites = (Map<String, String>) lang.get(languages_cap);
				Iterator lang_it = langcapabilites.entrySet().iterator();

				while (lang_it.hasNext()) {
					Map.Entry<String, String> pair = (Map.Entry) lang_it.next();
					capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
				}
			}
			if ((ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile"))
					&& (ReadProperty.get_propValue("Platform").equalsIgnoreCase("Android"))) {
				mDriver = new AndroidDriver<MobileElement>(
						new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"),
						capabilities);
				DriverManager.setMDriver(mDriver);
			} else if ((ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile"))
					&& (ReadProperty.get_propValue("Platform").equalsIgnoreCase("iOS"))) {
				mDriver = new IOSDriver<MobileElement>(
						new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"),
						capabilities);
				DriverManager.setMDriver(mDriver);
			} else if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser")) {
				driver = new RemoteWebDriver(
						new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"),
						capabilities);
				DriverManager.setDriver(driver);

			}
		} catch (Exception e) {
			logger.error("Driver Not initialize" + e.getMessage());
		}

	}

	public static String getAppPath(String app, String extent) {
		File file = new File(app);
		File f = null;
		try {
			File[] files = file.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					if (name.toLowerCase().endsWith(extent)) {
						return true;
					} else {
						return false;
					}
				}
			});
			f = files[0];
		} catch (Exception e) {
			logger.info("Application under Test app " + extent + "file is not" + app + " folder location");
		}
		return f.getAbsolutePath().toString();
	}
}