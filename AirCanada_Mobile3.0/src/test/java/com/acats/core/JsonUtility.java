package com.acats.core;

import java.util.Map;
import org.junit.Assert;

import io.restassured.response.ValidatableResponse;

public class JsonUtility {

	
	public static void validate_json_response_(Map<String, String> nameValuePairs) {
        ValidatableResponse valResponse = RestUtility.response.then();
        for (Map.Entry<String, String> item : nameValuePairs.entrySet()) {
        //    valResponse.body(item.getKey(), equalTo(item.getValue()));
        }
    }
	
	public void the_is_not_null_in_the_json_response(String elementName) {
        ValidatableResponse valResponse = RestUtility.response.then();
        String actualValue = valResponse.extract().response().body().path(elementName) + "";
        if(actualValue.equals("null")){
            Assert.fail(elementName + " is null ");
        }
    }
	
}