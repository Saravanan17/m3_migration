package com.acats.core;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

/**
 * 
 * @author HX029704
 *
 */


public class ExtentReportUtil extends BaseUtil {

    String fileName = ReadProperty.get_propValue("extentReportPath")+"Report-"+timestampinSeconds()+".html";
  


    public void ExtentReport() throws Throwable {
        extent = new ExtentReports();
        String testEnvironment = ReadProperty.get_propValue("testEnvironment");
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle("Automation Report");
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName("Automation Report");
        
        extent.setSystemInfo("OS : ", System.getProperty("os.name"));
        extent.setSystemInfo("OS Architecture : ", System.getProperty("os.arch"));
        extent.setSystemInfo("Java Version : ", System.getProperty("java.version"));
        extent.setSystemInfo("Machine Name : ", System.getProperty("machine.name"));
        extent.setSystemInfo("Test Environment : ", testEnvironment.toUpperCase());
        extent.attachReporter(htmlReporter);
    }

    public void ExtentReportScreenshot(String scenarioName) throws IOException {
        String base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BASE64);        scenarioDef.fail(scenarioName,
				MediaEntityBuilder.createScreenCaptureFromBase64String(base64Screenshot).build());
    }

    
    public void FlushReport(){
        extent.flush();
    }
    
    public static String timestampinSeconds() {
		return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss-SSS").format(new Date());
	}

    /**
     * To Convert Image to Base64
     * @param filePath
     * @return
     * @throws IOException
     */
    
    public String renderImageBase64(String filePath) throws IOException {
		byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		return encodedString;
	}
    
    /**
	 * To get Browser Details
	 * 
	 * @return
	 */
	public static String getBrowser() {
		Capabilities cap = null;
		String browserName = null;
		if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser")) {
			cap = ((RemoteWebDriver) DriverManager.getDriver()).getCapabilities();
			browserName = cap.getBrowserName().toLowerCase();
		} else if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile")) {
			cap = DriverManager.getMDriver().getCapabilities();
			browserName = cap.getCapability("platformName").toString();
		}
		return StringUtils.capitalize(browserName);
	}

	/**
	 * To get Browser Version
	 * 
	 * @return
	 */
	public static String getVersion() {
		Capabilities cap = null;
		String version = null;

		if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Browser")) {
			cap = ((RemoteWebDriver) DriverManager.getDriver()).getCapabilities();
			version = cap.getVersion().toString();
		} else if (ReadProperty.get_propValue("Environment").equalsIgnoreCase("Mobile")) {
			cap = DriverManager.getMDriver().getCapabilities();
			version = cap.getCapability("osVersion").toString();
		}
		return version;
	}


}
