package com.acats.core;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.mongodb.MongoClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bson.Document;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

public class BaseUtil {

	public static WebDriver Driver;

	public static ExtentReports extent;

	public static ExtentTest scenarioDef;

	public static ExtentTest features;

	public static String reportLocation = "/extent/";

	public static boolean isDriverOpened;

	public static Map<String, String> jiraIssueStatusMap;

	public static String currentConfigFile;

	public static String currentEnvironment;

	public static String currentTagName;

	public static List<Document> documentMap = new ArrayList<>();

	public static boolean getIsDriverOpened() {
		return isDriverOpened;
	}

	public static void setIsDriverOpened(boolean isDriverOpened) {

		BaseUtil.isDriverOpened = isDriverOpened;
	}


	public static Map<String, String> getJiraIssueStatusMap() {
		return jiraIssueStatusMap;
	}

	public static void setJiraIssueStatusMap(Map<String, String> jiraIssueStatusMap) {
		BaseUtil.jiraIssueStatusMap = jiraIssueStatusMap;
	}


	public static String renderTagName(String featuretag, String scenarioName) {
		String tagName = null;
		String tag = fontRed("Tag:");
		String sce = fontRed("Scenario:");
		tagName = tag + featuretag + renderSpace() + sce + scenarioName;
		return tagName;
	}

	public static String fontRed(String text) {
		String redText = "<font color=\"green\"><b>" + text + "</b></font>";
		return redText;

	}

	public static String renderSpace() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <= 5; i++) {
			sb.append("&nbsp");
		}
		return sb.toString();
	}

	public static String getCurrentConfigFile() {
		return currentConfigFile;
	}

	public static void setCurrentConfigFile(String currentConfigFile) {
		BaseUtil.currentConfigFile = currentConfigFile;
	}

	public static String getCurrentEnvironment() {
		return currentEnvironment;
	}

	public static void setCurrentEnvironment(String currentEnvironment) {
		BaseUtil.currentEnvironment = currentEnvironment;
	}

	public static void updateDB(ITestResult result) {
		Document issueDetDoc = new Document();
		Object[] featureTestName = result.getParameters();
		String scenarioName = featureTestName[0].toString();
		String featureName = featureTestName[1].toString();
		String tagId = getCurrentTagName();
		int status = result.getStatus();
		issueDetDoc.put("Feature_Name", featureName);
		issueDetDoc.put("Scenario_Name", scenarioName);
		issueDetDoc.put("id", tagId);
		documentMap.add(issueDetDoc);
	}

	public static void updateMongoDb() {
		new MongoClient().getDatabase("ReportData").getCollection("Reports").insertMany(documentMap);
	}

	public static String getCurrentTagName() {
		return currentTagName;
	}

	public static void setCurrentTagName(String currentTagName) {
		BaseUtil.currentTagName = currentTagName;
	}

	public static boolean isDriverOpened() {
		String browser = "";
		boolean isDriverOpened = false;
		if (!(NGTestListener.browsermap.isEmpty())
				&& (NGTestListener.browsermap.get((int) Thread.currentThread().getId()) != null)) {
			browser = NGTestListener.browsermap.get((int) Thread.currentThread().getId()).toString();
			isDriverOpened = browser != null ? true : false;

		}
		return isDriverOpened;
	}
}
