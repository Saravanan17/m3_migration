package com.acats.core;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Lib_WebGeneric {

	WebDriver driver;
	private static String homeWindow = null;

	public static void accept_Alert() {
		try {
			Alert alert = DriverManager.getDriver().switchTo().alert();
			alert.accept();
		} catch (Exception e) {
		}
	}

	/**
	 * Authenticating the alert
	 * 
	 * @param actions
	 * @param username
	 * @param password
	 * @return
	 */

	public String screenCapture(String imgLocation) {
		File scrFile = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(imgLocation));
		} catch (IOException e) {
		}
		return imgLocation;
	}

	/**
	 * Method to switch to the newly opened window
	 */
	public static void switchToWindow() {
		homeWindow = DriverManager.getDriver().getWindowHandle();
		for (String window : DriverManager.getDriver().getWindowHandles()) {
			DriverManager.getDriver().switchTo().window(window);
		}
	}

	/**
	 * To navigate to the main window from child window
	 */
	public static void switchToMainWindow() {
		for (String window : DriverManager.getDriver().getWindowHandles()) {
			if (!window.equals(homeWindow)) {
				DriverManager.getDriver().switchTo().window(window);
				DriverManager.getDriver().close();
			}
			DriverManager.getDriver().switchTo().window(homeWindow);
		}
	}

	/**
	 * This method returns the no.of windows present
	 * 
	 * @return
	 */
	public static int getWindowCount() {
		return DriverManager.getDriver().getWindowHandles().size();
	}

	/****************** frames *********************/

	public static void frames(WebElement frameElement) {
		try {
			DriverManager.getDriver().switchTo().frame(frameElement);
		} catch (Exception e) {
		}
	}

	public static void switchToDefaultcontent() {
		try {
			DriverManager.getDriver().switchTo().defaultContent();
		} catch (Exception e) {
		}
	}

	public static void navigateToUrl(String url) {
		try {
			DriverManager.getDriver().navigate().to(url);
		} catch (Exception e) {
		}
	}

	public static void closeBrowser() {
		try {
			DriverManager.getDriver().close();
		} catch (Exception e) {
		}
	}

	public static void setText(WebElement element, String value) {
		try {
			waitForElementVisibility(element);
			element.clear();
			element.sendKeys(value);
		} catch (Exception e) {
		}
	}

	public static void implicitWait(long time) {
		DriverManager.getDriver().manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	/**
	 * Verifying the visibility of element only for assert conditions
	 */

	public static boolean isElementPresent(WebElement element) {
		boolean elementPresent = false;
		try {
			waitForElementVisibility(element);
			if (element.isDisplayed()) {
				elementPresent = true;
			}
		} catch (Exception e) {

		}
		return elementPresent;
	}

	public static void click(WebElement element) {
		try {
			waitForElementVisibility(element);
			element.click();
		} catch (Exception e) {
		}
	}

	/******************
	 * getting the text from non editable field
	 *********************/

	public static String getText(WebElement element) {
		String text = null;
		try {
			waitForElementVisibility(element);
			if (element.getText() != null) {
				text = element.getText();
			}
		} catch (Exception e) {
		}
		return text;
	}

	/******************
	 * getting the text from editable field
	 *********************/

	public static String getValue(WebElement element) {
		String value = null;
		try {
			waitForElementVisibility(element);
			if (element.getAttribute("value") != null) {
				value = element.getAttribute("value");
			}
		} catch (NullPointerException e) {
		}
		return value;
	}

	/**
	 * Method to select the option from dropdown by value
	 */
	public static void selectByValue(WebElement element, String value) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByValue(value);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to select the option from dropdown by visible text
	 */
	public static void selectByText(WebElement element, String text) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByVisibleText(text);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to select the option from dropdown by index
	 */
	public static void selectByIndex(WebElement element, int index) {
		try {
			Select obj_select = new Select(element);
			obj_select.selectByIndex(index);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to perform mouseover action on required element
	 * 
	 * @param element
	 */
	public void jsMouseOver(WebElement element) {
		String code = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
		((JavascriptExecutor) driver).executeScript(code, element);
	}

	/**
	 * Method to wait for page load using javascript
	 */
	public static void jsWaitForPageLoad() {
		String pageReadyState = (String) ((JavascriptExecutor) DriverManager.getDriver()).executeScript("return document.readyState");
		while (!pageReadyState.equals("complete")) {
			pageReadyState = (String) ((JavascriptExecutor) DriverManager.getDriver()).executeScript("return document.readyState");

		}
	}

	/**
	 * To pause execution until get expected elements visibility
	 * 
	 * @param element
	 */
	public static void waitForElementVisibility(WebElement element) {
		WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), 30);
		// wait.until(ExpectedConditions.visibilityOf(element));

	}

	/**
	 * To pause the execution @throws
	 */
	public static void pause(int milliSeconds) throws InterruptedException {
		Thread.sleep(milliSeconds);
	}


	/**
	 * To get Date Format
	 * @param Date1
	 * @param Date2
	 */
	public static void enterDate(String Date1, String Date2) {
		try {
			String dateValue = Date1;
			String[] originDate = dateValue.split("/");
			String ODate = originDate[0];
			int OMonth = Integer.parseInt(originDate[1])-1;
			String OYear = originDate[2];
			String Odates="//td[@data-year='" + OYear + "' and @data-month='" + OMonth
					+ "']//following::span[text()='" + ODate + "']";
			String dateValue1 = Date2;
			String[] DestDate = dateValue1.split("/");
			String DDate = DestDate[0];
			int DMonth = Integer.parseInt(DestDate[1])-1;
			String DYear = DestDate[2];
			String Ddates="//td[@data-year='" + DYear + "' and @data-month='" + DMonth
					+ "']//following::span[text()='" + DDate + "']";
			DriverManager.getDriver().findElement(By.xpath(Odates)).click();
			Thread.sleep(3000);
			DriverManager.getDriver().findElement(By.xpath(Ddates)).click();

		} catch (Exception e) {
			e.getLocalizedMessage();
		}

	}

}
