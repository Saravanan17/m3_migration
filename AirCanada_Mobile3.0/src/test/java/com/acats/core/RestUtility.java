package com.acats.core;

import java.util.List;

import org.json.simple.JSONObject;

import io.cucumber.datatable.*;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestUtility {



	public static String path; 
	public static Response response;
	public static ContentType contentType;
	public static RequestSpecBuilder requestBuilder;


	public static void setBaseURI (String baseURI){
		requestBuilder.setBaseUri(baseURI);
	//	RestAssured.baseURI = baseURI;
	}

	public static void setBasePath(String basePathTerm){
		RestAssured.basePath = basePathTerm;
	}

	public static void resetBaseURI (){
		RestAssured.baseURI = null;
	}


	public static void resetBasePath(){
		RestAssured.basePath = null;
	}

	public static void  createSearchQueryPath(String searchTerm, String jsonPathTerm, String param, String paramValue) {
		path = searchTerm + "/" + jsonPathTerm + "?" + param + "=" + paramValue;
	}


	public static Response getResponse() {
		return response;
	}

	public static JsonPath getJsonPath (Response res) {
		String json = res.asString();
		return new JsonPath(json);
	}

	public static ContentType getContentType() {
		return contentType;
	}

	public static void setContentType(ContentType contentType) {
		RestUtility.contentType = contentType;
	}

	public static RequestSpecBuilder constructPostbody(List<List<String>> datTable) {
		requestBuilder= new RequestSpecBuilder();
		JSONObject requestParams = new JSONObject();
		for (int j = 1; j < datTable.size(); j++) {
			List<String> keyValue = datTable.get(j);
			requestParams.put(keyValue.get(0), keyValue.get(1));
		}
		requestBuilder.setBody(requestParams.toJSONString());
		return requestBuilder;
	}

	public static void constructGetPathParam(List<List<String>> datTable) {
		requestBuilder= new RequestSpecBuilder();
		JSONObject requestParams = new JSONObject();
		for (int j = 1; j < datTable.size(); j++) {
			List<String> keyValue = datTable.get(j);
			requestParams.put(keyValue.get(0), keyValue.get(1));
		}
		requestBuilder.addParam(requestParams.toJSONString());
	}
}

