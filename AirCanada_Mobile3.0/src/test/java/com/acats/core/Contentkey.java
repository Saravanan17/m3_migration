package com.acats.core;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Contentkey {
	public static HashMap<String, String> getCopyKeyValue = new HashMap<String, String>();

	public static HashMap<String, String> getCopyKeybyLanguage() throws IOException, InvalidFormatException {

		FormulaEvaluator fmevlEvaluator;
		String language = System.getProperty("Language") == null ? ReadProperty.get_propValue("Language")
				: System.getProperty("Language");
		Workbook workbook = WorkbookFactory.create(new File(ReadProperty.get_propValue("contentSheetPath")));
		fmevlEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
		DataFormatter formatter = new DataFormatter();
		int copykeyindex = 0;
		for (int i = 1; i < workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Iterator<Cell> colIterator = sheet.getRow(0).cellIterator();
			while (colIterator.hasNext()) {
				Cell colname = colIterator.next();
				if (colname.toString().equalsIgnoreCase("Copy Key")) {
					copykeyindex = colname.getColumnIndex();
					break;
				}
			}
			for (int j = 1; j <= sheet.getLastRowNum(); j++) {
				Row row = sheet.getRow(j);
				String cell = formatter
						.formatCellValue(row.getCell(copykeyindex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));

				if (!(cell.isEmpty())) {
					Cell cellvalue = (language.equalsIgnoreCase("English"))
							? row.getCell(copykeyindex + 1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK)
							: row.getCell(copykeyindex + 2, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);// ,
					switch (cellvalue.getCellType()) {
					case STRING:
						getCopyKeyValue.put(cell, formatter.formatCellValue(cellvalue));
						break;
					case BLANK:
						break;
					case FORMULA:
						getCopyKeyValue.put(cell, formatter.formatCellValue(cellvalue, fmevlEvaluator));
						break;
					case NUMERIC:
						getCopyKeyValue.put(cell, formatter.formatCellValue(cellvalue));
						break;
					case ERROR:
						break;
					case BOOLEAN:
						break;

					default:
						break;
					}
				}
			}
		}
		return getCopyKeyValue;

	}

	public static String getContentsheetbyCopykey(String copykey) {
		
		return getCopyKeyValue.get(copykey);
		
	}
}
