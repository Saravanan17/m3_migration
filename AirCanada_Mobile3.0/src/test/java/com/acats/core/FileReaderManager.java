package com.acats.core;

import com.acats.Runner;
import com.acats.appname.locators.HomeLocator;
import com.acats.appname.locators.LoginLocator;
import com.acats.appname.locators.SearchResults;
import com.acats.mobile.locators.Navigationtab;

import io.appium.java_client.AppiumDriver;

public class FileReaderManager {

	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private static Lib_WebGeneric libweb;
	private static Runner runner;
	private static HomeLocator acWeb;
	private DriverManager driver;
	private static LoginLocator AC_01_Login_PO;
private static SearchResults search;
	private FileReaderManager() {
	}

	public static FileReaderManager getInstance() {
		return fileReaderManager;
	}

	public DriverManager getWebDriver() {
		return (driver == null) ? new DriverManager() : driver;
	}

	public static HomeLocator getAcWebsite() {
		return (acWeb == null) ? new HomeLocator() : acWeb;
	}

	public Runner getRunner() {
		return (runner == null) ? new Runner() : runner;
	}	

	public Lib_WebGeneric getwebgeneric() {
		return (libweb == null) ? new Lib_WebGeneric() : libweb;

	}
	
	public SearchResults getSearchResult() {
		return (search == null) ? new SearchResults() : search;

	}

	public static LoginLocator getAC_01_Login_PO() {
		return AC_01_Login_PO;
	}

	public static void setAC_01_Login_PO(LoginLocator aC_01_Login_PO) {
		AC_01_Login_PO = aC_01_Login_PO;
	}
	
	public Navigationtab navication;
	
	public Navigationtab getNavigationtab(LoginLocator aC_01_Login_PO) {
		return (navication == null) ? new Navigationtab(DriverManager.getMDriver()) : navication;

	}
		
}
