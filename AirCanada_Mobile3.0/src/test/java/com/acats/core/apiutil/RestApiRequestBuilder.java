package com.acats.core.apiutil;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestApiRequestBuilder {

	private static RequestSpecification request;
	private static Response response;



	public static RequestSpecification constructRequestParam(String parameterName,String value) {
		request =given().param(parameterName,value);
		return request;
	}


	public static RequestSpecification constructPostbody(List<List<String>> datTable) {
		RestAssured.baseURI ="https://reqres.in/api/users";
		 RequestSpecification request = RestAssured.given();
		 
		 JSONObject requestParams = new JSONObject();
		 requestParams.put("name", "Virender"); // Cast
		 requestParams.put("job", "Singh");
		 request.body(requestParams.toJSONString());
		 Response response = request.post("");
		return request;
	}

	public static Response callRestGetApi(String url, RequestSpecification request) {
		response = request.when().get(url); 
		return response;
	}

	public static Response callRestPostApi(String url,RequestSpecification request) {
		response =request.when().post("");
		return response;
	}

	public static Response callSoapPostApi(FileInputStream fileInputStream){
		try {
			response = given()
					.header("Content-Type", "text/xml")
					.and()
					.body(IOUtils.toString(fileInputStream, "UTF-8"))
					.when()
					.post("");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
	
	public static Response callSoapPostApi_new(String requestBody){
        try {
        response = given()
                .header("Accept-Encoding", "gzip,deflate")
                 .header("Content-Type", "text/xml;charset=UTF-8")
                 .header("SOAPAction", "")
                .and()
                .body(requestBody)
                .when()
                .post();
        System.out.println("^^^^^^^Response is "+response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}


