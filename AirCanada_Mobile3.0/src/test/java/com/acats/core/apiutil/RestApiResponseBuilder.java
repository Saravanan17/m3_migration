package com.acats.core.apiutil;

import java.util.Map;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.apache.commons.lang3.StringUtils;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class RestApiResponseBuilder {
	
	
	private static ValidatableResponse json;
	
	public static ValidatableResponse retrieveJsonResponse(Response response ,int statusCode) {
		json = response.then().statusCode(statusCode);
		return json;
	}
	
	 public void response_contains_in_any_order(Map<String, String> responseFields) {
	        for (Map.Entry<String, String> field : responseFields.entrySet()) {
	            if (StringUtils.isNumeric(field.getValue())) {
	                json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
	            } else {
	                json.body(field.getKey(), containsInAnyOrder(field.getValue()));
	            }
	        }
	    }

}
