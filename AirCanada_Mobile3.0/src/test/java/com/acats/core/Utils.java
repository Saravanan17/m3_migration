package com.acats.core;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class Utils {

	static Logger logger = Logger.getLogger(Utils.class);

	/**
	 * To wait for the specific element on the page
	 * 
	 * @param driver  : Webdriver
	 * @param element : Webelement to wait for
	 * @param maxWait : Max wait duration
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = Stopwatch();
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				logger.info("Element is displayed:: " + element.toString());
			}
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			logger.info("Unable to find a element after " + elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * To wait for the specific element on the page
	 * 
	 * @param driver  : Webdriver
	 * @param element : Webelement to wait for
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element) {
		boolean statusOfElementToBeReturned = false;
		long startTime = Stopwatch();
		long maxwait = Long.parseLong(ReadProperty.get_propValue("maxwait"));
		WebDriverWait wait = new WebDriverWait(driver, maxwait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				logger.info("Element is displayed:: " + element.toString());
			}
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			logger.info("Unable to find a element after " + elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	public static long Stopwatch() {
		long start = System.currentTimeMillis();

		return start;
	}

	public static double elapsedTime(long start) {
		long now = System.currentTimeMillis();
		return (now - start) / 1000.0;
	}

	/**
	 * To check whether locator string is xpath or css
	 * 
	 * @param driver
	 * @param locator
	 * @return Webelement
	 */
	public static WebElement checkLocator(WebDriver driver, String locator) {
		WebElement element = null;
		if (locator.startsWith("//")) {
			element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(400))
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		} else {
			element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(400))
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
							.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
		}
		return element;
	}

	/**
	 * To check whether locator string is xpath or css
	 * 
	 * @param driver
	 * @param locator
	 * @return Webelement
	 */
	public static List<WebElement> checkLocators(WebDriver driver, String locator) {
		List<WebElement> element = null;
		if (locator.startsWith("//")) {
			element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(400))
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
							.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
		} else {
			element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(400))
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
					.withMessage("Couldn't find " + locator))
							.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(locator)));
		}
		return element;
	}

	/**
	 * To Check Radio button is checked or not
	 * 
	 * @param element
	 * @return
	 */
	public static String getRadioOrCheckboxChecked(WebElement element) {
		if (element.getAttribute("class").contains("active")) {
			return "Yes";
		}

		if (null != element.getAttribute("checked")) {
			return "Yes";
		}

		for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
			if (childElement.getAttribute("class").contains("active")) {
				return "Yes";
			}
		}

		return "No";
	}

	public static boolean isRadioOrCheckBoxSelected(WebElement element) {
		if (element.getAttribute("class").contains("active")) {
			return true;
		}

		if (null != element.getAttribute("checked")) {
			return true;
		}

		for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
			if (childElement.getAttribute("class").contains("active")) {
				return true;
			}
		}

		return false;
	}

	public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable) {
		if ("YES".equalsIgnoreCase(enableOrDisable)) {
			if (!(isRadioOrCheckBoxSelected(element))) {
				element.click();
			}
		}
		if ("NO".equalsIgnoreCase(enableOrDisable)) {
			if (isRadioOrCheckBoxSelected(element)) {
				element.click();
			}
		}
	}

	public static void javaScriptMouseHoverForIE(WebDriver driver, WebElement element) {
		String code = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
		((JavascriptExecutor) driver).executeScript(code, element);
	}

	public static void javaScriptMouseHover(WebDriver driver, WebElement element) {
		String javaScript = "var evObj = document.createEvent('MouseEvents');"
				+ "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
				+ "arguments[0].dispatchEvent(evObj);";
		((JavascriptExecutor) driver).executeScript(javaScript, element);
	}

	public static void mouseHover(WebDriver driver, WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).clickAndHold(element).build().perform();
	}

	/**
	 * Wrapper to select option from combobox in browser and doesn't wait for
	 * spinner to disappear
	 * 
	 * @param btn                : String Input (CSS Locator) [of the ComboBox
	 *                           Field]
	 * 
	 * @param optToSelect        : option to select from combobox
	 * 
	 * @param driver             : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception
	 */
	public static void selectFromComboBox(String btn, String optToSelect, WebDriver driver, String elementDescription)
			throws Exception {

		WebElement element = checkLocator(driver, btn);
		if (!Utils.waitForElement(driver, element, 1))
			throw new Exception(elementDescription + " not found in page!!");

		try {
			Select selectBox = new Select(element);
			selectBox.selectByValue(optToSelect);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

	}// selectFromComboBox

	/**
	 * Wrapper to click on button/text/radio/checkbox in browser
	 * 
	 * @param btn                : String Input (CSS Locator) [of the Button Field]
	 * @param driver             : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception
	 */
	public static void clickOnElement(String btn, WebDriver driver, String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, btn);
		if (!Utils.waitForElement(driver, element, 1))
			throw new Exception(elementDescription + " not found in page!!");

		try {
			element.click();
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}

	}// clickOnButton

	public static void actionClick(WebElement element, WebDriver driver, String elementDescription) throws Exception {
		if (!Utils.waitForElement(driver, element, 5))
			throw new Exception(elementDescription + " not found in page!!");

		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click(element).build().perform();
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}
	}

	public static void javascriptClick(WebElement element, WebDriver driver, String elementDescription)
			throws Exception {
		if (!Utils.waitForElement(driver, element, 5))
			throw new Exception(elementDescription + " not found in page!!");

		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (NoSuchElementException e) {
			throw new Exception(elementDescription + " not found in page!!");
		}
	}

	public static int countElements(String xpath, WebDriver driver) {
		return driver.findElements(By.xpath(xpath)).size();
	}
/**
 * To waitUntil Angular Page loads and pending https request completed
 * @param driver
 */
	public static void waitforPageLoad(WebDriver driver) {
		try {
			ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driver) {
					return Boolean.valueOf(((JavascriptExecutor) driver)
							.executeScript("return (window.angular !== undefined) "
									+ "&& (angular.element(document).injector() !== undefined) "
									+ "&& (angular.element(document).injector().get('$http').pendingRequests.length === 0)")
							.toString());
				}
			};
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(pageLoadCondition);
		} catch (Exception e) {
			System.out.print(e);
			logger.error("Unable to load the page correctly");
		}
	}

}
