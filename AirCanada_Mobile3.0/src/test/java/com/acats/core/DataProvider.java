package com.acats.core;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.hw.ExtDataProvider;

import java.io.File;
import java.io.FileReader;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DataProvider implements ExtDataProvider,FileConstants {

	HashMap<String, HashMap<String, String[]>> data;
	static Workbook wb = null;
	static Sheet ws = null;
	DataFormatter formatter = new DataFormatter();
	int currentRow=0;
	final static Logger logger = Logger.getLogger(DataProvider.class);

	public DataProvider() throws Throwable {
		if (ReadProperty.get_propValue("FileMode").equalsIgnoreCase("Excel")) {
			data = new LinkedHashMap<>();
			try {
				String excelPath = ReadProperty.get_propValue("excelPath").trim();
				File srcFile = new File(excelPath);
				File destFile = new File("./temp.xlsx");
				FileUtils.copyFile(srcFile, destFile);
				wb = WorkbookFactory.create(destFile);
				FileUtils.forceDeleteOnExit(destFile);
				DataFormatter formatter = new DataFormatter();
				Iterator<Sheet> sheetIterator = wb.sheetIterator();
				int sheetnum = 0;
				while (sheetIterator.hasNext()) {
					ws = sheetIterator.next();
					Row scrow;
					String dataHeader = null;
					String cellValues[] = null;
					String scenarioName = null;

					int sceIdCol = 0;
					String sceColCellValue;
					boolean started = false;
					HashMap<String, String[]> tempData = null;
					Iterator<Row> rowIterator = ws.rowIterator();
					while (rowIterator.hasNext()) {
						Row row = rowIterator.next();
						scrow = ws.getRow(0);
						if (!started) {
							sceColCellValue = formatter.formatCellValue(scrow.getCell(0));
							if (!sceColCellValue.trim().equals("")) {
								sceColCellValue = sceColCellValue.replaceAll("\\s+", "").toLowerCase();
								if (sceColCellValue.equals("scenarioid")) {
									sceIdCol = 0;
									started = true;
								}
							}
						} else {

							dataHeader = formatter
									.formatCellValue(row.getCell(sceIdCol, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));
							if (!dataHeader.equals("")) {
								if (tempData != null) {
									data.put(scenarioName, tempData);
								}
								tempData = new HashMap<>();
								scenarioName = dataHeader;
							}
							dataHeader = formatter.formatCellValue(
									row.getCell(sceIdCol + 1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));
							if (!dataHeader.equals("")) {
								List<String> tempValues = new ArrayList<>();
								String tempCellVal;
								for (int j = 2; j < row.getLastCellNum(); ++j) {

									tempCellVal = formatter.formatCellValue(
											row.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));

									if (!tempCellVal.equals(""))
										tempValues.add(tempCellVal);
								}
								cellValues = new String[tempValues.size()];
								tempValues.toArray(cellValues);
								tempData.put(dataHeader, cellValues);
							}
						}
					}
					if (tempData != null)
						data.put(scenarioName, tempData);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		} else if (ReadProperty.get_propValue("FileMode").equalsIgnoreCase("JSON")) {
			data = new LinkedHashMap<>();
			String testScenarioId;

			Object obj = new JSONParser()
					.parse(new FileReader(ReadProperty.get_propValue("JsonPath")));
			JSONObject issueObject = (JSONObject) obj;
			JSONArray issueList = (JSONArray) issueObject.get("issue");
			for (int i = 0; i < issueList.size(); i++) {
				JSONObject issueKey = (JSONObject) issueList.get(i);
				testScenarioId = (String) issueKey.get("TestId");
				JSONObject objj = (JSONObject) issueKey.get("TestData");
				HashMap<String, Object> testDataMap = new Gson().fromJson(objj.toString(), HashMap.class);
				HashMap<String, String[]> tempDataMap = new HashMap<String, String[]>();
				for (Map.Entry data : testDataMap.entrySet()) {
					String[] strArray = new String[] { data.getValue().toString() };
					tempDataMap.put(data.getKey().toString(), strArray);
				}
				data.put(testScenarioId, tempDataMap);
			}

		} else if (ReadProperty.get_propValue("FileMode").equalsIgnoreCase("XML")) {

			data = new LinkedHashMap<>();
			HashMap<String, String[]> tempDataMap;
			try {
				String xmlPath = ReadProperty.get_propValue("xmlPath").trim();
				File inputFile = new File(xmlPath);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(inputFile);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("scenarioID");

				// Iterate the xml Root elememt
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						String scenarioID = eElement.getAttribute("id");
						NodeList carNameList = eElement.getElementsByTagName("item");
						tempDataMap = new HashMap<String, String[]>();
						for (int count = 0; count < carNameList.getLength(); count++) {
							Node node1 = carNameList.item(count);
							if (node1.getNodeType() == node1.ELEMENT_NODE) {
								Element car = (Element) node1;

								String[] strArray = new String[] { car.getTextContent().toString() };
								tempDataMap.put(car.getAttribute("key"), strArray);
								data.put(scenarioID, tempDataMap);
							}

						}

					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

		}else if (ReadProperty.get_propValue("FileMode").equalsIgnoreCase("HExcel")) {
			data = new LinkedHashMap<>();
			String excelPath = ReadProperty.get_propValue("HexcelPath").trim();
			Workbook workbook = WorkbookFactory.create(new File(excelPath));
			HashMap<String, String[]> testDataMap;
			data = new LinkedHashMap<>();
			for(Sheet sheet: workbook) {
			}
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();
			HashMap<String, String[]> tempData = null;
			int currentRow=0;
			int currentRowIndex = 0 ;
			String key =null;
			for (int i= currentRow;i<=sheet.getLastRowNum();i++) {
				testDataMap = new HashMap<>();;
				Row row = sheet.getRow(currentRow);
				if(row!=null) {
					for(Cell cell: row) {
						int colIndex = cell.getColumnIndex();
						if(colIndex!=0) {
							String dataHeader = dataFormatter.formatCellValue(cell);
							currentRowIndex =retrieveCurrentColValues(colIndex,currentRow,sheet,dataHeader,testDataMap);
						}else {
							if (cell != null) {
								key = cell.getStringCellValue();
							}
						}
					}
					data.put(key, testDataMap);
					currentRow = currentRowIndex;

				}
			}
		}
	}



	private static int retrieveCurrentColValues(int columnIndex,int rowInd,Sheet sheet,String dataHeader, HashMap<String, String[]> testDataMap) {
		String cellValues[] = null;
		int currentRowIndex = 0;
		List<String> tempValues = new ArrayList<>();
		for (int rowIndex = rowInd+1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			Row row = sheet.getRow(rowIndex);
			currentRowIndex=rowIndex+1;
			if (row != null) {
				Cell cell = row.getCell(columnIndex);
				if (cell != null) {
					String cellValue = cell.getStringCellValue();
					if(cellValue!=null&&cellValue.length()>=1) {
						tempValues.add(cellValue.trim());
						cellValues = new String[tempValues.size()];
						tempValues.toArray(cellValues);
						testDataMap.put(dataHeader.trim(), cellValues);
					}else {
						break;
					}
				}else {
					break;
				}
			}else {
				break;
			}
		}
		return currentRowIndex;
	}




	public HashMap<String, String[]> provideFor(String arg0) {
		return data.get(arg0);
	}

	

}
