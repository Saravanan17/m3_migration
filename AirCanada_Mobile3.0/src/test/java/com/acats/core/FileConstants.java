package com.acats.core;

public interface FileConstants {
	public static String BROWSER ="browser";
	public static String CONSOLE="console-api";
	public static String FLIGHT_SEARCH="flightSearch";
	public static String CURRENCY_CODE="currencyCode";
	public static String ORIGIN="origin";
	public static String DESTINATION="destination";
	public static String OD_PAIR="odPair";
	public static String BOOKING_MARKET="bookingMarket";
	/**
	 * Mention the directory as below 
	 */
	public static String DIGITAL_DATA ="console.dir(digitalDataAC)";
	public static String DIGITAL_DATA_JSON = "console.warn(JSON.stringify(digitalDataAC))";
	public static String workingDir = System.getProperty("user.dir");
}
