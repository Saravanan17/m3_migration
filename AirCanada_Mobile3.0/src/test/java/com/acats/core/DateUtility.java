package com.acats.core;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

public class DateUtility {
                
	// Date formats
	public static final String DATE_FORMAT_DD_MM_YYYY_DEFAULT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_EEEEE_MMMMM_DD_yyyy = "EEEEE,MMMMM dd,yyyy";
	public static final String DATE_FORMAT_EEE_MMM_DD = "EEE-MMM dd yy";
	final static Logger logger = Logger.getLogger(DateUtility.class);

	/**
	 * Method to convert String formed Date to Date object by accepting String and using Default Date format i.e. "dd-MM-yyyy"
	 * @param Date in String type
	 * @return Converted Date Object
	 */
	public static Date parseToDate(String date) {
		SimpleDateFormat dateFormat = null;
		Date defaultDate = null;
		try {
			dateFormat = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY_DEFAULT);
			defaultDate = dateFormat.parse(date);
		} catch (Exception e) {
			logger.error("error in parsing dateString [" + date + "] , Expected format [ "
					+ DATE_FORMAT_DD_MM_YYYY_DEFAULT + " ] , msg: " + e.getMessage());
		}
		return defaultDate;
	}
	
	
	/**
	 * Method to convert String formed Date to Date object by accepting String and using user specified Date format
	 * @param date :String formed Date
	 * @param passedStringDateFormat :Passed String formed Date format (different to Default Date format)
	 * @return Date:Converted Date using passed date format
	 */
	public static Date parseToDate(String date,String passedStringDateFormat) {
		SimpleDateFormat dateFormat = null;
		Date parsedDate = null;
		try {
			dateFormat = new SimpleDateFormat(passedStringDateFormat); 
			parsedDate = dateFormat.parse(date);
		} catch (Exception e) {
			logger.error("error in parsing dateString [" + date + "] , Expected format [ "
					+ passedStringDateFormat + " ] , msg: " + e.getMessage());
		}
		return parsedDate;
	}
	

	/**
	 * Method to convert Date object to String formed Date by accepting user specified Date format
	 * @param Date :Date object to be converted to String
	 * @param OutputDateFormat :User expected Date Format
	 * @return sDate :String formed Date
	 */
	public static String format(Date date, String OutputDateFormat) {
		SimpleDateFormat dateFormat = null;
		String sDate = null;
		try {
			dateFormat = new SimpleDateFormat(OutputDateFormat);
			sDate = dateFormat.format(date);
		} catch (Exception e) {
			logger.error("error in Formatting dateString [" + date + "] , [ "
					+ OutputDateFormat + " ]  should be valid format, msg: " + e.getMessage());
	 
		}
		return sDate;
	}

	/**
	 * Method to convert Default Formatted date String to another user expected format
	 * @param date :Default Formatted date String
	 * @param OutputDateFormat :User expected Date format
	 * @return outputDate :String formed date in user expected format
	 */
	public static String format(String date, String OutputDateFormat) {
		SimpleDateFormat dateFormat = null;
		Date defaultDate = null;
		String outputDate = null;
		try {
			dateFormat = new SimpleDateFormat(DATE_FORMAT_DD_MM_YYYY_DEFAULT);
			defaultDate = dateFormat.parse(date);
		}catch(Exception e) {
			logger.error("error in parsing dateString [" + date + "] , Expected format [ "
					+ DATE_FORMAT_DD_MM_YYYY_DEFAULT + " ] , msg: " + e.getMessage());
		}
		try {
			SimpleDateFormat outputFormat = new SimpleDateFormat(OutputDateFormat);
			outputDate = outputFormat.format(defaultDate);

		} catch (Exception e) {
			logger.error("error in formating [" + defaultDate + "] , format [ "
					+ OutputDateFormat + " should be in valid format] , msg: " + e.getMessage());
		}
		return outputDate;
	}

	/**
	 * Method to validate the format of date given by user
	 * 
	 * @param String
	 * @param String
	 * @return boolean
	 */
	public static boolean isValidFormat(String format, String value) {
		Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			date = dateFormat.parse(value);
			if (!value.equals(dateFormat.format(date))) {
				date = null;
				logger.error("User passes incorrect date format");
			}
		} catch (ParseException ex) {
			logger.error(
					"error in parsing dateString [" + value + "] , Expected format [ " + format + " ] ");
		}
		return date != null;
	}

	/**
	 * Method to convert date from any user given date String format to another date String format
	 * @param inputStringFormat :Given input date String format
	 * @param date :Given input date String
	 * @param OutputDateFormat :User expected date String format
	 * @return outputDate : Converted Date String
	 */
	public static String format(String inputStringFormat, String date, String OutputDateFormat)  {

		SimpleDateFormat dateFormat = null;
		Date defaultDate = null;		
		String outputDate = null;
		boolean validFormat = isValidFormat(inputStringFormat, date);
		try {
			if (validFormat != false) {
				dateFormat = new SimpleDateFormat(inputStringFormat);
				defaultDate = dateFormat.parse(date);
			}
			SimpleDateFormat outputFormat = new SimpleDateFormat(OutputDateFormat);
			outputDate = outputFormat.format(defaultDate);
		}catch(Exception e) {
			logger.error("error in formating [" + defaultDate + "] , format [ "
					+ OutputDateFormat + " should be in valid format] , msg: " + e.getMessage());
		}
		return outputDate;
	}
	
	/**
	 * Method to return given String formed Date with OrdinalIndicator
	 * @param date :Date String to be converted
	 * @param dateFormat :Expected dateFormat to be included with OrdinalIndicator
	 * @return Date :Processed Date string with OrdinalIndicator
	 */
	public static String getOridnalIndicatedDate(String date, String inputDateFormat) {

		Date dateObject = null;
		String ordinalIndicator = "th";
		String dateWithOridnalIndicator = null;

		// Finding out 'd'/'dd' index in given dateFormat to insert Ordinal Indicator
		int dateFieldIndex = inputDateFormat.lastIndexOf("d");
		
		dateObject = parseToDate(date, inputDateFormat);
		if(dateObject!=null) {
			int day = dateObject.getDate();

			if (!((day > 10) && (day < 19)))
				switch (day % 10) {
				case 1:
					ordinalIndicator = "st";
					break;
				case 2:
					ordinalIndicator = "nd";
					break;
				case 3:
					ordinalIndicator = "rd";
					break;
				default:
					ordinalIndicator = "th";
					break;
				}
		}			
		String dateFormatWithOridnalIndicator = inputDateFormat.substring(0, dateFieldIndex + 1) + "'" + ordinalIndicator
				+ "'" + inputDateFormat.substring(dateFieldIndex + 1);

		try {
			SimpleDateFormat outputFormat = new SimpleDateFormat(dateFormatWithOridnalIndicator);
			dateWithOridnalIndicator = outputFormat.format(dateObject);
		} catch (Exception e) {
			logger.error("error in formating [" + dateObject + "] , format [ " + dateFormatWithOridnalIndicator
					+ " is in valid format] , msg: " + e.getMessage());
			return null;
		}
		return dateWithOridnalIndicator;

	}    	
}

