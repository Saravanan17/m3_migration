package com.acats.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;

public class BrowserConsoleLogs {
	final static Logger logger = Logger.getLogger(BrowserConsoleLogs.class);

	public static Map<String, Object> captureBrowserLogs() throws ParseException{
		Map<String, Object> dataMap = null;
		JavascriptExecutor js = (JavascriptExecutor) DriverManager.getDriver();
		js.executeScript(FileConstants.DIGITAL_DATA);
		js.executeScript(FileConstants.DIGITAL_DATA_JSON);
		LogEntries logs = DriverManager.getDriver().manage().logs().get(FileConstants.BROWSER);
		for (LogEntry entry : logs) {
			if(entry.getMessage().contains(FileConstants.CONSOLE)) {
				logger.info(entry.getMessage());
				String message = entry.getMessage();
				message =message.replaceAll("\\\\", "");
				message = message.substring(17, message.length()-1);
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(message);
				dataMap =jsonToMap(json);
			} 
		}
		return dataMap;
	}

	public static Map<String, Object> jsonToMap(JSONObject json) {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if(json != null) {
			retMap = toMap(json);
		}
		logger.info("Adobe logs---"+retMap);
		return retMap;
	}
	/**
	 * To convert JSON to Map
	 * @param object
	 * @return
	 */
	public static Map<String, Object> toMap(JSONObject object) {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keysItr = object.keySet().iterator();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}
			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	/**
	 * To convert to list
	 * @param array
	 * @return
	 */
	public static List<Object> toList(JSONArray array) {
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < array.size(); i++) {
			Object value = array.get(i);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}
			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}
}
