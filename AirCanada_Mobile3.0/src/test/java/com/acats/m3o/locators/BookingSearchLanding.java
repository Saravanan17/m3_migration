package com.acats.m3o.locators;


import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.acats.core.DriverManager;
import com.acats.core.Lib_MobileGeneric;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

@SuppressWarnings("deprecation")
public class BookingSearchLanding extends Lib_MobileGeneric {

	static Logger logger = Logger.getLogger(BookingSearchLanding.class);
	WebDriverWait wait = new WebDriverWait(driver, 10);

	public BookingSearchLanding() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.getMDriver()), this);
	}

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Book'] | //XCUIElementTypeButton[@label='Réserver']")
	@AndroidFindBy(id = "action_bookings")
	private MobileElement btnBook;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup)[7] | (//android.widget.TextView)[2]")
	@iOSXCUITFindBy(xpath = "bookingSearch_bookSheet_departure_choosePrompt_unselected")
	private MobileElement btnChooseDepart;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup)[7] | (//android.widget.TextView)[2]")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'Allow')]")
	private MobileElement lblAllowAlertTextVerification;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup)[7] | (//android.widget.TextView)[2]")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'Allow")
	private MobileElement btnAllowAlert;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup)[7] | (//android.widget.TextView)[2]")
	@iOSXCUITFindBy(accessibility = "bookingSearch_cityList_searchFieldHint")
	public MobileElement lblBookingSearchCityListSearchFieldHint;


	@AndroidFindBy(id = "permission_message")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'access your location')]")
	public MobileElement popupPermissionMessage;

	@AndroidFindBy(id = "permission_allow_button")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'Allow')]")
	public MobileElement btnLocationPermissionAllow;

	public MobileElement getContinue_date() {
		return continue_date;
	}


	@AndroidFindBy(id = "dates_continue_button")
	@iOSXCUITFindBy(xpath = "//*[@name='bookingSearch_calendar_continueButton']")
	public MobileElement continue_date;

	@iOSXCUITFindAll({
			@iOSXCUITBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeCell/XCUIElementTypeStaticText") })
	public List<MobileElement> listOfAirport;

	public MobileElement getBtnBook() {
		return btnBook;
	}

	public MobileElement getBtnChooseDepart() {
		return btnChooseDepart;
	}

	public MobileElement getLblAllowAlertTextVerification() {
		return lblAllowAlertTextVerification;
	}

	public MobileElement getBtnAllowAlert() {
		return btnAllowAlert;
	}

	public MobileElement getLblBookingSearchCityListSearchFieldHint() {
		return lblBookingSearchCityListSearchFieldHint;
	}

	public void tapOnBookTab() {
		click(btnBook);
	}

	public void verifyBSCityAirportSelectionScreenIsDisplayed() {

		Assert.assertTrue(getLblBookingSearchCityListSearchFieldHint().isDisplayed());
	}

	public void selectAirportCity(String Airport) {
		waitForElementVisibility(getLblBookingSearchCityListSearchFieldHint());
		getLblBookingSearchCityListSearchFieldHint().sendKeys(Airport);
		selectAirport_from_list(Airport, listOfAirport);

	}


	public void selectDate(String depDate,String retDate) throws IOException, Throwable {
		
		String[] dep = depDate.split(" ");
		String DeptDate = dep[3];
		int Deptvalue = Integer.parseInt(DeptDate);
		
		if (retDate.isEmpty() || retDate.equalsIgnoreCase("NIL")) {
			dateSelection(Deptvalue);
			waitForElementVisibility(getContinue_date());
			click(getContinue_date());

		} else {
			String[] ret = retDate.split(" ");
			String RetDate = ret[3];
			int Retvalue = Integer.parseInt(RetDate);
			dateSelection(Deptvalue);
		dateSelection(Retvalue);
		waitForElementVisibility(getContinue_date());
		click(getContinue_date());}

	}

	public void clickOnChooseDepart() {
		btnChooseDepart.click();
	}

	public void verifyGeolocationPopup() {

		wait.until(ExpectedConditions.visibilityOf(popupPermissionMessage));

		Assert.assertTrue(popupPermissionMessage.isDisplayed());

	}

	public void clickAllowInGeoPopup() {

		try {
			Boolean b = btnLocationPermissionAllow.isDisplayed();

			if (b == true) {

				btnLocationPermissionAllow.click();
			}
		} catch (Throwable e) {

		}
	}

	

	

	/*
	 * public void selectAirport_from_list(String airportsName) { for (MobileElement
	 * airport : listOfAirport) { if
	 * (airport.getText().equalsIgnoreCase(airportsName)) { airport.click(); break;
	 * } } }
	 */

//	public String x="//android.view.ViewGroup/android.widget.TextView[@text='";
//	public String clos="\']";
//
//	
//	//*[contains(@text,'All airports')]
//	@AndroidFindBy(xpath = "//*[contains(@text,'All Airports')] | //*[contains(@text,'Tous les aéroports')]")
//	@iOSXCUITFindBy(xpath="//*[contains(@name,'All Airports')] | //*[contains(@name,'Tous les aéroports')]")
//	public MobileElement all_airportstext;
//	
//	public MobileElement getall_airportstext() {
//		return all_airportstext;
//	}
//		
//	@AndroidFindBy(id = "action_schedules")
//	@iOSXCUITFindBy(xpath = "//*[@name='Trips']")
//	public MobileElement profile_Btn;
//	
//
//
//	@iOSXCUITFindBy(accessibility = "bookingSearch_bookSheet_arrivalAirport_destinationChosen_unselected")
//	public MobileElement arrival_selected;
//
//	@iOSXCUITFindBy(id = "bookingSearch_cityList_sheetTitle_to")
//	public MobileElement booking_Sheet_Title_Flying_to;
//  
//	
//
//	@AndroidFindBy(id = "booking_summary_select_passengers_button")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='bookingSearch_bookSheet_passenger_single'] | //XCUIElementTypeButton[@name='bookingSearch_bookSheet_passenger_plural']")
//	public MobileElement passengerScreen_btn;
//
//	@AndroidFindBy(accessibility = "booking search notch")
//	@iOSXCUITFindBy(id = "bookingSearch_airports_notch_collapsed")
//	public MobileElement notch_btn;
//

//	
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'origin_button_layout')] | //*[contains(@resource-id,'selected_origin_text_view')]")
//	@iOSXCUITFindBy(xpath="//*[contains(@name,'bookingSearch_bookSheet_departure')]")
//	public MobileElement selected_origin_btn;
//	
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'origin_button_layout')] | //*[contains(@resource-id,'selected_origin_text_view')]")
//	@iOSXCUITFindBy(xpath="//*[contains(@name,'bookingSearch_bookSheet_arrival')]")
//	public MobileElement selected_arrival_btn;
//	
//	@AndroidFindBy(id = "selected_destination_text_view")
//	//@iOSXCUITFindBy(id="bookingSearch_bookSheet_departure_choosePrompt_unselected")
//	@iOSXCUITFindBy(xpath="//*[contains(@name,'bookingSearch_bookSheet_arrival')]")
//	public MobileElement selected_destination_btn;
//
//	public MobileElement getSelected_origin_btn() {
//		return selected_origin_btn;
//	}
//
//	public MobileElement getSelected_destination_btn() {
//		return selected_destination_btn;
//	}

//
//	@iOSXCUITFindBy(accessibility = "bookingSearch_bookSheet_searchField_clearButton")
//	public MobileElement cross_btn;
//
//	@AndroidFindBy(id ="destination_image_view_active")
//	// newly added by siva
//	//@iOSXCUITFindBy(id="bookingSearch_bookSheet_arrival_choosePrompt_selected")
//	@iOSXCUITFindBy(xpath ="//*[@name='bookingSearch_bookSheet_arrival_choosePrompt_selected'] | //*[@name='bookingSearch_bookSheet_arrivalAirport_destinationChosen_unselected']")
//	public MobileElement arrival_btn;
//	// changed by gaurav for user story M3- 500
//	@AndroidFindBy(id = "destination_image_view_inactive")
//	@iOSXCUITFindBy(id="bookingSearch_bookSheet_arrival_arrival")
//	public MobileElement Active_arrival_btn;
//	
//	
//	
//	@AndroidFindBy(id = "direction_button")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='one way divider icon']")
//	public MobileElement direction_indicator;
//
//	//
//	@AndroidFindBy(id = "departure_image_view")
//	@iOSXCUITFindBy(xpath = "//*[@name='bookingSearch_bookSheet_departure_choosePrompt_selected'] | //*[@name='bookingSearch_bookSheet_departureAirport_originChosen_unselected']")
//	public MobileElement departureExpanded;
//	
//	@AndroidFindBy(id = "booking_sheet_title_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_cityList_sheetTitle_to")
//	public MobileElement bookingSearch_cityList_sheetTitle_to;
//	
//	@AndroidFindBy(id = "booking_sheet_title_textview")
//	@iOSXCUITFindBy(accessibility = "bookingSearch_calendar_sheetTitle_depart")
//	public MobileElement bookingSearch_calendar_sheetTitle_depart;
//	
//	//bookingSearch_calendar_sheetTitle_depart
//
//	@AndroidFindBy(id = "booking_sheet_title_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_bookSheet_sheetTitle")
//	public MobileElement bookingSearch_bookSheet_sheetTitle;
//	
//	
//	@AndroidFindBy(id = "return_hint_text_view")
//	@iOSXCUITFindBy(accessibility = "bookingSearch_calendar_returnDateHint_unselected")
//	public MobileElement bookingSearch_calendar_returnDateHint;
//	
//	
//	@AndroidFindBy(id = "selected_origin_text_view")
//	//@iOSXCUITFindBy(xpath ="//*[@name='bookingSearch_bookSheet_departure_choosePrompt_unselected'] | //*[@name='bookingSearch_bookSheet_departureAirport_originChosen_unselected']")  
//	@iOSXCUITFindBy(xpath="//*[contains(@name,'bookingSearch_bookSheet_departure')]")
//	public MobileElement selected_origin_text_view;
//	
//	@AndroidFindBy(id = "destination_prompt_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_bookSheet_arrival_choosePrompt_selected")
//	public MobileElement destination_prompt_textview;
//	
//	@AndroidFindBy(id = "destination_prompt_textview")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'bookingSearch_bookSheet_arrival')]")
//	public MobileElement destination_text;
//	
//	@AndroidFindBy(id = "booking_sheet_title_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_cityList_sheetTitle_from")
//	public MobileElement booking_Sheet_Title_Flying_from;
//
//	@AndroidFindBy(id = "booking_sheet_title_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_bookSheet_sheetTitle")
//	public MobileElement booking_Sheet_Title;
//
//	@iOSXCUITFindBy(id = "booking_sheet_title_textview")
//	public MobileElement calendar_roundTrip_title;
//
//	@AndroidFindBy(id = "od_search_airports_edittext")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'cityList_searchFieldHint')]")
//	public MobileElement searchAirports_Edittext;
//
//	@AndroidFindBy(id = "search_airplane_imageview")
//	@iOSXCUITFindBy(xpath = "//*[@name='search-bar-plane-icon']")
//	public MobileElement searchAirplane_Imageicon;
//
//	@AndroidFindBy(id = "search_icon_imageview")
//
//	@iOSXCUITFindBy(id="search-bar-search-icon")
//
//	public MobileElement searchAirports_magnifyGlass;
//
//	@AndroidFindBy(id = "airport_city_textview")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeCell[@name='AirportCell'][1]/XCUIElementTypeStaticText[2]")
//	public MobileElement airport_Citytext;
//
//	@AndroidFindBy(id = "airport_name_textview")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeCell[@name='AirportCell'][1]/XCUIElementTypeStaticText[1]")
//	public MobileElement airport_Nametext;
//
//	@AndroidFindBy(id = "airport_code_textview")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeCell[@name='AirportCell'][1]/XCUIElementTypeStaticText[3]")
//	public MobileElement airport_Codetext;
//
//	@AndroidFindBy(id = "no_results_found_text_view")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'No results')] | //*[contains(@name,'Aucun résultat')]")
//	//*[contains(@name,'No results, try another city or airport')] 
//	
//	public MobileElement airport_Errortext;
//
//	@AndroidFindBy(id = "airport_code_textview")
//	@iOSXCUITFindBy(xpath = "//*[@name='airports_tableView_cell']")
//	public MobileElement airport_code__textview;
//
//	@AndroidFindBy(id = "airport_city_textview")
//	@iOSXCUITFindBy(xpath = "//*[@name='airports_tableView_cell']")
//	public MobileElement airport_city_textview;
//
//	@AndroidFindBy(id = "airport_name_textview")
//	@iOSXCUITFindBy(xpath = "//*[@name='airports_tableView_cell']")
//	public MobileElement airport_name_textview;
//
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='airport_search_container']/XCUIElementTypeOther[1]") // changes
//	public MobileElement change_depart;
//
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='airport_search_container']/XCUIElementTypeOther[2]") // changes
//	public MobileElement change_arrival;
//
//	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='airport_search_container']/XCUIElementTypeOther[1]/XCUIElementTypeOther[1])/XCUIElementTypeStaticText[1]") // changes
//	public MobileElement change_depart_getText;
//
//	@AndroidFindBy(id = "depart_hint_text_view")
//	@iOSXCUITFindBy(accessibility = "bookingSearch_calendar_departDateHint")
//	public MobileElement depart_hint_text_view;
//
//	@AndroidFindBy(id = "booking_search_title_textview")
//	@iOSXCUITFindBy(id = "bookingSearch_bookLanding_pageTitle")
//	public MobileElement booking_search_title_textview;
//
//	@AndroidFindBy(xpath = "//android.widget.Button[@text='Confirm']")
//	// @iOSXCUITFindBy(xpath="//*[@name='Select departure date']")
//	public MobileElement web_Confirm;
//
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeAlert[@name='Travelling with children']")
//	@AndroidFindBy(xpath = "//*[contains(@label,'Travelling with')] | //*[contains(@label,'Voyager avec')]")
//	public MobileElement ErrorTitle;
//	
//	
//
//	@AndroidFindBy(id = "detail_header_text_view")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeCell[@name=\"BookTabTitleCell\"]")
//	public MobileElement BS_Summary_Title;
//
//	@AndroidFindBy(id = "booking_for_more_text_view")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Booking for more than 10 people?\"]")
//	public MobileElement BS_Summary_More_ten_passengers_Link;
//
//	@AndroidFindBy(id = "information_icon_imageView")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeImage[@name=\"helpImage\"]")
//	public MobileElement BS_Summary_More_ten_passengers_Icon;
//
//	@AndroidFindBy(id = "com.sec.android.app.sbrowser:id/url_bar_text")
//	@iOSXCUITFindBy(xpath = "//*[@value='‎aircanada.com']")
//	public MobileElement More_ten_Passenger_separate_browserlink;
//
//	@AndroidFindBy(id = "back_image_button")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[contains(@name,\"Back\")] | //XCUIElementTypeButton[@name=\"Précédent\"]")
//	public MobileElement back_button;
//
//	@AndroidFindBy(id = "origin_button")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,\"airport_search_container\")]/XCUIElementTypeOther[1]")
//	public MobileElement origin_City;
//
//	@AndroidFindBy(id = "permission_icon")
//	public MobileElement locationPermissionIcon;
//

//
//	@AndroidFindBy(id = "permission_deny_button")
//	public MobileElement locationPermissionDeny;
//
//	@AndroidFindBy(id = "permission_message")
//	public MobileElement Permission_Message_Txt;
//	
//	@AndroidFindBy(id = "origin_prompt_textview")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'bookingSearch_bookSheet_departure')]")
//	public MobileElement origin_prompt_textview;
//	
//	@AndroidFindBy(id = "checked_icon_imageView_1")
//	public MobileElement Green_Checkmark;
//
//	@AndroidFindBy(id = "checked_airport_layout_1")
//	public MobileElement Green_Checkmark_Layout;
//
//	@AndroidFindBy(id = "geo_icon_imageView_1")
//	public MobileElement Geo_Location_Icon;
//
//	@AndroidFindBy(id = "od_search_results_recyclerview")
//	public MobileElement OriginSearch_result;
//
//	@iOSXCUITFindBy(id = "airport-search-result-checkmark")
//	@AndroidFindBy(xpath = "//*[contains(@content-desc,'selectedAirportTest')]")
//	public MobileElement GreenCheckmarksyl;
//
//	@iOSXCUITFindBy(xpath = "(//*[@name=\"AirportSearchResultCell\"])[1]/XCUIElementTypeStaticText[4]")
//	//@AndroidFindBy(xpath = "//*[contains(@content-desc,'selectedAirportTest')]/following::*[3]")
//	@AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3] | //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup")
//	
//	public MobileElement GreenCheckmarkname;
//
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'airport-search-result-recent')]")
//	@AndroidFindBy(xpath = "//*[contains(@content-desc,'recentAirportTest')]")
//	public MobileElement recentchkmarksyl;
//
//	// @AndroidFindBy(id = "depart_hint_text_view")
//	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"YYZ\"])[2] ")
//	public MobileElement depart_Airport;
//
//	@iOSXCUITFindBy(id = "Would you like to start a new booking search? If you do, you'll lose your current booking progress.")
//	public MobileElement RtnFlightDateTextView;
//
//	@iOSXCUITFindBy(accessibility = "bookingSearch_calendar_departDate")
//	@AndroidFindBy(id = "include_selected_dates_one_way")
//	public MobileElement Dateselect;
//
//	@AndroidFindBy(id = "destination_button")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,\"airport_search_container\")]/XCUIElementTypeOther[2]")
//	public MobileElement destination_City;
//
//	@AndroidFindBy(id = "//*[@content-desc='This airport was recently searched']")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,\"airport-search-result-recent\")] | //*[@content-desc=\"Cet aéroport a été récemment recherché\"]")
//	public MobileElement RecentCheckmarksyl;
//
//	@AndroidFindBy(xpath = "//*[@content-desc='This airport is near you']")
//	public MobileElement GeoLoacatedsyl;
//
//	@iOSXCUITFindBy(xpath = "(//*[@name=\"AirportSearchResultCell\"])[2]/XCUIElementTypeStaticText[2]")
//	@AndroidFindBy(xpath = "//*[contains(@content-desc,'recentAirportTest')]/following::*[3]")
//	public MobileElement recentchkmarkname;
//
//	@iOSXCUITFindBy(xpath = "(//*[@name=\"AirportSearchResultCell\"])[1]/XCUIElementTypeStaticText[4]")
//	@AndroidFindBy(xpath = "//*[contains(@content-desc,'geoLocatedAirportTest')]")
//	public MobileElement GioCheckmarksyl;
//
//	@iOSXCUITFindBy(xpath = "(//*[@name=\"AirportSearchResultCell\"])[1]/XCUIElementTypeStaticText[4]")
//	@AndroidFindBy(xpath = "//*[contains(@content-desc,'geoLocatedAirportTest')]/following::*[3]")
//	public MobileElement GioCheckmarkname;
//	
//	@AndroidFindBy(id = "airport_text_view")
//	@iOSXCUITFindBy(accessibility  = "flightSearchResults_resultsList_departureCity")
//	public MobileElement SRFromtxt;
//
//	@AndroidFindBy(xpath = "//*[contains(@text,'New booking')] | //*[contains(@text,'Nouvelle recherche')]")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'new booking')] | //*[contains(@name,'nouvelle recherche')]")
//	public MobileElement btn_New_booking_msg;
//
//	@AndroidFindBy(xpath = "//*[contains(@text,'CANCEL')] | //*[contains(@text,'ANNULER')]")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'Cancel')] | //*[contains(@name,'Annuler')]")
//	public MobileElement btn_Cancel;
//
//	@AndroidFindBy(id = "Back")
//	public MobileElement Back;
//	
//	
//	public MobileElement getActive_arrival_btn() {
//		return Active_arrival_btn;
//	}
//
//	public MobileElement getDepartureExpanded() {
//		return departureExpanded;
//	}
//
//	public MobileElement getCalendar_roundTrip_title() {
//		return calendar_roundTrip_title;
//	}
//
//	public MobileElement getCross_btn() {
//		return cross_btn;
//	}
//
//	public MobileElement getPermission_message_popup() {
//		return permission_message_popup;
//	}
//
//	public MobileElement getBooking_Sheet_Title_Flying_from() {
//		return booking_Sheet_Title_Flying_from;
//	}
//
//	public MobileElement getChange_depart_getText() {
//		return change_depart_getText;
//	}
//
//	public MobileElement getBack_button() {
//		return back_button;
//	}
//
//	public MobileElement getOrigin_City() {
//		return origin_City;
//	}
//
//	public MobileElement getDepart_Airport() {
//		return depart_Airport;
//	}
//
//	public MobileElement getRtnFlightDateTextView() {
//		return RtnFlightDateTextView;
//	}
//
//	public MobileElement getDateselect() {
//		return Dateselect;
//	}
//
//	public MobileElement getDestination_City() {
//		return destination_City;
//	}
//
//	public MobileElement getRecentCheckmarksyl() {
//		return RecentCheckmarksyl;
//	}
//
//	public MobileElement getGeoLoacatedsyl() {
//		return GeoLoacatedsyl;
//	}
//
//	public MobileElement getGioCheckmarksyl() {
//		return GioCheckmarksyl;
//	}
//
//	public MobileElement getGioCheckmarkname() {
//		return GioCheckmarkname;
//	}
//
//	public MobileElement getGreenCheckmarksyl() {
//		return GreenCheckmarksyl;
//	}
//
//	public MobileElement getGreenCheckmarkname() {
//		return GreenCheckmarkname;
//	}
//
//	public MobileElement getArrival_selected() {
//		return arrival_selected;
//	}
//
//	public MobileElement getRecentchkmarksyl() {
//		return recentchkmarksyl;
//	}
//
//	public MobileElement getRecentchkmarkname() {
//		return recentchkmarkname;
//	}
//
//	public MobileElement getLocationPermissionIcon() {
//		return locationPermissionIcon;
//	}
//
//	public MobileElement getLocationPermissionAllow() {
//		return locationPermissionAllow;
//	}
//
//	public MobileElement getLocationPermissionDeny() {
//		return locationPermissionDeny;
//	}
//
//	public MobileElement getPermission_Message_Txt() {
//		return Permission_Message_Txt;
//	}
//
//	public MobileElement getGreen_Checkmark() {
//		return Green_Checkmark;
//	}
//
//	public MobileElement getGreen_Checkmark_Layout() {
//		return Green_Checkmark_Layout;
//	}
//
//	public MobileElement getGeo_Location_Icon() {
//		return Geo_Location_Icon;
//	}
//
//	public MobileElement getOriginSearch_result() {
//		return OriginSearch_result;
//	}
//
//	public MobileElement getBS_Summary_Title() {
//		return BS_Summary_Title;
//	}
//
//	public MobileElement getBS_Summary_More_ten_passengers_Link() {
//		return BS_Summary_More_ten_passengers_Link;
//	}
//
//	public MobileElement getBS_Summary_More_ten_passengers_Icon() {
//		return BS_Summary_More_ten_passengers_Icon;
//	}
//
//	public MobileElement getMore_ten_Passenger_separate_browserlink() {
//		return More_ten_Passenger_separate_browserlink;
//	}
//
//	public MobileElement getErrorTitle() {
//		return ErrorTitle;
//	}
//
//	public MobileElement getBooking_search_title_textview() {
//		return booking_search_title_textview;
//	}
//
//	public MobileElement getWeb_Confirm() {
//		return web_Confirm;
//	}
//
//	public MobileElement getAirport_name_textview() {
//		return airport_name_textview;
//	}
//
//	public MobileElement getChange_depart() {
//		return change_depart;
//	}
//
//	public MobileElement getChange_arrival() {
//		return change_arrival;
//	}
//
//	public MobileElement getDepart_hint_text_view() {
//		return depart_hint_text_view;
//	}
//
//	public MobileElement getAirport_city_textview() {
//		return airport_city_textview;
//	}
//
//	public MobileElement getAirport_code__textview() {
//		return airport_code__textview;
//	}
//
//	public MobileElement getAirport_Errortext() {
//		return airport_Errortext;
//	}
//
//	public MobileElement getBook_Btn() {
//		return Book_Btn;
//	}
//
//	public MobileElement getAirport_Citytext() {
//		return airport_Citytext;
//	}
//
//	public MobileElement getAirport_Nametext() {
//		return airport_Nametext;
//	}
//
//	public MobileElement getAirport_Codetext() {
//		return airport_Codetext;
//	}
//	
//	public MobileElement getProfile_Btn() {
//		return profile_Btn;
//	}
//
//	public MobileElement getPassengerScreen_btn() {
//		return passengerScreen_btn;
//	}
//
//	public MobileElement getNotch_btn() {
//		return notch_btn;
//	}
//
//	public MobileElement getChooseDepart_btn() {
//		return chooseDepart_btn;
//	}
//
//	public MobileElement getBooking_Sheet_Title_Flying_to() {
//		return booking_Sheet_Title_Flying_to;
//	}
//
//	public MobileElement getArrival_btn() {
//		return arrival_btn;
//	}
//
//	public MobileElement getDirection_indicator() {
//		return direction_indicator;
//	}
//
//	public MobileElement getBooking_Sheet_Title() {
//		return booking_Sheet_Title;
//	}
//
//	public MobileElement getSearchAirports_Edittext() {
//		return searchAirports_Edittext;
//	}
//	
//	
//
//	public MobileElement getSearchAirplane_Imageicon() {
//		return searchAirplane_Imageicon;
//	}
//
//	public MobileElement getSearchAirports_magnifyGlass() {
//		return searchAirports_magnifyGlass;
//	}
//	
//	public MobileElement getSRFromtxt() {
//		return SRFromtxt;
//	}
//	
//	public MobileElement getBtn_New_booking() {
//		return btn_New_booking_msg;
//	}
//	
//	public MobileElement getBtn_Cancel() {
//		return btn_Cancel;
//	}
//	
//	public MobileElement getBack() {
//		return Back;
//	}
//
//	
//	public MobileElement getchoosedepart() {
//		return chooseDepart;
//	}
//	
//	@AndroidFindBy(accessibility = "Choose departure airport")
//	@iOSXCUITFindBy(id="bookingSearch_bookSheet_departure_choosePrompt_unselected")
//	public MobileElement chooseDepart;
//	
//	@AndroidFindBy(id = "destination_image_view_inactive")
////	@iOSXCUITFindBy(id="bookingSearch_bookSheet_departure_choosePrompt_unselected")
//	public MobileElement Arrivalinactive;
//
//	
//	public MobileElement getArrival_btnInactive() {
//		return Arrivalinactive;
//	}
//	@AndroidFindBy(id = "bottom_sheet_header_layout")
////	@iOSXCUITFindBy(id="bookingSearch_bookSheet_departure_choosePrompt_unselected")
//	public MobileElement bottom_sheet_header_layout;
//
//	
//	public MobileElement getbottom_sheet_header_layout() {
//		return bottom_sheet_header_layout;
//	}
//	
//	/////////////////////////////
//	@AndroidFindBy(id = "boarding_pass_quick_access_image_view")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_button;
//
//	
//	public MobileElement getquick_access_button() {
//		return quick_access_button;
//	}
//	
//	@AndroidFindBy(id = "boarding_pass_quick_access_flight_number_text_view")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_flight_number;
//
//	
//	public MobileElement getquick_access_flight_number() {
//		return quick_access_flight_number;
//	}
//	
//	@AndroidFindBy(id = "boarding_pass_quick_access_overall_status_text_view")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_flight_status;
//
//	
//	public MobileElement getquick_access_flight_status() {
//		return quick_access_flight_status;
//	}
//	
//	@AndroidFindBy(id = "NULL")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_origin_airport;
//
//	
//	public MobileElement getquick_access_origin_airport() {
//		return quick_access_origin_airport;
//	}
//	
//	@AndroidFindBy(id = "NULL")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_destination_airport;
//
//	
//	public MobileElement getquick_access_destination_airport() {
//		return quick_access_destination_airport;
//	}
//	
//	@AndroidFindBy(id = "boarding_pass_quick_access_route_text_view")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_airport_route;
//
//	
//	public MobileElement getquick_access_airport_route() {
//		return quick_access_airport_route;
//	}
//		
//	@AndroidFindBy(xpath = "//android.widget.TextView")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_gate_number;
//
//	
//	public MobileElement getquick_access_gate_number() {
//		return quick_access_gate_number;
//	}
//	
//	@AndroidFindBy(xpath = "//android.widget.TextSwitcher[@class= 'android.widget.TextView']")
//	//android.widget.TextView)[2]
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_boarding_time;
//
//	
//	public MobileElement getquick_access_boarding_time() {
//		return quick_access_boarding_time;
//	}
//	
//	@AndroidFindBy(id = "num_boarding_passes_text_view")
//	@iOSXCUITFindBy(id="NULL")
//	public MobileElement quick_access_boarding_pass_number;
//
//	
//	public MobileElement getquick_access_boarding_pass_number() {
//		return quick_access_boarding_pass_number;
//	}
//	
//	@AndroidFindBy(id = "NULL")
//	@iOSXCUITFindBy(id="bookingSearch_calendar_departDate")
//	public MobileElement departdate_afterselection;
//
//	
//	public MobileElement getdepartdate_afterselection() {
//		return departdate_afterselection;
//	}
//	
//	@AndroidFindBy(id = "NULL")
//	@iOSXCUITFindBy(id="bookingSearch_calendar_returnDate")
//	public MobileElement returndate_afterselection;
//
//	
//	public MobileElement getreturndate_afterselection() {
//		return returndate_afterselection;
//	}
}
