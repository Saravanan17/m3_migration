
package com.acats.m3o.locators;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.acats.core.DriverManager;


import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class TripsScreen  {
	public TripsScreen() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.getMDriver()), this);

	}
	
	@AndroidFindBy(id="tab_header_text_view")
	@iOSXCUITFindBy(accessibility="trips_tripsListEmpty_header")
	private MobileElement Lbltripsheader;
	
	public MobileElement getLblTripsheader() {
		return Lbltripsheader;
	}

	public void verifyDefaultTripScreenIsDisplayed() {
		Assert.assertTrue(getLblTripsheader().isDisplayed());
	}

//	//@AndroidFindBy(xpath = "(//*[contains(@text,'Trips')] | //*[contains(@text,'Voyages')])[2]")
//	@AndroidFindBy(id="action_trips")
//	@iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name='Trips']|//XCUIElementTypeButton[@name='Voyages']")
//	public MobileElement trips_temp_button;
//
//	public MobileElement getTrips_temp_button() {
//		return trips_temp_button;
//	}
//	@AndroidFindBy(id = "trips_add_reservation_button")
//	//@iOSXCUITFindBy(xpath = "//*[@name='Add existing reservation'] | //XCUIElementTypeOther[@name='Ajouter une réservation existante']")
//	@iOSXCUITFindBy(id = "trips_tripsList_tripBlockButton")
//	public MobileElement add_reservation_temp_button;
//	
//	@AndroidFindBy(id="tab_header_text_view")
//	@iOSXCUITFindBy(id="trips_tripsList_header")
//	public MobileElement totalTrips;
//	
//
//	public MobileElement getadd_reservation_temp_button() {
//		return add_reservation_temp_button;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'text_view')]")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='Add existing reservation']")
//	public MobileElement boarding_pass_header_text_view;
//
//	public MobileElement getBoarding_pass_header_text_view() {
//		return boarding_pass_header_text_view;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'close_button')]")
//	@iOSXCUITFindBy(id = "boardingPass_addBoardingPass_closeButton")
//	public MobileElement boarding_pass_sheet_close_button;
//	
//	public MobileElement getboarding_pass_sheet_close_button() {
//		return boarding_pass_sheet_close_button;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'booking_reference_edit_text')]")
//	@iOSXCUITFindBy(xpath = "//*[@name='boardingPass_addBoardingPass_bookingReference_labelUnselected'] | (//XCUIElementTypeCell[@name='DetailsInputStringCell'])[1]/XCUIElementTypeTextField")
//	public MobileElement boarding_pass_booking_reference_edit_text;
//	
//	@AndroidFindBy(id = "boarding_pass_booking_reference_edit_text")
//	public MobileElement boardingpass_trip_edit;
//	
//	
//	
//	
//	@AndroidFindBy(id = "trips_add_trip_text_view")
//	@iOSXCUITFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeOther")
//	public MobileElement add_Existing_screen_header;
//	
//	public MobileElement getboarding_pass_booking_reference_edit_text() {
//		return boarding_pass_booking_reference_edit_text;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'last_name_edit_text')]")
//	//@iOSXCUITFindBy(id = "boardingPass_addBoardingPass_lastName_labelUnselected")
//	@iOSXCUITFindBy(xpath = "//*[@name='boardingPass_addBoardingPass_lastName_labelUnselected'] | (//XCUIElementTypeCell[@name='DetailsInputStringCell'])[2]/XCUIElementTypeTextField")
//	public MobileElement boarding_pass_last_name_edit_text;
//
//	public MobileElement getboarding_pass_last_name_edit_text() {
//		return boarding_pass_last_name_edit_text;
//	}
//
//	@AndroidFindBy(id = "continue_button")
//	@iOSXCUITFindBy(id = "trips_tripsList_retrieveButton")
//	public MobileElement trips_add_button;
//
//	public MobileElement gettrips_add_button() {
//		return trips_add_button;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'clear_booking_reference_button')]")
//	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Clear text field'])[1]")
//	public MobileElement clear_booking_reference_button;
//
//	public MobileElement getclear_booking_reference_button() {
//		return clear_booking_reference_button;
//	}
//
//	@AndroidFindBy(xpath = "//*[contains(@resource-id,'clear_last_name_button')]")
//	@iOSXCUITFindBy(xpath = "(//XCUIElementTypeOther[@name='Clear text field'])[2] ")
//	public MobileElement clear_last_name_button;
//
//	public MobileElement getclear_last_name_button() {
//		return clear_last_name_button;
//	}
//	@AndroidFindBy(id = "textinput_error")
//	@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_bookingReference_errorFormatUnselected")
//		public MobileElement tripsbPassErrFormat;
//	
//	
//	@AndroidFindBy(id = "message")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'Booking reference not found')]")
//	public MobileElement errorPopup;
//	
//	@AndroidFindBy(id = "button1")
//	@iOSXCUITFindBy(xpath = "//*[contains(@name,'OK')]")
//	public MobileElement button1;
//	
//	public MobileElement gettripsbPassErrFormat() {
//		return tripsbPassErrFormat;
//	}
//	//@AndroidFindBy(id = "booking_reference_error_image_view")
//		@AndroidFindBy(id = "trips_booking_reference_error_image_view")
//		@iOSXCUITFindBy(xpath = "(//XCUIElementTypeImage[@name='iconAlert'])[1]")
//		public MobileElement booking_reference_error_image_view;
//
//		public MobileElement getbooking_reference_error_image_view() {
//			return booking_reference_error_image_view;
//		}
//		
//		@AndroidFindBy(id = "textinput_error")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_lastName_errorFormatUnselected")
//		public MobileElement triplastNameErrFormat;
//		
//		public MobileElement gettriplastNameErrFormat() {
//			return triplastNameErrFormat;
//		}
//		
//		//@AndroidFindBy(id = "last_name_error_image_view")
//		@AndroidFindBy(id = "trips_last_name_error_image_view")
//		@iOSXCUITFindBy(id = "(//XCUIElementTypeImage[@name='iconAlert'])[2]")
//		public MobileElement last_name_error_image_view;
//
//		public MobileElement getlast_name_error_image_view() {
//			return last_name_error_image_view;
//		}
//		@AndroidFindBy(id = "textinput_error")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_bookingReference_errorEmptyUnselected")
//		public MobileElement brNumEmptyErr;
//		
//		public MobileElement getbrNumEmptyErr() {
//			return brNumEmptyErr;
//		}
//		
//		@AndroidFindBy(id = "textinput_error")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_lastName_errorEmptyUnselected")
//		public MobileElement LNameEmptyErr;
//		
//		
//		public MobileElement getLNameEmptyErr() {
//			return LNameEmptyErr;
//		}
//		
//
//		//retrieve_info_response_text_view
//		@AndroidFindBy(id = "back_layout_button")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_lastName_errorEmptyUnselected")
//		public MobileElement retrieve_info_response_text_view;
//		
//		
//		public MobileElement getretrieve_info_response_text_view() {
//			return retrieve_info_response_text_view;
//		}
//		
//		@AndroidFindBy(id = "dismiss_button")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_lastName_errorEmptyUnselected")
//		public MobileElement dismiss_button;
//		
//		
//		public MobileElement getdismiss_button() {
//			return dismiss_button;
//		}
//		@AndroidFindBy(id = "android:id/button1")
//		@iOSXCUITFindBy(xpath = "//*[@name='OK']")
//		public MobileElement connection_timeout;
//		
//		
//		public MobileElement getconnection_timeout() {
//			return connection_timeout;
//		}
//		
//		@AndroidFindBy(id = "tab_header_text_view")
//		@iOSXCUITFindBy(accessibility = "boardingPass_addBoardingPass_lastName_errorEmptyUnselected")
//		public MobileElement trips_header_text_view;
//		
//		
//		public MobileElement gettrips_header_text_view() {
//			return trips_header_text_view;
//		}
//		
//		@AndroidFindBy(id = "actual_response_text_view")
//		@iOSXCUITFindBy(accessibility = "<NULL>")
//		public MobileElement actual_response_text_view;
//		
//		public MobileElement getactual_response_text_view() {
//			return actual_response_text_view;
//		}
//		@AndroidFindBy(id = "info_loading_progress_bar")
//		@iOSXCUITFindBy(accessibility = "<NULL>")
//		public MobileElement info_loading_progress_bar;
//		
//		public MobileElement getinfo_loading_progress_bar() {
//			return info_loading_progress_bar;
//		}
//		
//		@AndroidFindBy(id = "trips_header_text_view")
//        @iOSXCUITFindBy(id = "trips_tripsList_header")
//        public MobileElement trips_header_text;
//        
//        public MobileElement gettrips_header_text() {
//               return trips_header_text;
//        }
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_layout_countdown_text_view')]")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_countdown_twoToTenDays'])[1]")
//        public MobileElement trip_countdown_text;
//        
//        public MobileElement gettrip_countdown_text() {
//                        return trip_countdown_text;
//        }
//        @AndroidFindBy(id = "trips_refresh_time_text_view")
//        @iOSXCUITFindBy(id = "flightStatus_resultsList_lastUpdated_lessThanMinute")
//        public MobileElement trips_updated_time;
//        
//
//        public MobileElement gettrips_updated_time() {
//                        return trips_updated_time;
//        }
//        @AndroidFindBy(id = "trips_booking_reference_edit_text")
//        @iOSXCUITFindBy(xpath = "//*[@name='boardingPass_addBoardingPass_bookingReference_labelUnselected'] | (//XCUIElementTypeCell[@name='DetailsInputStringCell'])[1]/XCUIElementTypeTextField")
//        public MobileElement bookingref_editText;
//
//        public MobileElement getbookingref_editText() {
//                        return bookingref_editText;
//        }
//
//        
//        @AndroidFindBy(id = "trips_last_name_edit_text")
//        @iOSXCUITFindBy(xpath = "//*[@name='boardingPass_addBoardingPass_lastName_labelUnselected'] | (//XCUIElementTypeCell[@name='DetailsInputStringCell'])[2]/XCUIElementTypeTextField")
//        public MobileElement lastname_editText;
//
//        public MobileElement getlastname_editText() {
//                        return lastname_editText;
//        }
//
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_block_image_view')]")
//        @iOSXCUITFindBy(id = "<NULL>")
//        public MobileElement trip_block_view;
//        
//        public MobileElement gettrip_block_view() {
//                        return trip_block_view;
//        }
//        
//        @AndroidFindBy(id="trip_block_image_view")
//        public MobileElement trip_blckView;
//        
//        public String trp_blckView = "trip_block_image_view";
//        
//        @AndroidFindBy(id = "dismiss_button")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='trips_tripsList_retrieveButton']")
//        public MobileElement response_dismiss_button;
//
//        public MobileElement getresponse_dismiss_button() {
//                        return response_dismiss_button;
//        }
//
//        @AndroidFindBy(id = "trips_add_reservation_button")
//        @iOSXCUITFindBy(id = "trips_tripsList_tripBlockButton")
//        public MobileElement add_existing_reservation;
//        
//
//        public MobileElement getadd_existing_reservation() {
//                        return add_existing_reservation;
//        }
//
//        @AndroidFindBy(id = "trips_layout_one_stop_image_view")
//        @iOSXCUITFindBy(id = "<NULL>")
//        public MobileElement one_stop_view;
//        
//
//        public MobileElement getone_stop_view() {
//                        return one_stop_view;
//        }
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_layout_bound_departure_date_text_view')]")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_boundDepartureDate'])[1]")
//        //name = trips_tripsList_tripBlock_boundDepartureDate
//        public MobileElement trip_departure_date;
//        
//        public MobileElement gettrip_departure_date() {
//                        return trip_departure_date;
//        }
//
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_layout_arrival_time_text_view')]")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_boundArrivalTime'])[1]")
//        //name = trips_tripsList_tripBlock_boundArrivalTime
//        public MobileElement trip_arrival_time;
//        
//        public MobileElement gettrip_arrival_time() {
//                        return trip_arrival_time;
//        }
//
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_layout_departure_time_text_view')]")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_boundDepartureTime'])[1]")
//        //name = trips_tripsList_tripBlock_boundDepartureTime
//        public MobileElement trip_departure_time;
//        
//        public MobileElement gettrip_departure_time() {
//                        return trip_departure_time;
//        }
//
//
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_layout_bound_route_text_view')]")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_boundRoute'])[1]")
//        public MobileElement trip_bound_route;
//        
//        public MobileElement gettrip_bound_route() {
//                        return trip_bound_route;
//        }
//
//        @AndroidFindBy(id = "trips_layout_two_stop_first_image_view")
//        @iOSXCUITFindBy(id = "<NULL>")
//        public MobileElement two_stop_view;
//        
//
//        public MobileElement gettwo_stop_view() {
//                        return two_stop_view;
//        }
//        @AndroidFindBy(id = "trips_layout_multi_stop_text_view")
//        @iOSXCUITFindBy(id = "<NULL>")
//        public MobileElement three_stop_view;
//        
//
//        public MobileElement getthree_stop_view() {
//                        return three_stop_view;
//        }
//
//        @AndroidFindBy(id = "trip_block_booking_reference_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_bookingReference'][1]")
//        public MobileElement trip_block_booking_reference;
//        
//        public MobileElement gettrip_block_booking_reference() {
//                        return trip_block_booking_reference;
//        }
//
//
//        
//        @AndroidFindBy(id = "trip_block_destination_text_view")
//        @iOSXCUITFindBy(id = "trips_tripsList_header")
//        public MobileElement trips_destination_header;
//        
//
//        public MobileElement gettrips_destination_header() {
//                        return trips_destination_header;
//        }
//
//        
//        @AndroidFindBy(id = "action_trips")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Trips'] | //XCUIElementTypeButton[@name='trips_tab_identifier']")
//        public MobileElement trips_tab_button;
//        
//
//        public MobileElement getTrips_tab_button() {
//                        return trips_tab_button;
//        }
//
//        @AndroidFindBy(id = "trips_recycler_view")
//        @iOSXCUITFindBy(id = "<NULL>")
//        public MobileElement trips_recycler_view;
//        
//
//        public MobileElement gettrips_recycler_view() {
//                        return trips_recycler_view;
//        }
//
//        @AndroidFindBy(id = "trip_detail_booking_reference_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripItinerary_bookingReference']")
//        public MobileElement booking_reference_display;
//        
//        public MobileElement getbooking_reference_display() {
//                        return booking_reference_display;
//        }
//		
//        
//        @AndroidFindBy(id = "trip_detail_header_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripItinerary_overviewHeader']")
//        public MobileElement trip_details_screen;
//        
//        public MobileElement gettrip_details_screen() {
//                        return trip_details_screen;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_overview_image_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='trips_tripItinerary_overviewButton_unselected']")
//        public MobileElement overview_tab;
//        
//        public MobileElement getoverview_tab() {
//                        return overview_tab;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_overview_card_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name='trips_tripItinerary_overviewButton_unselected']")
//        public MobileElement overview_tab_unselected;
//        
//        public MobileElement getoverview_tab_unselected() {
//                        return overview_tab_unselected;
//        }
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_detail_tab_card_view')]")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeCollectionView[@name='GroupedSearchResultHeaderCollectionView']/XCUIElementTypeCell[2]")
//        public MobileElement date_tab;
//        
//        public MobileElement getdate_tab() {
//                        return date_tab;
//        }
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_detail_nav_bound_month_text_view')]")
//        @iOSXCUITFindBy(xpath = "//*[contains(@name,'trips_tripItinerary_shortMonth')]")
//        public MobileElement date_tab_month;
//        
//        public MobileElement getdate_tab_month() {
//                        return date_tab_month;
//        }
//        
//        @AndroidFindBy(xpath = "//*[contains(@resource-id,'trip_detail_nav_bound_date_text_view')]")
//        @iOSXCUITFindBy(xpath = "//*[contains(@name,'trips_tripItinerary_dayOfMonth_unselected')]")
//        public MobileElement date_tab_date;
//        
//        public MobileElement getdate_tab_date() {
//                        return date_tab_date;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_flight_number_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeCell[@name='GroupedSearchResultBodyTableCollectionViewCell']/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
//        public MobileElement date_tab_bound_text_view;
//        
//        public MobileElement getdate_tab_bound_text_view() {
//                        return date_tab_bound_text_view;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_header_text_view")
//        @iOSXCUITFindBy(xpath = "//*[@name='trips_tripItinerary_weekdayHeader']")
//        public MobileElement date_tab_day_itinerary;
//        
//        public MobileElement getdate_tab_day_itinerary() {
//                        return date_tab_day_itinerary;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_nav_bound_same_day_index_text_view")
//        @iOSXCUITFindBy(xpath = "<NULL>")
//        public MobileElement date_tab_unique_identifier;
//        
//        public MobileElement getdate_tab_unique_identifier() {
//                        return date_tab_unique_identifier;
//        }
//        
//        @AndroidFindBy(id = "detail_header_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripItinerary_header']")
//        public MobileElement date_tab_trip_to;
//        
//        public MobileElement getdate_tab_trip_to() {
//                        return date_tab_trip_to;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_bound_countdown_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_countdown_dayPlural']")
//        public MobileElement countdown_days;
//        
//        public MobileElement getcountdown_days() {
//                        return countdown_days;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_flight_number_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_flightNumber'])")
//        public MobileElement flight_number;
//        
//        public MobileElement getflight_number() {
//                        return flight_number;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_departure_airport_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_origin'])[1]")
//        public MobileElement departure_airport;
//        
//        public MobileElement getdeparture_airport() {
//                        return departure_airport;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_arrival_airport_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_destination'])[1] ")
//        public MobileElement arrival_airport;
//        
//        public MobileElement getarrival_airport() {
//                        return arrival_airport;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_departure_time_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_scheduledDepartureTime'])[1]")
//        public MobileElement departure_time;
//        
//        public MobileElement getdeparture_time() {
//                        return departure_time;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_arrival_time_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_scheduledArrivalTime'])[1]")
//        public MobileElement arrival_time;
//        
//        public MobileElement getarrival_time() {
//                        return arrival_time;
//        }
//        
//        @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='+1']")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_arrivalDateOffset'])[2]")
//        public MobileElement extra_days_plus1;
//        
//        public MobileElement getextra_days_plus1() {
//                        return extra_days_plus1;
//        }
//        
//        @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='+2']")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_arrivalDateOffset'])[2]")
//        public MobileElement extra_days_plus2;
//        
//        public MobileElement getextra_days_plus2() {
//                        return extra_days_plus2;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_layover_airport_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='flightStatus_resultsList_stopLayoverAirport'])[1]")
//        public MobileElement layover_airport;
//        
//        public MobileElement getlayover_airport() {
//                        return layover_airport;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_layover_duration_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='flightStatus_resultsList_stopLayoverDuration_hoursMins']")
//        public MobileElement layover_duration;
//        
//        public MobileElement getlayover_duration() {
//                        return layover_duration;
//        }
//        
//        @AndroidFindBy(id = "trip_detail_bound_countdown_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripsList_tripBlock_countdown_past']")
//        public MobileElement countdown_days_past;
//        
//        public MobileElement getcountdown_days_past() {
//                        return countdown_days_past;
//        }
//        
//        @AndroidFindBy(id = "details_flight_info_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_flightInfo")
//        public MobileElement marketing_flight_number_type;
//        
//        public MobileElement getmarketing_flight_number_type() {
//                        return marketing_flight_number_type;
//        }
//        
//        @AndroidFindBy(id = "Null")
//        @iOSXCUITFindBy(xpath = "Null")
//        public MobileElement aircraft_type;
//        
//        public MobileElement getaircraft_type() {
//                        return aircraft_type;
//        }
//        
//        @AndroidFindBy(id = "Null")
//        @iOSXCUITFindBy(xpath = "Null")
//        public MobileElement operating_carrier;
//        
//        public MobileElement getoperating_carrier() {
//                        return operating_carrier;
//        }
//        
//        @AndroidFindBy(id = "details_flight_duration_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_flightDuration")
//        public MobileElement flight_duration;
//        
//        public MobileElement getflight_duration() {
//                        return flight_duration;
//        }
//        
//        @AndroidFindBy(id = "details_cabin_alert_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_resultsList_cabinName_economy")
//        public MobileElement cabin_service;
//        
//        public MobileElement getcabin_service() {
//                        return cabin_service;
//        }
//        
//        @AndroidFindBy(id = "trip_itinerary_details_dismiss_button")
//        @iOSXCUITFindBy(accessibility = "GotItTableViewCell")
//        public MobileElement got_it_button;
//        
//        public MobileElement getgot_it_button() {
//                        return got_it_button;
//        }
//        
//        @AndroidFindBy(id = "touch_outside")
//        @iOSXCUITFindBy(xpath = "Null")
//        public MobileElement tap_outside;
//        
//        public MobileElement gettap_outside() {
//                        return tap_outside;
//        }
//        
//        
//        @AndroidFindBy(id = "details_early_morning_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_rougeDisclaimerButton")
//        public MobileElement early_morning_warning;
//        
//        public MobileElement getearly_morning_warning() {
//                        return early_morning_warning;
//        }
//        
//        @AndroidFindBy(id = "Null")
//        @iOSXCUITFindBy(xpath = "Null")
//        public MobileElement rouge_warning;
//        
//        public MobileElement getrouge_warning() {
//                        return rouge_warning;
//        }
//
//        @AndroidFindBy(id = "trip_itinerary_details_origin_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_origin'])[1]")
//        public MobileElement departure_airport_code;
//        
//        public MobileElement getdeparture_airport_code() {
//                        return departure_airport_code;
//        }
//        
//        @AndroidFindBy(id = "trip_itinerary_details_destination_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_destination'])[1] ")
//        public MobileElement arrival_airport_code;
//        
//        public MobileElement getarrival_airport_code() {
//                        return arrival_airport_code;
//        }
//        
//        @AndroidFindBy(id = "details_origin_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_departureTime")
//        public MobileElement departure_airport_city;
//        
//        public MobileElement getdeparture_airport_city() {
//                        return departure_airport_city;
//        }
//        
//        @AndroidFindBy(id = "details_destination_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_arrivalTime")
//        public MobileElement arrival_airport_city;
//        
//        public MobileElement getarrival_airport_city() {
//                        return arrival_airport_city;
//        }
//        
//        @AndroidFindBy(id = "details_departure_time_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_departureTime")
//        public MobileElement departure_time_detail;
//        
//        public MobileElement getdeparture_time_detail() {
//                        return departure_time_detail;
//        }
//        
//        @AndroidFindBy(id = "details_arrival_time_text_view")
//        @iOSXCUITFindBy(accessibility = "flightSearchResults_fareSelection_arrivalTime")
//        public MobileElement arrival_time_detail;
//        
//        public MobileElement getarrival_time_detail() {
//                        return arrival_time_detail;
//        }
//        
//        @AndroidFindBy(id = "trip_itinerary_notch_image_view")
//        @iOSXCUITFindBy(accessibility = "iconNotch1")
//        public MobileElement notch_line;
//        
//        @AndroidFindBy(xpath="//android.widget.Button[@content-desc='Back ']|//android.widget.Button[@content-desc='Retour ']")
//        public MobileElement backBtn;
//        
//        public MobileElement getnotch_line() {
//                        return notch_line;
//        }
//        
//        @AndroidFindBy(id = "details_departure_date_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_destination'])[1] ")
//        public MobileElement departure_date_detail;
//        
//        public MobileElement getdeparture_date_detail() {
//                        return departure_date_detail;
//        }
//        
//        @AndroidFindBy(id = "details_arrival_date_text_view")
//        @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name='trips_tripItinerary_destination'])[1] ")
//        public MobileElement arrival_date_detail;
//        
//        public MobileElement getarrival_date_detail() {
//                        return arrival_date_detail;
//        }
//        
//        @AndroidFindBy(id = "trip_block_booking_reference_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='trips_tripItinerary_bookingReference']")
//        public MobileElement booking_reference_trips_list;
//        
//        public MobileElement getbooking_reference_trips_list() {
//                        return booking_reference_trips_list;
//        }
//        
//        @AndroidFindBy(id="trip_detail_view_pager")
//        public MobileElement tripViewResult;
//        
//        public String tripViewRsult ="//android.widget.TextView[@content-desc='Résumé du tarif']|//android.widget.TextView[@content-desc='Fare summary']";
//        
//        @AndroidFindBy(accessibility="Retrieve entered reservation")
//        public MobileElement retrieveBtn;
//        
//        @AndroidFindBy(id="trips_booking_reference_edit_text")
//        public MobileElement bookingRefNum;
//        
//        public String removeBtn ="remove_trip_text_view";
//        public String removeBtnIOS = "(//XCUIElementTypeButton)[3]";
//        
//        @AndroidFindBy(id="remove_trip_text_view")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Remove']|//XCUIElementTypeStaticText[@name='Retirer']")
//        public MobileElement rmveBtn;
//        
//        public String remveHeader = "//android.widget.TextView[@text='Remove trip']|//android.widget.TextView[@text='Retirer le voyage']";
//        public String remveMsg = "//android.widget.TextView[contains(@text,'cancel your booking.')]|//android.widget.TextView[contains(@text,'pas votre réservation.')]";
//        public String remveMsgIOS ="//XCUIElementTypeStaticText[@name=\"Do you want to remove this trip? Removing the trip doesn't cancel your booking.\"]";
//        public String removBtn ="button1";
//        public String removBtnIOS="//XCUIElementTypeButton[@name=\"Remove\"]";
//        public String CanBtn ="button2";
//        public String CanBtnIOS="//XCUIElementTypeButton[@name=\"Cancel\"]";
//        
//        @AndroidFindBy(xpath="//android.widget.TextView[@text='Remove trip']|//android.widget.TextView[@text='Retirer le voyage']")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Remove trip']")
//        public MobileElement rmveHeader;
//        
//        @AndroidFindBy(xpath="//android.widget.TextView[contains(@text,'cancel your booking.')]|//android.widget.TextView[contains(@text,'pas votre réservation.')]")
//        @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Do you want to remove this trip? Removing the trip doesn't cancel your booking.\"]")
//        public MobileElement removeMsg;
//        
//        @AndroidFindBy(id="button1")
//        @iOSXCUITFindBy(xpath ="//XCUIElementTypeButton[@name=\"Remove\"]") 
//        public MobileElement remveBtn;
//        
//        @AndroidFindBy(id="button2")
//        @iOSXCUITFindBy(xpath ="//XCUIElementTypeButton[@name=\"Cancel\"]")  
//        public MobileElement CancelBtn;
//        
//        @AndroidFindBy(id="trips_add_reservation_button")
//        @iOSXCUITFindBy(id="trips_tripsList_tripBlockButton")
//        public MobileElement addTripsButton;
//        

	
	
	
}
