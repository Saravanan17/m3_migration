package com.acats.m3o.locators.helper;

public class FlightStatustestdata {

	private String flightNumber = "";
	private String  flightStatusHomeHeader = "";
	private String flightRoutetxt = "";
	private String flightNumbertxt = "";
	private String flightNumberHeader = "";
	private String flightNumberssearchHint = "";
	private String marketingCode = "";
	private String selDate = "";

	public String getSelDate() {
		return selDate;
	}

	public void setSelDate(String selDate) {
		this.selDate = selDate;
	}

/*	public FlightStatustestdata(String flightNumber, String flightRoutetxt, String flightNumbertxt,
			String flightNumberHeader, String flightNumberssearchHint, String marketingCode, String selDate) {
		super();
		this.flightNumber = flightNumber;
		this.flightRoutetxt = flightRoutetxt;
		this.flightNumbertxt = flightNumbertxt;
		this.flightNumberHeader = flightNumberHeader;
		this.flightNumberssearchHint = flightNumberssearchHint;
		this.marketingCode = marketingCode;
		this.selDate = selDate;
	}*/

	public String getMarketingCode() {
		return marketingCode;
	}

	public void setMarketingCode(String marketingCode) {
		this.marketingCode = marketingCode;
	}

	public String getFlightNumberssearchHint() {
		return flightNumberssearchHint;
	}

	public void setFlightNumberssearchHint(String flightNumberssearchHint) {
		this.flightNumberssearchHint = flightNumberssearchHint;
	}

	public String getFlightNumberHeader() {
		return flightNumberHeader;
	}

	public void setFlightNumberHeader(String flightNumberHeader) {
		this.flightNumberHeader = flightNumberHeader;
	}

	public String getFlightRoutetxt() {
		return flightRoutetxt;
	}

	public void setFlightRoutetxt(String flightRoutetxt) {
		this.flightRoutetxt = flightRoutetxt;
	}

	public String getFlightNumbertxt() {
		return flightNumbertxt;
	}

	public void setFlightNumbertxt(String flightNumbertxt) {
		this.flightNumbertxt = flightNumbertxt;
	}

	public void setFlightNumer(String flightNumer) {
		this.flightNumber = flightNumer;
	}

	public String getFlightNumer() {
		return flightNumber;
	}

	public String getFlightStatusHomeHeader() {
		return flightStatusHomeHeader;
	}

	public void setFlightStatusHomeHeader(String flightStatusHomeHead) {
		flightStatusHomeHeader = flightStatusHomeHead;
	}
}
