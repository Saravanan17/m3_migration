package com.acats.m3o.locators;

import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.acats.core.Contentkey;
import com.acats.core.DriverManager;
import com.acats.core.Lib_MobileGeneric;
import com.acats.core.ReadProperty;
import com.acats.m3o.locators.helper.FlightStatustestdata;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import junit.framework.AssertionFailedError;

public class FlightStatusFlightNumber extends Lib_MobileGeneric {

	static Logger logger = Logger.getLogger(FlightStatusFlightNumber.class);
	AppiumDriver<MobileElement> driver;
	FlightStatustestdata objFStestdata = new FlightStatustestdata();
	boolean flag1 = false, flag2 = false, flag3 = false;
	int loopVar = 0, datecal = 0, dateVar1 = 0, dateVar2 = 0, dateVar3 = 0, dateVar4 = 0, maxSwipe = 3,n=1;
	String dateinString1, Language, dateinString2, date1, date2, temp, valTxt, valTxt1 = " ", newlne = "\n", date3;
	String pastTxtback = "Too far back", passTxtright = "Too far out", leftNav = "left", rightNav = "right",
			smpleDteFormat = "dd/MMMM/yyyy";
	String add = "plus", pltandroid = "android", platform = "platform", strToRep = "StringToRep";
	String[] splitVar = null;
	MobileElement element1, element2;
	String contentsheetbyCopykey;
	String dataByColumNameforlanguageSpecific;
	Calendar c = Calendar.getInstance();

	public FlightStatusFlightNumber(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
	}

	String dateXpath1Andrd = "//android.widget.TextView[@content-desc=\"" + "StringToRep" + "\"]";
	String dateXpath1IOS = "(//XCUIElementTypeStaticText[@name=\"" + "StringToRep" + "\"])[2]";

	@AndroidFindBy(id = "flight_number_edit_text")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_searchFieldHint")
	public MobileElement txtflightNumber;

	@AndroidFindBy(id = "air_canada_logo")
	@iOSXCUITFindBy(id = "AC")
	public MobileElement logoaircanada;

	@AndroidFindBy(id = "flight_status_search_clear_button")
	@iOSXCUITFindBy(accessibility = "crossImage")
	public MobileElement btnflightstatussearchclear;

	@AndroidFindBy(id = "search_flight_status_button")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_searchFlightButton")
	public MobileElement btnSearchflightstatusnumber;

	@AndroidFindBy(id = "search_flight_status_reset_button")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_resetButton")
	public MobileElement btnresetsearch;

	@AndroidFindBy(id = "flight_number_edit_text")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_searchFieldHint")
	public MobileElement txtflightstatussearchHint;

	@AndroidFindBy(id = "search_by_flight_number_label")
	@iOSXCUITFindBy(accessibility = "flightStatus_flightSearch_header")
	public MobileElement txtflightStatusHomeHeaderbyNumber;

	@AndroidFindBy(id = "air_canada_flight_code")
	@iOSXCUITFindBy(id = "AC")
	public MobileElement txtflightstatusmarketingcodeAC;

	@AndroidFindBy(id = "flight_status_search_clear_button")
	@iOSXCUITFindBy(id = "search bar search icon")
	public MobileElement flightstatussearchIcon;

	@AndroidFindBy(id = "date_picker_date_past_hint_text_view")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_pastDateHint")
	public MobileElement txtPstDtePckerHint;

	@AndroidFindBy(id = "date_picker_future_date_hint")
	@iOSXCUITFindBy(id = "flightStatus_flightSearch_futureDateHint")
	public MobileElement txtFutDtePckerHint;

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: enterFlightNumber()
	 * @Description: Method to enter flightNumber
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void enterFlightNumber(FlightStatustestdata flightNumber) {

		txtflightNumber.sendKeys(flightNumber.getFlightNumer());
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: airCanadaLogo verification()
	 * @Description: Method to verify on airCanadaLogo
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void airCanadaLogo() {
		assertTrue(logoaircanada.isDisplayed());
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: clearButton verification()
	 * @Description: Method to verify clearButton
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void clearButton() {
		assertTrue(btnflightstatussearchclear.isDisplayed());
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: resetButton verification()
	 * @Description: Method to verify resetButton
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void resetButton() {

		assertTrue(btnresetsearch.isDisplayed());
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: searchFlightstatusNumber()
	 * @Description: Method to verify flightNumber button
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void searchFlightstatusNumber() {

		assertTrue(btnSearchflightstatusnumber.isDisplayed());
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: tapOnResetButton()
	 * @Description: Method to tap on ResetButton
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void tapOnResetButton() {
		resetButton();
		btnresetsearch.click();

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: tapOnCancelButton()
	 * @Description: Method to tap on CancelButton
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void tapOnCancelButton() {
		clearButton();
		btnflightstatussearchclear.click();

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: contentkeyFlightNumberHeader()
	 * @Description: Method to verify contentkey for FlightNumber Header
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void verificationFlightNumberHeader(FlightStatustestdata FSNumberHead) {

		Assert.assertEquals(true, Contentkey.getContentsheetbyCopykey(FSNumberHead.getFlightNumberHeader())
				.equals(txtflightStatusHomeHeaderbyNumber.getText()));

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: contentkeyFlightNumberHint()
	 * @Description: Method to verify contentkey for FlightNumber Hint
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */

	public void verificationFlightNumberHint(FlightStatustestdata numberHint) {

		Assert.assertEquals(true, Contentkey.getContentsheetbyCopykey(numberHint.getFlightNumberssearchHint())
				.equals(txtflightstatussearchHint.getText()));

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: marketingtxtandsearchIconVerification()
	 * @Description: Method to verify contentkey for marketing logo
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */

	public void verificationMarketingtxtandsearchIcon(FlightStatustestdata flightNumberCode) {

		Assert.assertEquals(true, Contentkey.getContentsheetbyCopykey(flightNumberCode.getMarketingCode())
				.equals(txtflightstatusmarketingcodeAC.getText()));
		assertTrue(flightstatussearchIcon.isDisplayed());

	}
	
	public void datePickerAndTextVal() {
        flag1 = swipeAndSelDate(maxSwipe,objFStestdata.getSelDate(),leftNav);
        flag2 =swipeAndSelDate(maxSwipe,objFStestdata.getSelDate(),rightNav);
        swipeAndSelDate(n,objFStestdata.getSelDate(),"");
        if(flag1&&flag2) {
            flag3=true;
            logger.info("Left and Right text validation successful");
        }
        else if(!flag1) {
            logger.info("Left side text not displayed as expected");
            flag3=false;
        }else if(!flag2) {
            logger.info("Right side text not displayed as expected");
            flag3=false;
        }
        if(!flag3) {
        	Assert.fail();
        }

 

    }
	
	public boolean valFlightStatusTxt(String txtsde) {
        if(txtsde.equalsIgnoreCase(leftNav)) {
            valTxt = txtPstDtePckerHint.getText().replace(newlne, valTxt1).trim();
            if(pastTxtback.equals(valTxt)) 
                flag1=true;
        }else {
            valTxt = txtFutDtePckerHint.getText().replace(newlne, valTxt1).trim();
            if(passTxtright.equals(valTxt)) 
                flag1=true;
        }
        return flag1;
    }
	
	  public void swipe(int dteVar1,int dteVar2 ) {
	        if(ReadProperty.get_propValue(platform).equalsIgnoreCase(pltandroid)) {
	            date1= dateXpath1Andrd.replace(strToRep, String.valueOf(dteVar1));
	            date2= dateXpath1Andrd.replace(strToRep, String.valueOf(dteVar2));
	        }else {
	            date1 = dateXpath1IOS.replace(strToRep, String.valueOf(dteVar1));
	            date1 = dateXpath1IOS.replace(strToRep, String.valueOf(dteVar2));
	        }
	        element1 = DriverManager.getMDriver().findElementByXPath(date1);
	        element2 =  DriverManager.getMDriver().findElementByXPath(date2);
	        Lib_MobileGeneric.swipeByElements(element1, element2);

	 

	    }
	  
	  public boolean swipeAndSelDate(int n,String date,String textVal) {
	        if(!(textVal.length()>1)) {
	            if(date.contains(add)) {
	                date1 =  date.split(" ")[3];
	                dateVar1 = Integer.parseInt(date1);
	                SimpleDateFormat sdf = new SimpleDateFormat(smpleDteFormat);
	                c.setTime(new Date()); 
	                dateinString1 = sdf.format(c.getTime());
	                date3 = dateinString1.split("/")[0];
	                dateVar2 = Integer.parseInt(date3);
	                c.add(Calendar.DATE, dateVar1);
	                dateinString2 = sdf.format(c.getTime());
	                date2 = dateinString2.split("/")[0];
	                dateVar3 = Integer.parseInt(date2);
	                swipe(dateVar3,dateVar2);
	            }else {
	                date1 =  date.split(" ")[3];
	                dateVar1 = Integer.parseInt(date1);
	                SimpleDateFormat sdf = new SimpleDateFormat(smpleDteFormat);
	                c.setTime(new Date()); 
	                dateinString1 = sdf.format(c.getTime());
	                date3 = dateinString1.split("/")[0];
	                dateVar2 = Integer.parseInt(date3);
	                c.add(Calendar.DATE, -dateVar1);
	                dateinString2 = sdf.format(c.getTime());
	                date2 = dateinString2.split("/")[0];
	                dateVar3 = Integer.parseInt(date2);
	                swipe(dateVar3,dateVar2);
	            }
	        }else {
	            if(textVal.equalsIgnoreCase("right")) {
	                SimpleDateFormat sdf = new SimpleDateFormat(smpleDteFormat);
	                c.setTime(new Date()); 
	                for(loopVar=1;loopVar<=n;loopVar++) {
	                    dateinString1 = sdf.format(c.getTime());
	                    dateinString2 = dateinString1.split("/")[0];
	                    datecal = Integer.parseInt(dateinString2);
	                    if(loopVar==1) {
	                        dateVar2 = datecal;
	                    }
	                    dateVar1 = datecal +loopVar;
	                    swipe(dateVar1,dateVar2);
	                    dateVar2 = datecal+loopVar;
	                }
	                flag1 = valFlightStatusTxt(textVal);
	                swipe(datecal,dateVar1);
	            }else {
	                SimpleDateFormat sdf = new SimpleDateFormat(smpleDteFormat);
	                c.setTime(new Date()); 
	                for(loopVar=1;loopVar<=n;loopVar++) {
	                    dateinString1 = sdf.format(c.getTime());
	                    dateinString2 = dateinString1.split("/")[0];
	                    datecal = Integer.parseInt(dateinString2);
	                    if(loopVar==1) {
	                        dateVar2 = datecal;
	                    }
	                    dateVar1 = datecal -loopVar;
	                    swipe(dateVar1,dateVar2);
	                    dateVar2 = datecal-loopVar;
	                }
	                flag1 = valFlightStatusTxt(textVal);
	                swipe(datecal,dateVar1);
	            }
	        }
	        return flag1;
	    }

}
