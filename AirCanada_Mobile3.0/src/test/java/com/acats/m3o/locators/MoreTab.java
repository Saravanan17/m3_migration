package com.acats.m3o.locators;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.acats.core.DriverManager;
import com.acats.core.FileReaderManager;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class MoreTab  {
	public MoreTab() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.getMDriver()), this);
	}
	public String intEnv ="int_env";
	public String crtEnv = "crt_env";
	public String prepodEnv ="preprod_env";
	public String prod ="prod_env";
	public String pssUAT ="pss_uat";
	public String iOSIntEnv="int";
	public String iOSCrtEnv = "crt";
	public String iOSpss="pss";
	public String iOSpreprod="preProd";
	public String iOSprod= "preProd";
	
	

	//@AndroidFindBy(xpath = "//*[contains(@text,'Important Disclosures')]|//*[contains(@text,'Avis Importants')]")
	@AndroidFindBy(xpath = "//*[@text='Terms & Conditions - Android']")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name='Important Disclosures']|//*[@name=\"Avis Importants\"]")
	public MobileElement Mobile_app_web_view;

	@AndroidFindBy(xpath = "//*[@text='UC Browser']")
	public MobileElement select_ucbrowser;
	
	@AndroidFindBy(xpath = "//*[@text='Always'] | //*[@text='Toujours']")
	public MobileElement select_always;
	
	
	@AndroidFindBy(xpath = "//*[@text='Air Canada Privacy Protection Policy'] | //*[@text='Air Canada – Politique sur la protection des renseignements personnels']")
	@iOSXCUITFindBy(xpath = "//*[@name='URL']")
	public MobileElement Privacy_app_web_view;
	

	@AndroidFindBy(xpath = "//*[@text='Our General Terms and Conditions of Carriage - Air Canada'] | //*[@text='Nos conditions générales de transport - Air Canada']")
	@iOSXCUITFindBy(xpath = "//*[@name='URL']")
	public MobileElement Conditions_app_web_view;

	@AndroidFindBy(id = "action_more")
	@iOSXCUITFindBy(xpath = "//*[@name='More']|//XCUIElementTypeButton[@name='Plus']")
	public MobileElement More_btn;

	@AndroidFindBy(xpath = "//*[contains(@text,'More')]|//*[contains(@text,'Plus')]")
	@iOSXCUITFindBy(accessibility = "generalStories_more_moreHeader")
	public MobileElement More_txt;

	@AndroidFindBy(id = "designed_by_text_view")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Designed and developed in Canada\"]|//*[@name=\"Conçu et développé au Canada\"]")
	public MobileElement Design_and_development_in_canada_txt;

	//@AndroidFindBy(id = "legal_text_view")
	@AndroidFindBy(id = "com.aircanada.mobile.dev:id/more_legal_text_view")
	@iOSXCUITFindBy(accessibility = "generalStories_more_legalButton")
	public MobileElement Legal_btn;

	@AndroidFindBy(id = "legal_title_textview")
	@iOSXCUITFindBy(accessibility = "generalStories_legal_legalHeader")
	public MobileElement Legal_txt;

	@AndroidFindBy(id = "mobile_terms_link_layout")
	@iOSXCUITFindBy(xpath = "(//*[@name=\"LegalLinkTableViewCell\"])[1]")
	public MobileElement Mobile_app_btn;

	@AndroidFindBy(xpath = "//*[contains(@text,'terms and conditions')]|//*[contains(@text,'Termes et conditions')]")
	@iOSXCUITFindBy(accessibility = "generalStories_legal_termsConditionsButton")
	public MobileElement Mobile_app_txt;

	@AndroidFindBy(id = "privacy_policy_link_layout")
	@iOSXCUITFindBy(xpath = "(//*[@name=\"LegalLinkTableViewCell\"])[2]")
	public MobileElement Privacy_policy_btn;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"Select to view\"])[2]/android.widget.TextView|//*[contains(@text,'Politique de confidentialité')]")
	@iOSXCUITFindBy(accessibility = "generalStories_legal_privacyPolicyButton")
	public MobileElement Privacy_policy_txt;

	@AndroidFindBy(id = "conditions_of_carriage_link")
	@iOSXCUITFindBy(xpath = "(//*[@name='LegalLinkTableViewCell'])[3]")
	public MobileElement Conditions_of_carriage_and_tariffs_btn;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup[@content-desc=\"Select to view\"])[3]/android.widget.TextView|//*[contains(@text,'Conditions de transport et tarifs')]")
	@iOSXCUITFindBy(accessibility = "generalStories_legal_carriageTariffsButton")
	public MobileElement Conditions_of_carriage_and_tariffs_txt;

	@AndroidFindBy(id = "login_button")
	@iOSXCUITFindBy(id = "accountLogin_login_logInButton")
	public MobileElement LoginButton;

	@AndroidFindBy(id = "maps")
	@iOSXCUITFindBy(id = "Airport Map")
	public MobileElement MapsButton;

	@AndroidFindBy(id = "account_logout_button")
	@iOSXCUITFindBy(id = "accountManagement_logOut_primaryButton")
	public MobileElement LogoutMenu;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Are you sure you want to log out?\"] | //XCUIElementTypeStaticText[@name=\"Êtes-vous sûr de vous déconnecter?\"]")
	public MobileElement LogoutConfPopupTitle;

	@AndroidFindBy(id = "android:id/message")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"We'll clear this device of your account information and log out.\"] | //XCUIElementTypeStaticText[@name=\"Nous effacerons les informations de votre compte sur cet appareil et vous déconnecter.\"]")
	public MobileElement LogoutConfPopupMessage;

	@AndroidFindBy(id = "android:id/button2")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Cancel\"] | //XCUIElementTypeButton[@name=\"Annuler\"]")
	public MobileElement CancelBtnInLogoutConfPopup;

	@AndroidFindBy(id = "android:id/button1")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"Log out\"] | //XCUIElementTypeButton[@name=\"Déconnexion\"]")
	public MobileElement LogoutBtnInLogoutConfPopup;

	@AndroidFindBy(id = "custom_snack_bar_text_view")
	@iOSXCUITFindBy(id = "generalStories_inAppConfirmation_loggedOut_message")
	public MobileElement LogoutConfNotificationText;

	@AndroidFindBy(id = "com.aircanada.mobile.dev:id/more_incomplete_features")
	@iOSXCUITFindBy(id = "Incomplete features")
	public MobileElement IncompleteFeatures;
	

	//added/Modified By Dharti
	//android.widget.LinearLayout[contains(@resource-id,'more_check_in')]/android.widget.ImageButton/android.widget.TextView
	@AndroidFindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'more_check_in')]/android.widget.TextView")
    @iOSXCUITFindBy(accessibility = "generalStories_more_checkIn")
    private MobileElement Checkinbtn_More;
    
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'more_boarding_passes')]/android.widget.TextView")
    @iOSXCUITFindBy(accessibility = "generalStories_more_boardingPasses")
    private MobileElement BoardingPassesbtn_More;
    
    
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[contains(@resource-id,'more_customer_support')]/android.widget.TextView")
    @iOSXCUITFindBy(accessibility = "generalStories_more_customerSupport")
    private MobileElement CustomerSupportbtn_More;
    
    
    @AndroidFindBy(id = "more_airport_and_terminal_maps")
    @iOSXCUITFindBy(accessibility= "generalStories_more_airportMaps")
    private MobileElement AirportandTerminalMapbtn_More;
    
    @AndroidFindBy(id = "more_wifi_and_entertainment")
    @iOSXCUITFindBy(accessibility = "generalStories_more_wifiEntertainment")
    private MobileElement WifiandEntertainmentbtn_More;
    

    @AndroidFindBy(id = "more_settings")
    @iOSXCUITFindBy(accessibility = "Settings")
    private MobileElement Settingsbtn_More;
    

    
    @AndroidFindBy(id = "more_feedback")
    @iOSXCUITFindBy(accessibility = "homeScreen_menuTab_feedback")
    private MobileElement Feedback_More;
    
    @AndroidFindBy(id = "more_canadian_flag")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name=\"🇨🇦\"]")
    private MobileElement Flag_More;
    
    
    @AndroidFindBy(id = "com.aircanada.mobile.dev:id/more_version_text_view")
    @iOSXCUITFindBy(id = "generalStories_more_appVersion")
    private MobileElement AppVersionInMore;
    
    

    @AndroidFindBy(id = "more_designed_by_text_view")
    @iOSXCUITFindBy(id = "generalStories_more_madeInCanadaText")
    private MobileElement DesignedandDevelopment_txt_More;
    
    @AndroidFindBy(id="pin_edit_text")
    public MobileElement pinEnvChange;
    
    @AndroidFindBy(id="validate_button")
    public MobileElement cntueEnvChange;
    
    @AndroidFindBy(id="more_version_text_view")
    @iOSXCUITFindBy(accessibility="generalStories_more_appVersion")
    public MobileElement currrentEnv;
    
    @AndroidFindBy(id="close_button")
    @iOSXCUITFindBy(accessibility="Stop")
    public MobileElement clseBtnEnv;
    
    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"2\"]")
    public MobileElement keypadTwo;
    
    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"7\"]")
    public MobileElement keypadSeven;
    
    @iOSXCUITFindBy(xpath="//XCUIElementTypeButton[@name=\"3\"]")
    public MobileElement keypadThree;
    
    
    @iOSXCUITFindBy(id = "1")
	public MobileElement one;
	
	@iOSXCUITFindBy(id = "2")
	public MobileElement two;

	@iOSXCUITFindBy(id = "3")
	public MobileElement three;
	
	@iOSXCUITFindBy(id = "4")
	public MobileElement four;
	
	@iOSXCUITFindBy(id = "5")
	public MobileElement five;
	
	@iOSXCUITFindBy(id = "6")
	public MobileElement six;
	
	@iOSXCUITFindBy(id = "7")
	public MobileElement seven;
	
	@iOSXCUITFindBy(id = "8")
	public MobileElement eight;
	
	@iOSXCUITFindBy(id = "9")
	public MobileElement nine;
	
	@iOSXCUITFindBy(id = "0")
	public MobileElement zero;
	
	
	@AndroidFindBy(id="enable_remote_config_text_view")
    @iOSXCUITFindBy(xpath="(//XCUIElementTypeStaticText)[50]")
    public MobileElement enableRemoteConfig;
    
    @AndroidFindBy(id="enable_remote_config_switch")
    @iOSXCUITFindBy(xpath="//XCUIElementTypeSwitch[@name=\"Enable Remote Config\"]")
    public MobileElement enableRemoteConfigSwitch;
    
    @AndroidFindBy(id="use_altea_tokenizer_text_view")
    @iOSXCUITFindBy(xpath="(//XCUIElementTypeStaticText)[68]")
    public MobileElement alteaProvider;
    
    @AndroidFindBy(id="use_altea_tokenizer_dev_text_view")
    @iOSXCUITFindBy(xpath="(//XCUIElementTypeStaticText)[69]")
    public MobileElement alteaDev;
    
    @AndroidFindBy(id="use_altea_tokenizer_dev_switch")
    @iOSXCUITFindBy(xpath="(//XCUIElementTypeSwitch[@name=\"useAlteaTokenizer, Dev, Prod\"])[2]")
    public MobileElement alteaDevSwitch;

    
	public MobileElement getLoginButton() {
		return LoginButton;
	}

	public MobileElement getMobile_app_web_view() {
		return Mobile_app_web_view;
	}

	public MobileElement getPrivacy_app_web_view() {
		return Privacy_app_web_view;
	}

	public MobileElement getConditions_app_web_view() {
		return Conditions_app_web_view;
	}

	public MobileElement getMore_btn() {
		return More_btn;
	}

	public MobileElement getMore_txt() {
		return More_txt;
	}

	public MobileElement getDesign_and_development_in_canada_txt() {
		return Design_and_development_in_canada_txt;
	}

	public MobileElement getLegal_btn() {
		return Legal_btn;
	}

	public MobileElement getLegal_txt() {
		return Legal_txt;
	}

	public MobileElement getMobile_app_btn() {
		return Mobile_app_btn;
	}

	public MobileElement getMobile_app_txt() {
		return Mobile_app_txt;
	}

	public MobileElement getPrivacy_policy_btn() {
		return Privacy_policy_btn;
	}

	
	public MobileElement getPrivacy_policy_txt() {
		return Privacy_policy_txt;
	}

	public MobileElement getConditions_of_carriage_and_tariffs_btn() {
		return Conditions_of_carriage_and_tariffs_btn;
	}

	public MobileElement getConditions_of_carriage_and_tariffs_txt() {
		return Conditions_of_carriage_and_tariffs_txt;
	}

	public MobileElement getCheckinbtn_More() {
		return Checkinbtn_More;
	}

	public MobileElement getBoardingPassesbtn_More() {
		return BoardingPassesbtn_More;
	}

	public MobileElement getCustomerSupportbtn_More() {
		return CustomerSupportbtn_More;
	}

	public MobileElement getAirportandTerminalMapbtn_More() {
		return AirportandTerminalMapbtn_More;
	}

	public MobileElement getWifiandEntertainmentbtn_More() {
		return WifiandEntertainmentbtn_More;
	}

	public MobileElement getSettingsbtn_More() {
		return Settingsbtn_More;
	}

	public MobileElement getFeedback_More() {
		return Feedback_More;
	}

	public MobileElement getFlag_More() {
		return Flag_More;
	}

	public MobileElement getDesignedandDevelopment_txt_More() {
		return DesignedandDevelopment_txt_More;
	}
	
	public MobileElement getAppVersionInMore() {
		return AppVersionInMore;
	}
	
	public MobileElement getInclompleteFeatures() {
		return IncompleteFeatures;
	}

	
	
	
}
