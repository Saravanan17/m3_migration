package com.acats.m3o.locators;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.acats.core.Contentkey;
import com.acats.core.Lib_MobileGeneric;
import com.acats.m3o.locators.helper.FlightStatustestdata;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class FlightStatusLandingScreen extends Lib_MobileGeneric {
	static Logger logger = Logger.getLogger(FlightStatusLandingScreen.class);
	Contentkey contentKey = new Contentkey();
	AppiumDriver<MobileElement> driver;
	FlightStatustestdata objFStestdata = new FlightStatustestdata();

	public FlightStatusLandingScreen(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
	}

	@AndroidFindBy(id = "flight_status_by_number_button")
	@iOSXCUITFindBy(accessibility = "flightStatus_sheet_searchFlightButton")
	public MobileElement btnFlightNumber;

	@AndroidFindBy(id = "flight_number_text_view")
	@iOSXCUITFindBy(accessibility = "flightStatus_sheet_searchFlightButton")
	public MobileElement txtFlightNumber;

	@AndroidFindBy(id = "flight_status_by_route_button")
	@iOSXCUITFindBy(accessibility = "flightStatus_sheet_searchRouteButton")
	public MobileElement btnFlightRoute;

	@AndroidFindBy(id = "search_by_route_text_view")
	@iOSXCUITFindBy(accessibility = "flightStatus_sheet_searchRouteButton")
	public MobileElement txtFlightRoute;

	@AndroidFindBy(id = "action_flight_status")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Flight status'] |//XCUIElementTypeButton[@name='État de vol']")
	public MobileElement tabFlightStatus;

	@AndroidFindBy(id = "tab_header_text_view")
	@iOSXCUITFindBy(accessibility = "flightStatus_home_header")
	public MobileElement lblFlightStatusHomeHeader;

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: tapOnFlightNumber()
	 * @Description: Method to tap on flightNumber
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */

	public void tapOnFlightNumber() {
		btnFlightNumber.click();
	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: tapOnFlightRoute()
	 * @Description: Method to tap on flightRoute
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */

	public void tapOnFlightRoute() {
		btnFlightRoute.click();

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: tapOnFlightStatus()
	 * @Description: Method to tap on flightStatus
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void tapOnflightStatus() {

		tabFlightStatus.click();

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: contentKeyVerification()
	 * @Description: Method to validate content key for flightStatus Header
	 * @param: contentKey - string
	 * @Date: 09/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */

	public void contentkeyValidationforFlightStatusHeader(FlightStatustestdata fstestdata) {
		boolean status=false;
		status=Contentkey.getContentsheetbyCopykey(fstestdata.getFlightStatusHomeHeader())
			.equals(lblFlightStatusHomeHeader.getText());
		Assert.assertEquals(status,true);

	}

	/**
	 * ********************************************************************************************
	 *
	 * @Project Name: Mobile3.0 Application
	 * @Function Name: contentKeyVerification()
	 * @Description: Method to validate content key for flightRoute and flightNumber
	 * @Date: 10/12/2019
	 * @author: Madhankumar
	 *
	 *          *******************************************************************************************
	 */
	public void contentkeyFlightRouteandFlightNumber(FlightStatustestdata fsrRouteNumber) {

		Assert.assertEquals(true, Contentkey.getContentsheetbyCopykey(fsrRouteNumber.getFlightRoutetxt())
				.equals(txtFlightRoute.getText()));

		Assert.assertEquals(true, Contentkey.getContentsheetbyCopykey(fsrRouteNumber.getFlightNumbertxt())
				.equals(txtFlightNumber.getText()));

	}

}
