package com.acats.m3o.stepdef;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.acats.core.DriverManager;
import com.acats.core.Lib_MobileGeneric;
import com.acats.m3o.locators.BookingSearchLanding;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

@SuppressWarnings("deprecation")
public class BookingSearchLandingStepDef extends Lib_MobileGeneric {

	BookingSearchLanding objBookingSearchLanding;

	@When("^I tap on Book tab$")
	public void i_tap_on_book_tab() throws Throwable {
		objBookingSearchLanding = new BookingSearchLanding();
		objBookingSearchLanding.tapOnBookTab();
	}

	@Then("^I verify that user gets navigated to \"([^\"]*)\"$")
	public void i_verify_that_user_gets_navigated_to_something(String strArg1) throws Throwable {
		objBookingSearchLanding.verifyBSCityAirportSelectionScreenIsDisplayed();
	}

	@When("^I select \"([^\"]*)\" and \"([^\"]*)\" on BS - City/Airport Selection screen$")
	public void i_select_something_and_something_on_bs_cityairport_selection_screen(String departureairport,
			String arrivalairport) throws Throwable {

		objBookingSearchLanding.selectAirportCity(departureairport);
		objBookingSearchLanding.selectAirportCity(arrivalairport);

	}

	@When("^I select \"([^\"]*)\" \"([^\"]*)\" and taps on Continue$")

	public void iSelectAndTapsOnContinue(String departDate, String returnDate) throws Throwable {
		
		objBookingSearchLanding.selectDate(departDate, returnDate);
		
	}
	
	   @And("^I tap on Choose depart on BSlanding screen$")
	    public void i_tap_on_choose_depart_on_bslanding_screen() throws Throwable {
		   objBookingSearchLanding.clickOnChooseDepart();
	    }
	   
	   @And("^I verify that App displays a Geolocation permission popup$")
	    public void i_verify_that_app_displays_a_geolocation_permission_popup() throws Throwable {
		   objBookingSearchLanding.verifyGeolocationPopup();
	   }
	   
	   @And("^I Tap on Allow while using the app$")
	    public void i_tap_on_allow_while_using_the_app() throws Throwable {
		   objBookingSearchLanding.clickAllowInGeoPopup();
	    
	    }

}
