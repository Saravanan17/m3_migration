package com.acats.m3o.stepdef;

import org.testng.Assert;

import com.acats.core.DriverManager;
import com.acats.core.Lib_MobileGeneric;
import com.acats.m3o.locators.FlightStatusFlightNumber;
import com.acats.m3o.locators.FlightStatusLandingScreen;
import com.acats.m3o.locators.helper.FlightStatustestdata;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EndtoEnd_FlightStatus_stepdefinitions {

	FlightStatusLandingScreen objFSLandingScreen;
	FlightStatusFlightNumber objFSFlightNumber;
	FlightStatustestdata fstestdata = new FlightStatustestdata();

	@When("^user taps on the flight status tab$")
	public void user_taps_on_the_flight_status_tab() throws Throwable {
		objFSLandingScreen = new FlightStatusLandingScreen(DriverManager.getMDriver());
		objFSLandingScreen.tapOnflightStatus();
	}

	@When("^I Tap on Flight Number in the FS_Landing screen$")
	public void i_tap_on_flight_number_in_the_fslanding_screen() throws Throwable {
		objFSLandingScreen = new FlightStatusLandingScreen(DriverManager.getMDriver());

		objFSLandingScreen.tapOnFlightNumber();
	}

	@When("^I enter the \"([^\"]*)\" on flightNumber search field$")
	public void i_enter_the_something_on_flightnumber_search_field(String flightnumber) throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		FlightStatustestdata fstestdata = new FlightStatustestdata();
		fstestdata.setFlightNumer(flightnumber);
		objFSFlightNumber.enterFlightNumber(fstestdata);

	}

	@And("^I verify aircanada logo is displayed$")
	public void i_verify_aircanada_logo_is_displayed() throws Throwable {
		objFSFlightNumber.airCanadaLogo();
	}

	@And("^I verify that flightStatus flightSearch searchFlightButton and reset button are displayed for flightnumber$")
	public void i_verify_that_flightstatus_flightsearch_searchflightbutton_and_reset_button_are_displayed_for_flightnumber()
			throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		objFSFlightNumber.resetButton();
		objFSFlightNumber.searchFlightstatusNumber();
	}

	@When("^I tap on reset button in flightnumber screen$")
	public void i_tap_on_reset_button_in_flightnumber_screen() throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		objFSFlightNumber.tapOnResetButton();
	}

	@And("^I tap on search button \"([^\"]*)\" for flightnumber$")
	public void i_tap_on_search_button_something_for_flight_number(String strArg1) throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		objFSFlightNumber.searchFlightstatusNumber();
	}

	@Then("^I verify a sheet is displayed with header \"([^\"]*)\"$")
	public void i_verify_a_sheet_is_displayed_with_header_something(String flightStatusHomeHeader) throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		FlightStatustestdata fstestdata = new FlightStatustestdata();
		fstestdata.setFlightStatusHomeHeader(flightStatusHomeHeader);
		objFSLandingScreen.contentkeyValidationforFlightStatusHeader(fstestdata);

	}

	@And("^I verify that buttons \"([^\"]*)\" \"([^\"]*)\" are displayed$")
	public void i_verify_that_buttons_something_something_are_displayed(String flightRoute, String flightNumber)
			throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		fstestdata.setFlightRoutetxt(flightRoute);
		fstestdata.setFlightNumbertxt(flightNumber);
		objFSLandingScreen.contentkeyFlightRouteandFlightNumber(fstestdata);
	}

	@Then("^I verify a sheet is displayed with header \"([^\"]*)\" for FlightNumber$")
	public void i_verify_a_sheet_is_displayed_with_header_something_for_flightnumber(String FlightNumberHead)
			throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		fstestdata.setFlightNumberHeader(FlightNumberHead);
		objFSFlightNumber.verificationFlightNumberHeader(fstestdata);
	}

	@And("^I verify that an input field with a hint is displayed \"([^\"]*)\" for entering flight number$")
	public void i_verify_that_an_input_field_with_a_hint_is_displayed_something_for_entering_flight_number(
			String flightNumberhint) throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		fstestdata.setFlightNumberssearchHint(flightNumberhint);
		objFSFlightNumber.verificationFlightNumberHint(fstestdata);
	}

	@And("^I verify the Marketing code with logo \"([^\"]*)\" and Search_icon is displayed$")
	public void i_verify_the_marketing_code_with_logo_something_and_searchicon_is_displayed(String marketingCode)
			throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		fstestdata.setMarketingCode(marketingCode);
		objFSFlightNumber.verificationMarketingtxtandsearchIcon(fstestdata);
	}

	@Then("^I verify the flightStatus flightSearch clearButton button is displayed$")
	public void i_verify_the_flightstatus_flightsearch_clearbutton_button_is_displayed() throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		objFSFlightNumber.clearButton();
	}

	@And("^I tap on cancel icon in flightnumber$")
	public void i_tap_on_cancel_icon_in_flightnumber() throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		objFSFlightNumber.tapOnCancelButton();
	}

	@And("^I select \"([^\"]*)\" from date picker$")
	public void i_select_something_from_date_picker(String datepicker) throws Throwable {
		objFSFlightNumber = new FlightStatusFlightNumber(DriverManager.getMDriver());
		fstestdata.setSelDate(datepicker);
		objFSFlightNumber.datePickerAndTextVal();
	}
}
