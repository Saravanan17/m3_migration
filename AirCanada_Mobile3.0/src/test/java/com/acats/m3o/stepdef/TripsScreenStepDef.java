
package com.acats.m3o.stepdef;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.acats.core.DriverManager;
import com.acats.m3o.locators.TripsScreen;

import cucumber.api.java.en.Then;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class TripsScreenStepDef  {
	
	TripsScreen objTrips_Screen; 
	
	 @Then("^verify that App is navigated by default on Trips landing screen$")
	    public void verify_that_app_is_navigated_by_default_on_trips_landing_screen() throws Throwable {
	    	objTrips_Screen = new TripsScreen();
	    	objTrips_Screen.verifyDefaultTripScreenIsDisplayed();
	    
	    }
	
	
	
}
