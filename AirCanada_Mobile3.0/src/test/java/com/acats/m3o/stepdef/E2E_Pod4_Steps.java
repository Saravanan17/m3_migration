package com.acats.m3o.stepdef;

import com.acats.core.Contentkey;
import com.acats.core.DriverManager;
import com.acats.core.InitializeDriver;
import com.acats.m3o.locators.TripsScreen;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class E2E_Pod4_Steps {
	TripsScreen objTrips_Screen;

	@Given("^I launch mobile application$")
	public void i_launch_mobile_30_application() throws Throwable {
		InitializeDriver.startDriver(null);

//    	DriverManager.getMDriver().launchApp();
//    	        throw new PendingException();
	}

	@And("^I tap on \"([^\"]*)\" tab in menu bar$")
	public void i_tap_on_something_tab_in_menu_bar(String strArg1) throws Throwable {
//        throw new PendingException();
	}

	@Given("^I verify the CustomerSupport Menu redirects properly to the CustomerSupport Screen$")
	public void i_verify_the_customersupport_menu_redirects_properly_to_the_customersupport_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify ContactUs by Phone option$")
	public void i_verify_contactus_by_phone_option() throws Throwable {
		throw new PendingException();
	}

	@And("^verify chit sheet displayed$")
	public void verify_chit_sheet_displayed() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify ContactUs via Twitter$")
	public void i_verify_contactus_via_twitter() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify ContactUs via Messenger$")
	public void i_verify_contactus_via_messenger() throws Throwable {
		throw new PendingException();
	}

	@Given("^I verify user feedback through Usabilla to send a feedback$")
	public void i_verify_user_feedback_through_usabilla_to_send_a_feedback() throws Throwable {
		throw new PendingException();
	}

	@And("^I navigate back to \"([^\"]*)\" tab in menu bar$")
	public void i_navigate_back_to_something_tab_in_menu_bar(String strArg1) throws Throwable {
		throw new PendingException();
	}

	@Given("^I verify Missing or Damaged Baggage validate it redirects to baggage page$")
	public void i_verify_missing_or_damaged_baggage_validate_it_redirects_to_baggage_page() throws Throwable {
		throw new PendingException();
	}

	@Given("^I verify More information and validate it redirects to More info page$")
	public void i_verify_more_information_and_validate_it_redirects_to_more_info_page() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the AirportMaps Menu redirects properly to the AirportMaps Screen$")
	public void i_verify_the_airportmaps_menu_redirects_properly_to_the_airportmaps_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify search in Airport Search maps$")
	public void i_verify_search_in_airport_search_maps() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the WifiAndEntertainment Menu redirects properly to the WifiAndEntertainment Screen$")
	public void i_verify_the_wifiandentertainment_menu_redirects_properly_to_the_wifiandentertainment_screen()
			throws Throwable {
		throw new PendingException();
	}

	@And("^I verify popups in the WifiAndEntertainment menu$")
	public void i_verify_popups_in_the_wifiandentertainment_menu() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the More Screen$")
	public void i_verify_the_content_validations_in_the_more_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the CustomerSupport Screen$")
	public void i_verify_the_content_validations_in_the_customersupport_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the ByPhone popup$")
	public void i_verify_the_content_validations_in_the_byphone_popup() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the Usabilla feedback screen$")
	public void i_verify_the_content_validations_in_the_usabilla_feedback_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the SelectAirportMap Screen$")
	public void i_verify_the_content_validations_in_the_selectairportmap_screen() throws Throwable {
		throw new PendingException();
	}

	@And("^I verify the Content validations in the WifiAndEntertainment Screen$")
	public void i_verify_the_content_validations_in_the_wifiandentertainment_screen() throws Throwable {
		throw new PendingException();
	}

}
