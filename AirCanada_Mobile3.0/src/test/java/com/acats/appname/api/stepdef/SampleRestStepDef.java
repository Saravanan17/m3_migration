package com.acats.appname.api.stepdef;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.acats.core.JsonUtility;
import com.acats.core.RestUtility;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class SampleRestStepDef {
	final static Logger logger = Logger.getLogger(SampleRestStepDef.class);

	@Given("^I want to execute \"([^\"]*)\" service \"([^\"]*)\"$")
	public void i_want_to_execute_something_service_something(String servicetype, String servicename) throws Throwable {
		logger.info("*********Executing Rest API*********");
	}

	@When("^I submit \"([^\"]*)\" request \"([^\"]*)\"$")
	public void i_submit_something_request_something(String servicetype, String url) throws Throwable {
		RestUtility.setBaseURI(url);
		logger.info("*********API URL : *********"+url);
		RequestSpecification request = RestAssured.given()
				.spec(RestUtility.requestBuilder.build());
		if(servicetype.contains("post")) {
			RestUtility.response =request.post();
		}else {
			RestUtility.response =request.get();
			logger.info("Response :" + RestUtility.response.prettyPrint());
		}
	}

	@Then("^I Verify response status code is \"([^\"]*)\"$")
	public void i_verify_status_code_is_something(int statuscode) throws Throwable {
		RestUtility.response.then().statusCode(statuscode);
	}

	@And("^I enter the below parameter$")
	public void i_enter_the_below_parameter(DataTable dt) throws Throwable {
		List<List<String>> datTable = dt.asLists(String.class);
		RestUtility.constructPostbody(datTable);
	}

	@And("^I Validate json response$")
	public void i_validate_json_response(Map<String, String> nameValuePairs) throws Throwable {
		JsonUtility.validate_json_response_(nameValuePairs);
	}

	@And("^I have the required path parameter$")
	public void i_have_the_required_path_parameter(DataTable dt) throws Throwable {
		List<List<String>> datTable = dt.asLists(String.class);
		RestUtility.constructGetPathParam(datTable);
	}


}
