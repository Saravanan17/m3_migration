package com.acats.appname.locators;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.acats.core.DriverManager;
import com.acats.core.FileReaderManager;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomeLocator  {
	public HomeLocator() {
		PageFactory.initElements(new AppiumFieldDecorator(DriverManager.getDriver()), this);
	}
	
	@FindBy(id = "enCAEdition")
	public WebElement Edition;
	
	@FindBy(id = "origin_focus_0")
	public WebElement originClk;

	@FindBy(id = "origin_R_0")
	public WebElement originTxt;
	
	@FindBy(id = "destination_label_0")
	public WebElement destClk;
	
	@FindBy(id = "destination_R_0")
	public WebElement destTxt;
	
	@FindBy(xpath = "//span[@class='flight-calendar-label default']")
	public WebElement calenderClk;
	
	@FindBy(xpath = "//td[@data-year='2019' and @data-month='6']//following::span[text()='25']")
	public WebElement sel_OrginDate;
	
	@FindBy(xpath = "//td[@data-year='2019' and @data-month='6']//following::span[text()='28']")
	public WebElement SelDestinationDate;
	
	@FindBy(id = "calendarSelectActionBtn")
	public WebElement clkOk;
	
	@FindBy(xpath = "//*[@id=\"magnet-fields-wrapper\"]/div[3]/div[3]/input")
	public WebElement ClkFind;
	
	@FindBy(id="pageLogo")
	public WebElement logo;
	
	
	@FindBy(id="header-searchbox-input")
	public WebElement headerInputSearchBox;

	public WebElement getHeaderInputSearchBox() {
		return headerInputSearchBox;
	}

	@FindBy(xpath="//*[@id=\"book-1\"]/div[2]/div[1]/div[1]/p[2]/span[2]/a")
	public WebElement isbnBySearchResults;
	
	@FindBy(xpath = "//button[@class='search-active-magnet btn btn-primary']")
    public WebElement findButton;

	public WebElement getIsbnBySearchResults() {
		return isbnBySearchResults;
	}

}
