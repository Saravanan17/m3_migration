package com.acats.appname.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.acats.core.DriverManager;

public class SearchResults {

	public SearchResults() {
		PageFactory.initElements(DriverManager.getDriver()	, this);
	}
	
	@FindBy(className ="page-title-container")
	public WebElement searchResultheader;
	
	@FindBy(id="boundTitleInnerContainer")
	public WebElement Titlecontainer;
	
}
