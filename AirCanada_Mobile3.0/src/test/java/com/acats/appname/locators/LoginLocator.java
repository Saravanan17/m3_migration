package com.acats.appname.locators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginLocator {


	@FindBy(id = "enCAEdition']")
	public WebElement canadianEditionLink;

	@FindBy(id = "enCAEdition")
	public WebElement Edition;

	@FindBy(id = "origin_focus_0")
	public WebElement originClk;

	@FindBy(id = "origin_R_0")
	public WebElement originTxt;

	@FindBy(id = "destination_label_0")
	public WebElement destClk;

	@FindBy(id = "destination_R_0")
	public WebElement destTxt;

	@FindBy(xpath = "//span[@class='flight-calendar-label default']")
	public WebElement calenderClk;

	@FindBy(xpath = "//td[@data-year='2019' and @data-month='6']//following::span[text()='25']")
	public WebElement sel_OrginDate;

	@FindBy(xpath = "//td[@data-year='2019' and @data-month='6']//following::span[text()='28']")
	public WebElement SelDestinationDate;

	@FindBy(id = "calendarSelectActionBtn")
	public WebElement clkOk;

	@FindBy(xpath = "//div/input[@data-se-id='btnFlightsSearchOption2']")
	public WebElement ClkFind;



}
