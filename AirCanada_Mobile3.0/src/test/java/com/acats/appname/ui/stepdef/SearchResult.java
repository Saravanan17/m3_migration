package com.acats.appname.ui.stepdef;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.acats.core.DriverManager;
import com.acats.core.FileReaderManager;
import com.acats.core.Utils;

import cucumber.api.java.en.Then;

public class SearchResult {

	@Then("^I Verify Search Results page displayed$")
	public void i_Verify_Home_page_displayed() throws Exception {
		WebElement element = FileReaderManager.getInstance().getSearchResult().searchResultheader;
		boolean status=false;
		status=Utils.waitForElement(DriverManager.getDriver(), element);
		Assert.assertEquals(status, true);
	}
}
