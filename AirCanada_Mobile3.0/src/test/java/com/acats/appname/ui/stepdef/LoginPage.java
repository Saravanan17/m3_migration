package com.acats.appname.ui.stepdef;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import com.acats.core.BrowserConsoleLogs;
import com.acats.core.DriverManager;
import com.acats.core.FileReaderManager;
import com.acats.core.InitializeDriver;
import com.acats.core.Lib_WebGeneric;
import com.acats.core.ReadProperty;
import com.acats.core.Utils;
import com.acats.core.ValidateDigitalDataAC;
import com.acats.core.xray.JiraXrayTestStatusImpl;

import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginPage {

	public Scenario scenario;
	JiraXrayTestStatusImpl jiraXrayTestStatusImpl;

	WebDriver driver;

	@Given("^I open application in \"([^\"]*)\" browser$")
	public void i_open_application_in_something_browser(String browser) throws Throwable {
		InitializeDriver.startDriver(null);
		DriverManager.getDriver().get(ReadProperty.get_propValue("ACURL"));
	}

	@When("^I launch application in \"([^\"]*)\" Edition$")
	public void i_launch_Air_Canada_application_in_Canada_Edition(String editionname) throws Throwable {
		DriverManager.getDriver().manage().window().maximize();
		try {
			JavascriptExecutor js = (JavascriptExecutor) DriverManager.getDriver();
			js.executeScript("arguments[0].click()", DriverManager.getDriver().findElement(By.id("enCAEdition")));
		} catch (Exception e) {
			DriverManager.getDriver().findElement(By.id("enCAEdition")).click();
		}
	}

	@When("^User enter \"([^\"]*)\" and \"([^\"]*)\" place on home page$")
	public void user_enter_and_place_on_home_page(String origin, String destination) throws Exception {
		FileReaderManager.getAcWebsite().originClk.click();
		FileReaderManager.getAcWebsite().originTxt.sendKeys(origin);
		Utils.waitForElement(DriverManager.getDriver(), FileReaderManager.getAcWebsite().destTxt);
		FileReaderManager.getAcWebsite().destTxt.sendKeys(Keys.ENTER);
		FileReaderManager.getAcWebsite().originTxt.sendKeys(Keys.TAB);
		FileReaderManager.getAcWebsite().destClk.click();
		FileReaderManager.getAcWebsite().destTxt.sendKeys(destination);
		Utils.waitForElement(DriverManager.getDriver(), FileReaderManager.getAcWebsite().destTxt);
		FileReaderManager.getAcWebsite().destTxt.sendKeys(Keys.ENTER);
		FileReaderManager.getAcWebsite().destTxt.sendKeys(Keys.TAB);
	}

	@When("^User select \"([^\"]*)\" and \"([^\"]*)\" from calender$")
	public void user_select_and_from_calender(String arg1, String arg2) throws Exception {
		Utils.waitForElement(DriverManager.getDriver(), FileReaderManager.getAcWebsite().clkOk);
		FileReaderManager.getInstance().getwebgeneric().enterDate(arg1, arg2);
		FileReaderManager.getAcWebsite().clkOk.click();
	}

	@Then("^I Verify HomeLocator page displayed$")
	public void i_Verify_Home_page_displayed() throws Exception {
		FileReaderManager.getAcWebsite().ClkFind.click();
		Utils.waitforPageLoad(DriverManager.getDriver());
	}

	@And("^I compare with Browser logs \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void i_compare_with_browser_logs(String bookingmagent, String currencycode, String odpair, String origin,
			String destination) throws Throwable {
		Map<String, Object> dataMap = BrowserConsoleLogs.captureBrowserLogs();
		ValidateDigitalDataAC.validateData(dataMap, currencycode, odpair, origin, destination, bookingmagent);
	}

	@And("^User clicks on \"([^\"]*)\" Button in \"([^\"]*)\" page$")
	public void user_clicks_on_button(String button, String page) throws Throwable {
		Lib_WebGeneric.waitForElementVisibility(FileReaderManager.getInstance().getAcWebsite().findButton);
		if (button.equalsIgnoreCase("Find") && page.equalsIgnoreCase("Booking magnet")) {
			FileReaderManager.getInstance().getAcWebsite().findButton.click();
		}
	}
}
