package com.acats.appname.hybrid.stepdef;

import static io.restassured.RestAssured.given;

import org.openqa.selenium.Keys;

import com.acats.Runner;
import com.acats.core.DriverManager;
import com.acats.core.ExtentReportUtil;
import com.acats.core.FileReaderManager;
import com.acats.core.InitializeDriver;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.Map;

import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import cucumber.api.java.en.And;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.openqa.selenium.WebDriver;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class LoginPageHybrid {

	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;
	private WebDriver driver;
	private String isbn;
	final static Logger logger = Logger.getLogger(LoginPageHybrid.class);

	/* Initiating the Browser and Driver By passing the url */

	@Given("^a user opens \"([^\"]*)\"$")
	public void a_user_opens_isbnsearch_something(String url) throws Throwable {
		//InitializeDriver.startDriver();
		DriverManager.getDriver().get(url);
	}

	/* Search with Book name by getting the id from the getAcWebsite() */  

	@When("^searches for the \"([^\"]*)\"$")
	public void searches_for_the_book(String bookName) {
		FileReaderManager.getAcWebsite().headerInputSearchBox.clear();
		FileReaderManager.getAcWebsite().headerInputSearchBox.sendKeys(bookName);
		FileReaderManager.getAcWebsite().headerInputSearchBox.sendKeys(Keys.ENTER);
	}

	@Then("^the user retrieves the response$")
	public void the_user_retrieves_the_ISBN_from_the_search_result() {
		isbn= FileReaderManager.getAcWebsite().isbnBySearchResults.getText();
		isbn = isbn.trim();
		logger.info("Retrieving Book number from UI to be passed to API " + isbn);
	}
	@When("^a user submit the request \"([^\"]*)\"$")
	public void a_user_retrieves_the_book_by_isbn_For_Rest_Webservice(String getBookByIsbnUrl) throws Throwable {
	//	Runner.scenarioDef.debug(MarkupHelper.createLabel(ExtentReportUtil.getBrowser() + " " +ExtentReportUtil.getVersion(), ExtentColor.TRANSPARENT));
		DriverManager.closeDriver(true);
		if (request == null) {
			request = given().param("q", "isbn:" + isbn.trim());
		}
		response = request.when().get(getBookByIsbnUrl);
		logger.info("Response :" + response.prettyPrint());
		System.out.println("response: " + response.prettyPrint());
	}

	@Then("^the status code is \"([^\"]*)\"$")
	public void verify_status_code(int statusCode) {
		json = response.then().statusCode(statusCode);

	}


	@And("response includes the following in any order")
	public void response_contains_in_any_order(Map<String, String> responseFields) {
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if (StringUtils.isNumeric(field.getValue())) {
				json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
			} else {
				json.body(field.getKey(), containsInAnyOrder(field.getValue()));
			}
		}
	}

}
