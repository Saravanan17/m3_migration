package com.acats;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.acats.core.BaseUtil;
import com.acats.core.Contentkey;
import com.acats.core.DriverManager;
import com.acats.core.ExtentReportUtil;
import com.acats.core.NGTestListener;
import com.acats.core.ReadProperty;
import com.acats.core.ScreenshotUtility;
import com.acats.core.xray.JiraConstants;
import com.acats.core.xray.JiraXrayBaseClass;
import com.acats.core.xray.JiraXrayTestStatusImpl;
import com.acats.core.xray.JiraXrayUpdateBitbucket;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.hw.ExtDataProvider;
import com.hw.FeatureProcessor;
import cucumber.api.CucumberOptions;
import cucumber.api.PickleStepTestStep;
import cucumber.api.Scenario;
import cucumber.api.TestCase;
import cucumber.api.TestStep;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.PickleEventWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import io.appium.java_client.AppiumDriver;

@CucumberOptions(features = { "src/test/resources/compiled" }, plugin = { "json:target/cucumber.json" })
public class Runner implements JiraConstants {

	private static TestNGCucumberRunner runner;
	public static HashMap<String, Integer[]> runIdCounter;
	public static DesiredCapabilities capabilities;
	public String username;
	public String accessKey;
	public static WebDriver driver;
	public static AppiumDriver<?> mobileDriver;
	public static String s_id;
	public static ExtentTest features;
	public static String scenarioName;
	int count = 0;
	public static String featureFilePath;
	int featureCount = 0;
	public static Scenario scenario;
	final static Logger logger = Logger.getLogger(Runner.class);
	public static ExtentReportUtil extentReportUtil;
	public static String featureName = "";

	String remoteurl;
	String node;
	static Map<String, Integer> stepIndexMap = new HashMap<String, Integer>();

	static {
		runIdCounter = new HashMap<>();

		ReadProperty.initilizeLog();
		//connectToJira();
	}

	public Runner() {
		runner = new TestNGCucumberRunner(this.getClass());
	}

	@DataProvider(parallel = true)
	public Object[][] data() {

		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {
		}
		Object scenarios[][] = runner.provideScenarios();
		return scenarios;
	}

	@Test(dataProvider = "data")
	public void g(PickleEventWrapper pickleWrapper, CucumberFeatureWrapper featureWrapper) throws Throwable {
		runner.runScenario(pickleWrapper.getPickleEvent());
	}

	/**
	 * To Establish JIRA Connectivity
	 */
	private static void connectToJira() {
		try {
			long millis = System.currentTimeMillis();
			System.setProperty("log4jFileName", millis + "-" + Math.round(Math.random() * 1000));
			logger.info("***********Application Started......... *************");
			logger.info("***********Connect to JIRA *************");
			JiraXrayBaseClass jiraXrayApiMainClass = new JiraXrayBaseClass();
			jiraXrayApiMainClass.connectToJira();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Executed once before the Application Starts
	 * 
	 * @throws Throwable
	 */
	@BeforeClass
	public void setup() throws Throwable {
		Contentkey.getCopyKeybyLanguage();
		extentReportUtil = new ExtentReportUtil();
		extentReportUtil.ExtentReport();
		ScreenshotUtility.deleteDirectory(screenshotPath, "screenshot");
		ScreenshotUtility.deleteDirectory(extentReportPath, "Report");
		if (!ReadProperty.get_propValue("CaptureFailedScreenshot").equalsIgnoreCase("none")) {
			featureFilePath = ScreenshotUtility.screenshotPathByFeature();
		}
		String tagname = System.getProperty("Featurename");
		String srcDir[] = new String[] { "features/Mobile" };
		List<String> tags;
		if (tagname == null) {
			tags = Arrays.asList(new String[] { "@P1" });
		} else {
			tags = Arrays.asList(new String[] { tagname });
		}
		// Data sources do not have any data.
		ExtDataProvider provider = new com.acats.core.DataProvider();
		Path outputPath = Paths.get(System.getProperty("user.dir"), "src", "test", "resources", "compiled");
		File outputDir = new File(outputPath.toUri());
		if (!outputDir.exists()) {
			outputDir.mkdir();
		}
		FileUtils.cleanDirectory(outputDir);
		for (String srcLoc : srcDir) {
			Path path = Paths.get(System.getProperty("user.dir"), "src", "test", "resources", srcLoc);
			System.out.println(provider);
			processEntity(new File(path.toString()), outputDir, provider, tags);
		}
	}

	@BeforeMethod(alwaysRun = true)
	@Parameters(value = { "config" })
	public void setUp(String config_file) throws Throwable {
		try {
			BaseUtil.setCurrentConfigFile(config_file);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * To Run Feature files
	 * 
	 * @param entity
	 * @param outputDir
	 * @param provider
	 * @param tags
	 * @throws IOException
	 */
	void processEntity(File entity, File outputDir, ExtDataProvider provider, List<String> tags) throws IOException {
		if (entity.isDirectory())
			for (File tFile : entity.listFiles())
				processEntity(tFile, outputDir, provider, tags);
		else {
			String parts[] = entity.getName().split("\\.");
			if (parts[1].equalsIgnoreCase("feature")) {
				File destFeature = File.createTempFile(parts[0] + "-", ".feature", outputDir);
				FeatureProcessor.process(entity, destFeature, provider, tags);
			}
		}
	}

	@BeforeTest
	public void beforeTest(ITestContext result) {
		String featureTestName = result.getName();
		System.out.println(featureTestName);
	}

	/**
	 * Executed after Each Scenario
	 * 
	 * @param scenario
	 * @throws Throwable
	 */
	String destPath = null;

	@After
	public void tearDown(Scenario scenario) throws Throwable {
		scenarioName = scenario.getName();
	}

	/**
	 * Executed After each Scenario
	 * 
	 * @param result
	 * @throws Throwable
	 */

	@AfterMethod
	public synchronized void getResult(ITestResult result) throws Throwable {

		ExtentTest scenarioDef = NGTestListener.scenarioTestMap.get((int) Thread.currentThread().getId());
		String destPath = null;
		try {
			logger.debug("Cucumber After Method Executed..................");
			Object[] featureTestName = result.getParameters();
			String scenarioName = featureTestName[0].toString();
			String featureName = featureTestName[1].toString();
			boolean isDriverOpened = BaseUtil.isDriverOpened();
			if (result.getStatus() == ITestResult.FAILURE) {
				if (isDriverOpened
						&& ReadProperty.get_propValue("CaptureFailedScreenshot").equalsIgnoreCase("failureOnly")) {
					ScreenshotUtility.featureScreenshotFolder(featureName, featureFilePath);
					destPath = ScreenshotUtility.captureFailedScreenshot(scenarioName);
					ScreenshotUtility.handleExtentReport("failureOnly", scenarioDef,
							NGTestListener.failedStepMap.get((int) Thread.currentThread().getId()));
				} else {
					destPath = NGTestListener.failedStepPath.get((int) Thread.currentThread().getId());
				}
				logger.info("Test Scenario failed ---  FEATURE NAME----" + featureName + "SCENARIO NAME----"
						+ scenarioName);
				logger.error(result.getThrowable().getLocalizedMessage());
				scenarioDef.fail(result.getThrowable().toString());
				if (isDriverOpened) {
					scenarioDef.debug(MarkupHelper.createLabel("Browser Name  :" + ExtentReportUtil.getBrowser() + "--"
							+ "Version :" + ExtentReportUtil.getVersion(), ExtentColor.GREEN));
				}
			} else if (result.getStatus() == ITestResult.SUCCESS) {
				logger.info(
						"Test Scenario passed ---  FEATURE NAME---" + featureName + "SCENARIO NAME----" + scenarioName);
				if (isDriverOpened) {
					scenarioDef.debug(MarkupHelper.createLabel("Browser Name  :" + ExtentReportUtil.getBrowser() + "--"
							+ "Version :" + ExtentReportUtil.getVersion(), ExtentColor.GREEN));
				}
			}
			NGTestListener.browsermap.remove((int) Thread.currentThread().getId());

			DriverManager.closeDriver(isDriverOpened);
			BaseUtil.setIsDriverOpened(false);
			updateJiraExecutionStatus(result, destPath);
			BaseUtil.updateDB(result);

			logger.info("-------Jira status updated------");
		} catch (Throwable e) {
			System.out.println(e.getLocalizedMessage());
			logger.error(e.getLocalizedMessage());
		}

		updateJiraExecutionStatus(result, destPath);
		BaseUtil.updateDB(result);
	}

	/**
	 * Executed after all Feature files
	 * 
	 * @param ctxt
	 * @throws Exception
	 */
	@AfterClass
	public void afterClass(ITestContext ctxt) throws Exception {
		try {
			String xrayExportDelPath = ReadProperty.get_propValue("exportXrayPath").trim();
			logger.debug("Cucumber After class Executed..................");
			URI uri = new URI("https://username:access_key@www.browserstack.com/automate/sessions/" + s_id + ".json");
			HttpPut putRequest = new HttpPut(uri);

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add((new BasicNameValuePair("status", "Failed")));
			nameValuePairs.add((new BasicNameValuePair("reason", "Trying REST API")));
			putRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpClientBuilder.create().build().execute(putRequest);
			logger.info("***********Application Ends*************");
			extentReportUtil.extent.flush();
			JiraXrayUpdateBitbucket.updateXray();
			File dir = new File(xrayExportDelPath);
			ScreenshotUtility.delFilesinDir(dir);
			// BaseUtil.updateMongoDb();
		} catch (Throwable e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * To Update Status in JIRA
	 * 
	 * @param result
	 * @param string
	 * @throws IOException
	 * @throws Throwable
	 */
	private static void updateJiraExecutionStatus(ITestResult result, String failScreenshotPath)
			throws IOException, Throwable {
		try {
			logger.debug("*************************Updating Jira Status*******************");
			JiraXrayTestStatusImpl jiraXrayTestStatusImpl = new JiraXrayTestStatusImpl();
			jiraXrayTestStatusImpl.updateJiraStatus(result, failScreenshotPath);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@AfterStep
	public synchronized void testAfter(Scenario scenario) throws Throwable {
		int currentStepIndex = 0;
		String key = scenario.getId() + (int) Thread.currentThread().getId();
		ExtentTest scenarioDef = NGTestListener.scenarioTestMap.get((int) Thread.currentThread().getId());
		Field testCaseField = scenario.getClass().getDeclaredField("testCase");
		testCaseField.setAccessible(true);
		TestCase tc = (TestCase) testCaseField.get(scenario);
		Field testSteps = tc.getClass().getDeclaredField("testSteps");
		testSteps.setAccessible(true);
		List<TestStep> teststeps = tc.getTestSteps();
		List<TestStep> steps = new ArrayList<>();
		String path = null;
		for (TestStep testStep : teststeps) {
			if (testStep instanceof PickleStepTestStep) {
				steps.add(testStep);
			}
		}
		try {
			if (stepIndexMap.get(key) != null) {
				currentStepIndex = stepIndexMap.get(key);
			}
			PickleStepTestStep pts = (PickleStepTestStep) steps.get(currentStepIndex);
			String stepName = pts.getStepText();
			if (ReadProperty.get_propValue("CaptureFailedScreenshot").equalsIgnoreCase("allSteps")) {
				ScreenshotUtility.scenarioScreenshotFolder(scenario.getName());
				path = ScreenshotUtility.screenshotForWeb(stepName);
				ScreenshotUtility.handleExtentReport("allSteps", scenarioDef, stepName);
			} else {
				ScreenshotUtility.handleExtentReport("none", scenarioDef, stepName);
			}
			currentStepIndex = currentStepIndex + 1;
			stepIndexMap.put(key, currentStepIndex);
			NGTestListener.failedStepMap.put((int) Thread.currentThread().getId(), stepName);
			NGTestListener.failedStepPath.put((int) Thread.currentThread().getId(), path);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
