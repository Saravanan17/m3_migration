package com.acats.mobile.locators;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.acats.core.DriverManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class LoginMobilePage extends LoadableComponent<LoginMobilePage> {

	private WebDriver driver;
	public LoginMobilePage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
	}
	@Override
	protected void load() {
	}

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(acAeroNumEmail.isDisplayed());
	}

	@AndroidFindBy(id = "account_login_close_button")
	@iOSXCUITFindBy(id = "login IconClose")
	public MobileElement aeroLoginCloseBtn;

	@AndroidFindBy(id = "account_login_aeroplan_logo")
	@iOSXCUITFindBy(id = "graphic-aeroplanLogo")
	public MobileElement acAeroLogo;

	@AndroidFindBy(id = "account_login_title_header_textview")
	@iOSXCUITFindBy(id = "accountLogin_login_header")
	public MobileElement acAeroTitleHeader;

	@AndroidFindBy(id = "account_login_aeroplan_email_edittext")
	@iOSXCUITFindBy(id = "accountLogin_login_aeroplanEmail_labelSelected")
	public MobileElement acAeroNumEmail;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeTextField")
	public MobileElement acAeroNumEdit;

	@AndroidFindBy(id = "account_login_password_edittext")
	@iOSXCUITFindBy(xpath = "//*[@name='accountLogin_login_password_labelSelected']")
	public MobileElement acAreoPassword;

	@iOSXCUITFindBy(xpath = "//XCUIElemetTypeSecureTextField")
	public MobileElement acAeroPassEdit;

	@AndroidFindBy(id = "text_input_end_icon")
	// @iOSXCUITFindBy(accessibility="accountLogin_login_password_passwordUnmasking_accessibility_label")
	@iOSXCUITFindBy(xpath = "//*[@name='accountLogin_login_password_passwordUnmasking_accessibility_label']")
	// @iOSXCUITFindBy(xpath = "//XCUIElementTypeApplication[@name=\"Air Canada
	// Dev\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeImage/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeSecureTextField/XCUIElementTypeOther")
	public MobileElement acAreoToggle;

	@AndroidFindBy(id = "account_login_loginbutton")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Log in'] | //XCUIElementTypeButton[@name='Connecter'] ")
	public MobileElement acAeroLoginButton;

	public void enterUserNamePassword(String username, String password) {
		acAeroNumEmail.setValue(username);
		acAreoPassword.setValue(password);
		clickLogin();
	}

	public MyAccountMobile enterUserNamePassword_myaccount(String username, String password) {
		acAeroNumEmail.sendKeys(username);
		acAreoPassword.sendKeys(password);
		clickLogin();
		return new MyAccountMobile();
	}

	/*
	 * public Saved_Passengers enterUserNamePassword_RTI(String username, String
	 * password) throws Throwable { acAeroNumEmail.sendKeys(username);
	 * acAeroPassEdit.sendKeys(password); clickLogin(); return new
	 * Saved_Passengers(); }
	 */

	public void clickLogin() {

		acAeroLoginButton.click();
	}

}