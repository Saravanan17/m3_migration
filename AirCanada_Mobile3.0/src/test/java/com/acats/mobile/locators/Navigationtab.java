package com.acats.mobile.locators;

import org.openqa.selenium.support.PageFactory;

import com.acats.core.DriverManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Navigationtab {
	AppiumDriver<MobileElement> driver;

	public Navigationtab(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
	}

	@AndroidFindBy(id = "action_account")
	public MobileElement AccountMenu;

	@AndroidFindBy(id = "action_bookings")
	public MobileElement BookMenu;

	public LoginMobilePage clickonAccounttab() {
		AccountMenu.click();
		return new LoginMobilePage(DriverManager.getMDriver());
	}

	public Book_Search clickonBooktab() {
		BookMenu.click();
		return new Book_Search(DriverManager.getMDriver());
	}

}
