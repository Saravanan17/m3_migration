package com.acats.mobile.locators;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.acats.core.DriverManager;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class Book_Search {

	WebDriver driver;

	public Book_Search(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
	}

	public String x = "//android.view.ViewGroup/android.widget.TextView[@text='";
	public String clos = "\']";

	@AndroidFindBy(id = "dates_continue_button")
	@iOSXCUITFindBy(xpath = "//*[@name='bookingSearch_calendar_continueButton']")
	public MobileElement continue_date;

	@AndroidFindBy(xpath = "//*[contains(@text,'All Airports')] | //*[contains(@text,'Tous les aéroports')]")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'All Airports')] | //*[contains(@name,'Tous les aéroports')]")
	public MobileElement all_airportstext;

	@AndroidFindBy(id = "action_schedules")
	@iOSXCUITFindBy(xpath = "//*[@name='Trips']")
	public MobileElement profile_Btn;

	@iOSXCUITFindBy(accessibility = "bookingSearch_bookSheet_arrivalAirport_destinationChosen_unselected")
	public MobileElement arrival_selected;

	@iOSXCUITFindBy(id = "bookingSearch_cityList_sheetTitle_to")
	public MobileElement booking_Sheet_Title_Flying_to;

	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Book'] | //XCUIElementTypeButton[@label='Réserver']")
	@AndroidFindBy(id = "action_bookings")
	public MobileElement Book_Btn;

	@AndroidFindBy(xpath = "(//android.view.ViewGroup)[7] | (//android.widget.TextView)[2]")
	@iOSXCUITFindBy(xpath = "//*[@name='bookingSearch_bookSheet_departure_choosePrompt_unselected'] | //*[@name='bookingSearch_bookSheet_departureAirport_originChosen_unselected']")
	public MobileElement chooseDepart_btn;

	@AndroidFindBy(xpath = "//*[contains(@resource-id,'origin_button_layout')] | //*[contains(@resource-id,'selected_origin_text_view')]")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'bookingSearch_bookSheet_departure')]")
	public MobileElement selected_origin_btn;

	@AndroidFindBy(id = "selected_destination_text_view")
	@iOSXCUITFindBy(id = "bookingSearch_bookSheet_departure_choosePrompt_unselected")
	public MobileElement selected_destination_btn;

	@AndroidFindBy(id = "permission_message")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'access your location')]")
	public MobileElement permission_message_popup;

	@iOSXCUITFindBy(accessibility = "bookingSearch_bookSheet_searchField_clearButton")
	public MobileElement cross_btn;

	@AndroidFindBy(id = "destination_image_view_active")
	// newly added by siva
	// @iOSXCUITFindBy(id="bookingSearch_bookSheet_arrival_choosePrompt_selected")
	@iOSXCUITFindBy(xpath = "//*[@name='bookingSearch_bookSheet_arrival_choosePrompt_selected'] | //*[@name='bookingSearch_bookSheet_arrivalAirport_destinationChosen_unselected']")
	public MobileElement arrival_btn;

	@AndroidFindBy(xpath = "<NULL>")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[@name='Allow'] | //XCUIElementTypeButton[@name='Autoriser']")
	public MobileElement alertAllow;

	@AndroidFindBy(id = "od_search_airports_edittext")
	@iOSXCUITFindBy(xpath = "//*[contains(@name,'cityList_searchFieldHint')]")
	public MobileElement searchAirports_Edittext;

	@AndroidFindAll({ @AndroidBy(xpath = "//android.view.ViewGroup/android.widget.TextView") })
	public List<MobileElement> list_airports;

	String dateSelection_locator = "//android.widget.TextView[contains(@content-desc,'%s')]";

	@AndroidFindBy(id = "booking_summary_flight_search_button")
	@iOSXCUITFindBy(xpath = "//XCUIElementTypeButton[contains(@name,'bookingSearch_bookSheet_searchButton')]")
	public MobileElement tripButtonName;
	

	@AndroidFindBy(id = "departure_time_textView")
//	@iOSXCUITFindBy(xpath = "(//*[@name='FlightBlockCell'][1])//following-sibling::XCUIElementTypeStaticText[2]")
	@iOSXCUITFindBy(accessibility = "FlightBlockCell-departureTime")
	public MobileElement departure_time_textView;

//#############################################################################################

	public void clickonBooktab() {
		Book_Btn.click();
		if (alertAllow.isDisplayed()) {
			alertAllow.click();
		}
	}

	public void SearchFlight(String depart, String arrival) {
		arrival_selected.sendKeys(arrival);

	}

	public void verifyBSlandingscreen() {
		Assert.assertTrue(searchAirports_Edittext.isDisplayed());
	}

	public void clickOnChooseDepart() {
		chooseDepart_btn.click();
	}

	public void verifybookinglandingscreen() {
		Assert.assertTrue(chooseDepart_btn.isDisplayed());
	}

	public void selectAirport_from_list(String airportsName) {
		for (MobileElement airport : list_airports) {
			if (airport.getText().equalsIgnoreCase(airportsName)) {
				airport.click();
				break;
			}
		}
	}

	public void searchingflights_origin_arrival(String origin, String depart) {
		WebDriverWait wait = new WebDriverWait(DriverManager.getMDriver(), 30);
		wait.until(ExpectedConditions.visibilityOf(searchAirports_Edittext));
		searchAirports_Edittext.setValue(origin);
		selectAirport_from_list(origin);
		searchAirports_Edittext.setValue(depart);
		selectAirport_from_list(depart);
	}

	public void dateselection(int value) throws NumberFormatException, Exception, Throwable {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); // Now use today date.
		c.add(Calendar.DATE, value);
		String output = sdf.format(c.getTime());
		String[] date = output.split("/");
		String dateText = getNormalizedDateString(date);
		String dateformat = String.format(dateSelection_locator, dateText);
		MobileElement date_ele = DriverManager.getMDriver().findElement(By.xpath(dateformat));
		date_ele.click();
		WebDriverWait wait = new WebDriverWait(DriverManager.getMDriver(), 30);
		wait.until(ExpectedConditions.visibilityOf(continue_date));
		continue_date.click();
		MobileElement element = (MobileElement) wait.until(ExpectedConditions.visibilityOf(tripButtonName));
		element.click();
	}

	private String getNormalizedDateString(String date[]) throws NumberFormatException, Exception, Throwable {
		String Frenchmonth = "";
		String dateText = "";
		
		if (System.getProperty("Language").equalsIgnoreCase("English")) {
			dateText = date[1].toString() + " " + date[0] + ", " + date[2];
			return dateText;
		} else {
			if (date[1].equalsIgnoreCase("January")) {
				Frenchmonth = "janvier";
			} else if (date[1].equalsIgnoreCase("February"))
				Frenchmonth = "février";
			else if (date[1].equalsIgnoreCase("March"))
				Frenchmonth = "mars";
			else if (date[1].equalsIgnoreCase("April"))
				Frenchmonth = "avril";
			else if (date[1].equalsIgnoreCase("May"))
				Frenchmonth = "mai";
			else if (date[1].equalsIgnoreCase("June"))
				Frenchmonth = "juin";
			else if (date[1].equalsIgnoreCase("July"))
				Frenchmonth = "juillet";

			else if (date[1].equalsIgnoreCase("August"))
				Frenchmonth = "août";
			else if (date[1].equalsIgnoreCase("September"))
				Frenchmonth = "septembre";
			else if (date[1].equalsIgnoreCase("October"))
				Frenchmonth = "octobre";
			else if (date[1].equalsIgnoreCase("November"))
				Frenchmonth = "novembre";
			else if (date[1].equalsIgnoreCase("December"))
				Frenchmonth = "décembre";
			dateText = Integer.parseInt(date[0]) + " " + Frenchmonth + ", " + date[2];
			return dateText;
		}
	}
	
	public void verifysearchResultspage() {
		WebDriverWait wait = new WebDriverWait(DriverManager.getMDriver(), 50);
		wait.until(ExpectedConditions.visibilityOf(departure_time_textView));
		Assert.assertEquals(departure_time_textView.isDisplayed(), true);
	}
}
