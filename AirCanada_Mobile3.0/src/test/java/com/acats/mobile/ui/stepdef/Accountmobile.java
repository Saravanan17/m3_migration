package com.acats.mobile.ui.stepdef;

import com.acats.core.DriverManager;
import com.acats.mobile.locators.LoginMobilePage;

import cucumber.api.java.en.When;

public class Accountmobile {
	@When("I login with Valid crediantial \"([^\"]*)\",\"([^\"]*)\"")
	public void loginwithvalidcrediantials(String username,String password) {
		LoginMobilePage login=new LoginMobilePage(DriverManager.getMDriver());
		login.enterUserNamePassword(username, password);
	}
}
