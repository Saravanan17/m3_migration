package com.acats.mobile.ui.stepdef;

import java.util.concurrent.TimeUnit;

import com.acats.core.DriverManager;
import com.acats.core.InitializeDriver;
import com.acats.mobile.locators.Navigationtab;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class NavigationMobile {
	Navigationtab nav;


	@Given("I launch mobile 3.0 application")
	public void launchapp() throws Throwable {
		InitializeDriver.startDriver(null);
		DriverManager.getMDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		DriverManager.getMDriver().getSessionId();
	}

	@When("I tap on Account tab")
	public void navigatetoAccounttab() {
		nav=new Navigationtab(DriverManager.getMDriver());
		nav.clickonAccounttab();
	}

	
}
