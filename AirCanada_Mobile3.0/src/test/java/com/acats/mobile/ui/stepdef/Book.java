package com.acats.mobile.ui.stepdef;

import com.acats.core.DriverManager;
import com.acats.mobile.locators.Book_Search;
import com.acats.mobile.locators.Navigationtab;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Book {
	Book_Search book_search;

	@Then("I am on BS landing screen")
	public void iamonBooksearch_landingscreen() {
	}

//	@When("I tap on Book tab")
//	public void navigatetoBooktab() {
//		Navigationtab nav = new Navigationtab(DriverManager.getMDriver());
//		book_search = nav.clickonBooktab();
//	}
	
	@And("I tap on Choose depart on BS-landing screen")
	public void choosedepartbutton() {
		book_search.clickOnChooseDepart();
	}
	
	@And("I verify that user gets navigated to BS-City or Airport Selection screen")
	public void verifysearchpage() {
		book_search.verifyBSlandingscreen();
	}
	
	@And("I select \"([^\"]*)\" and \"([^\"]*)\" on BS - City or Airport Selection screen")
	public void enter_depart_andArrival_airports(String depart,String arrival) {
		book_search.searchingflights_origin_arrival(depart,arrival);
	}
	
	@Then("I select \"([^\"]*)\" from calender")
	public void select_travel_date(int date) throws NumberFormatException, Exception, Throwable
	{
		book_search.dateselection(10);
	}
	
	@Then("I should see Flight search results")
	public void verifyflightsearchresults() {
		book_search.verifysearchResultspage();
	}
}
