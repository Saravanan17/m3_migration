Feature: Test Set to run Web Application and API 

	@TEST_KQA-541 @TESTSET_KQA-540 @Regression @UI @data=TC004
	Scenario Outline: To Run Web Application Test
		    Given I open application in "<Browser>" browser
		    When I launch application in "<Edition_Name>" Edition
		    And User enter "<Origin>" and "<Destination>" place on home page
		    And User select "<OriginDate>" and "<DestinationDate>" from calender
		    And User clicks on "Find" Button in "Booking magnet" page
		    Then I Verify Search Results page displayed
		
		    Examples: 
		      | Browser | Origin | Destination | OriginDate | DestinationDate | Edition_Name |
	@TEST_KQA-545 @TESTSET_KQA-540 @P1 @Regression @api @data=TC002
	Scenario Outline: To run API Application Test
		Given I want to execute "<Service_Type>" service "<Service_Name>"
		And I have the required path parameter
			| key | value |
		When I submit "<Service_Type>" request "<url>"
		Then I Verify response status code is "<status_code>"
		And I Validate json response
			| data.first_name | Janet |
		
			Examples:
				| Service_Name | status_code | Service_Type | url | status_code |
	@TEST_KQA-937 @TESTSET_KQA-540 @Hybrid @Regression @data=TC003
	Scenario Outline: To Run Hybrid Application Test
		Given a user opens "<url>"
		When searches for the "<Book Name>"
		Then the user retrieves the response
		When a user submit the request "<API_URL>"
		Then the status code is "<status code>"
		And response includes the following in any order
			| items.volumeInfo.title     | Steve Jobs         |
			| items.volumeInfo.publisher | Simon and Schuster |
		
			Examples:
				| Book Name | status code | url | API_URL |
	@TEST_KQA-947 @TESTSET_KQA-540 @SIT @data=TC005
	Scenario Outline: To run Web Application Failure Test
		Given I open application in "<Browser>" browser
		When I launch application in "<Edition_Name>" Edition
		And User enter "<Origin>" and "<Destination>" place on home page
		And User select "<OriginDate>" and "<DestinationDate>" from calender
		Examples:
		|Browser| Origin | Destination | OriginDate | DestinationDate | Edition_Name |
		
