@e2efs @pod2e2e
Feature: End to End POD2 scenario validation

Background:
Given I launch mobile application
When user taps on the flight status tab
Then I verify a sheet is displayed with header "flightStatus_home_header"

######################### flight number ##########################

@data=M3-E2E_01 @e2efs111
Scenario Outline: M3-E2E_01_flightnumber Reset button and Cancel button fields verification in FS Landing screen
And I verify that buttons "flightStatus_sheet_searchRouteButton" "flightStatus_sheet_searchFlightButton" are displayed
When I Tap on Flight Number in the FS_Landing screen
Then I verify a sheet is displayed with header "flightStatus_flightSearch_header" for FlightNumber
And I verify that an input field with a hint is displayed "flightStatus_flightSearch_searchFieldHint" for entering flight number
And I verify the Marketing code with logo "flightStatus_flightSearch_marketingCode" and Search_icon is displayed
And I verify aircanada logo is displayed
When I enter the "<flightNumber>" on flightNumber search field
Then I verify the flightStatus flightSearch clearButton button is displayed
And I verify that flightStatus flightSearch searchFlightButton and reset button are displayed for flightnumber
When I tap on reset button in flightnumber screen 
Then I verify a sheet is displayed with header "flightStatus_home_header"
When I Tap on Flight Number in the FS_Landing screen
When I enter the "<flightNumber>" on flightNumber search field
And I tap on cancel icon in flightnumber
When I enter the "<flightNumber>" on flightNumber search field
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton" for flightnumber 
#Then I verify flight status details header "flightStatus_flightDetails_header" is displayed

Examples:
|flightNumber|DatePicker|


@data=M3-E2E_02 @e2efs
Scenario Outline: M3-E2E_02_FS Details screen validation for Flightnumber
And I verify that buttons "flightStatus_sheet_searchRouteButton" "flightStatus_sheet_searchFlightButton" are displayed
When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"
And I enter the "<flightNumber>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by flight number 
Then I verify flight status details header "flightStatus_flightDetails_header" is displayed
########### Below gherkin line covers Flight status various status based on the Schedule time #########
And I validate flight status for the selected flight in FS details screen
############ Additional Information are FlightNumber, Date, Status, Origin City, Destination City, Origin Time, Destination time, Distance, Kilometer, Operating Carrier and Operatedby AC###########
Then I validate additional information for the selected flight in FS details screen

Examples:
|flightNumber|DatePicker|

@data=M3-E2E_03 @e2efs
Scenario Outline: M3-E2E_03_Validation of "no flight" found by flight number and route
When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"
And I enter the "<flightNumber>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by flight number
Then I verify no flights found error popup is displayed
And I verify error message title is displayed
And I verify error message is displayed
And I verify cancel button and search by city buttons is present on error popup
When I tap on cancel button
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by flight number  
Then I verify no flights found error popup is displayed
When I tap search by city button
And I tap on the alerts once launch the application
Then I verify a sheet is displayed with header "flightStatus_routeSearch_header" for route
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then I verify no flights found error popup is displayed
And I verify error message title is displayed
And I verify error message is displayed
And I verify ok button is present on error popup
When I tap on Ok button
Then I verify a sheet is displayed with header "flightStatus_home_header"

Examples:
|flightNumber|DatePicker|origin|destination|



#############################  Flight route ########################################


@data=M3-E2E_04 @e2efs 
Scenario Outline: M3-E2E_04_ FS Flightroute swap button, reset button and fields verification in FS Landing screen
And I verify that buttons "flightStatus_sheet_searchRouteButton" "flightStatus_sheet_searchFlightButton" are displayed 
When I Tap on "flightStatus_sheet_searchRouteButton"
And I tap on the alerts once launch the application
Then I verify a sheet is displayed with header "flightStatus_routeSearch_header" for route
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker 
Then I verify two button "flightStatus_flightSearch_searchFlightButton" "flightStatus_flightSearch_resetButton" is displayed
And I verify swap button is displayed in between origin and destination
When I tap on swap button 
Then I verify origin and destination are getting interchanged
When I tap on reset button "flightStatus_flightSearch_resetButton" by route
Then I verify a sheet is displayed with header "flightStatus_home_header"
When I Tap on "flightStatus_sheet_searchRouteButton"
And I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then I verify that flight search results screen is displayed with all the label informations for route  
And I verify flight status search results by route "<origin>" and "<destination>" are displayed
When I tap on "<flightNumber>" in FS results screen
Then I verify flight status details header "flightStatus_flightDetails_header" is displayed 


Examples:
|origin|destination|DatePicker|flightNumber|


@data=M3-E2E_05 @e2efs
Scenario Outline: M3-E2E_05_FS Details screen validation for FlightRoute
When I Tap on "flightStatus_sheet_searchRouteButton"  
And I tap on the alerts once launch the application
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen

And I verify that flight search results screen is displayed with all the label information for route
And I verify flight status search results by route "<origin>" and "<destination>" are displayed
And I Verify non_stop label "flightStatus_resultsList_group_nonStop" is displayed

And I verify that flightstatus "<flightStatus>" is available in FS results screen
When I tap on "<flightNumber>" in FS results screen 
Then I verify flight status details header "flightStatus_flightDetails_header" is displayed  
########### Below gherkin line covers various flight status based on the Schedule time #########
And I validate flight status for the selected flight in FS details screen
############ Additional Information are FlightNumber, Date, Status, Origin City, Destination City, Origin Time, Destination time, Distance, Kilometer, Operating Carrier and Operatedby AC###########
Then I validate additional information for the selected flight in FS details screen

Examples:
|origin|destination|DatePicker|flightStatus|


@data=M3-E2E_06 @e2efs
Scenario Outline:M3-E2E_06_FS Flightroute validation for connection flights
When I Tap on "flightStatus_sheet_searchRouteButton"
And I tap on the alerts once launch the application
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen

And I verify that flight search results screen is displayed with all the label informations for route  
And I verify flight status search results by route "<origin>" and "<destination>" are displayed
And I Verify Connecting label "flightStatus_resultsList_group_connections" is displayed
When I swipe left on the flight status details page
And I verify layover details are displayed in the search results

When I tap on "<flightNumber>" in FS results screen 
Then I verify flight status details header "flightStatus_flightDetails_header" is displayed  
And I verify that flightstatus "<flightStatus>" is available in FS results screen
#And  I verify primary status for the selected filght in FS details screen 
And I validate the core flight status details for the selected flight
And I validate the additional information for the selected flight

When I swipe left on the flight status details page
And I validate the layover details in FS details Screen
When I swipe left on the flight status details page
And  I verify primary status for the selected filght in FS details screen   only primary status or full status
And I validate the core flight status details for the selected flight
And I validate the additional information for the selected flight

When I swipe right on the flight status details page
And I verify layover details "flightStatus_layoverDetails_header" header is displayed
When I swipe right on the flight status details page

And I tap on back button on FS details screen
Then user gets navigated to FS search results by route screen


Examples:
|origin|destination|DatePicker|flightStatus|flightNumber|

@data=M3-E2E_07 @e2efs
Scenario Outline:M3-E2E_07_FS Flight status validation of three Recent airports in Origin and Destination by route
When I Tap on "flightStatus_sheet_searchRouteButton"
And I tap on the alerts once launch the application

When I enter the "<origin1>" and "<destination1>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on back button in flight status
Then I verify that Search By Route Sheet is displayed

And I tap on origin
When I select the "<origin2>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I tap on destination 
When I select the "<destination2>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on back button in flight status
Then I verify that Search By Route Sheet is displayed

And I tap on origin
When I select the "<origin3>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I tap on destination 
When I select the "<destination3>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on back button in flight status
Then I verify that Search By Route Sheet is displayed

And I tap on origin
When I select the "<origin4>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I tap on destination 
When I select the "<destination4>" on "flightStatus_cityList_origin_choosePrompt_selected"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on back button in flight status
Then I verify that Search By Route Sheet is displayed

### Validation of Only last three recent searches are displayed in FS Landing screen ####
Then I verify the last three searches are displayed in the FS Landing screen

Examples:
|origin1|destination1|DatePicker|origin2|destination2|origin3|destination3|origin4|destination4|


@data=M3-E2E_8 @e2efs
Scenario Outline:M3-E2E_8_FS Flight status validate that the recent searches available on the flight number
And I verify that buttons "flightStatus_sheet_searchRouteButton" "flightStatus_sheet_searchFlightButton" are displayed
When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"

When I enter the "<flightNumber1>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by flight number 
Then I verify the flight status details page is displayed
When I tap on back button in flight status
When I tap on reset button "flightStatus_flightSearch_resetButton" for flight number 
And I verify the recent search flightNumberone "<flightNumber1>" is displayed on the flight status landing screen

When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"
When I enter the "<flightNumber2>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by number
Then I verify the flight status details page is displayed
When I tap on back button in flight status
When I tap on reset button "flightStatus_flightSearch_resetButton" for number
And I verify the recent search flightNumbertwo "<flightNumber2>" is displayed on the flight status landing screen

When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"
When I enter the "<flightNumber3>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by number
Then I verify the flight status details page is displayed
When I tap on back button in flight status
When I tap on reset button "flightStatus_flightSearch_resetButton" for number
And I verify the recent search flightNumberthree "<flightNumber3>" is displayed on the flight status landing screen

When I Tap on Flight Number "flightStatus_sheet_searchFlightButton"
When I enter the "<flightNumber4>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by number
Then I verify the flight status details page is displayed
When I tap on back button in flight status
When I tap on reset button "flightStatus_flightSearch_resetButton" for number
And I verify the recent search flightNumberthree "<flightNumber4>" is displayed on the flight status landing screen

### Validation of Only last three recent searches are displayed in FS Landing screen ####
Then I verify the last three searches are displayed in the FS Landing screen for flightNumber


Examples:
|flightNumber1|DatePicker|flightNumber2|flightNumber3|


@data=M3-E2E_9 @e2efs
Scenario Outline:M3-E2E_9_FS Flight status validation of track inbound flight error message
When I Tap on Flight Number "flightStatus_sheet_searchFlightButton" 
And I enter the "<flightNumber>" on "flightStatus_flightSearch_searchFieldHint" field
And I select "<DatePicker>" from date picker 
And I tap on search button "flightStatus_flightSearch_searchFlightButton" by flight number  
Then I verify flight status details header "flightStatus_flightDetails_header" is displayed
When I tap on Track inbound plane button in FS details screen until error message is displayed  
Then I verify that inbound error message is displayed
And I tap on ok button in inbound error message
Then I verify that inbound flight is displayed

Examples:
|flightNumber|DatePicker|

@data=M3-E2E_10 @e2efs
Scenario Outline:M3-E2E_10_Validation of trips retrieved details in flight status landing screen
When I tap on Book tab
And I tap on Choose depart on BS-landing screen
And I verify that App displays a Geolocation permission popup
And I Tap on Allow while using the app
Then I verify that user gets navigated to "BS-CityAirport Selection screen"
When I select "<Departure Airport>" and "<Arrival Airport>" on BS - CityAirport Selection screen
And I select "<Departure Date>" "<Return Date>" and taps on Continue
And I add "<Pax adult count>" for adult "<Pax child count>" for child "<Pax youth count>" for youth and tap on Continue on BS-Passenger screen
And I tap on Search One wayRound-trip Flights button
Then I verify that user is displayed with search results
When I select departure Flight with "<Departure_Number of connections>" number of connections from the list of flight with fare "<Depart_Fare_brand>" and return Flight with "<Return_Number of connections>" number of connections from the list of flight with fare "<Return_Fare_Brand>" and tap on continue button
And I verify that user get navigated to RTI screen
And I dismiss the Login snackbar
And I enter passenger details for "<Pax adult count>" for adult "<Pax child count>" for child "<Pax youth count>" for youth in e2e
Then I verify that user get navigated to RTI screen
And I tap on the payment option on the RTI screen
And I enter the "<Card Number>","<Expiration date>","<CVV Number>","<Card holder name>","<Country Name>", "<Province>", "<Street address>", "<Apartment Number>", "<City Name>", "<Postal Code>", "<Cardholder Email>" fields on the RTI Payment card details screen
Then I verify that user get navigated to RTI screen
When I tap on Price summary block
Then I verify all the details of the price summary block is displayed
And I verify Accept and pay is enabled after user adds passenger,payment and after RTI price summary loads
And I tap on Accept and book button in booking confirmation screen
And I verify user get navigated to the trips details screen when booking is successful
And I tap on Date tap on trip overview screen
And I verify that flight block details are displayed and verifying same flight in FS Landing screen
And I tap on Flight in landing screen
And I validate the core flight status details for the selected flight
And I validate the additional information for the selected flight

Examples:
|Departure Airport|Arrival Airport|Departure Date|Return Date|Pax adult count|Pax child count|Pax youth count|Departure_Number of connections|Depart_Fare_brand|Return_Number of connections|Return_Fare_Brand|Card Number|Expiration date|CVV Number|Card holder name|Country Name|Province|Street address|Apartment Number|City Name|Postal Code|Cardholder Email|



@data=M3-E2E_11 @e2efs
Scenario Outline:M3-E2E_11_Validation of Standby and upgrade list in disabled state
Then I verify a sheet is displayed with header "flightStatus_home_header"
When I Tap on "flightStatus_sheet_searchRouteButton"
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on "<Flight_Number>" in FS results screen
Then user gets navigated to FS details screen
And I verify secondary status is displayed as Arrived at gate
And I verify standby and upgrade list block is displayed in FS details screen
And I verify standby and Upgrade list block is disabled

Examples:
|origin|destination|DatePicker|Flight_Number|


@data=M3-E2E_12 @e2efs
Scenario Outline:M3-E2E_12_Verification of passengers in Standby and upgrade list  
Then I verify a sheet is displayed with header "flightStatus_home_header"
When I Tap on "flightStatus_sheet_searchRouteButton"
When I enter the "<origin>" and "<destination>"
And I select "<DatePicker>" from date picker
And I tap on search button "flightStatus_flightSearch_searchFlightButton"
Then user gets navigated to FS search results by route screen
When I tap on "<Flight_Number>" in FS results screen
Then user gets navigated to FS details screen
And I verify standby and upgrade list block is displayed in FS details screen
When user taps on standby and upgrade list chevron
Then user gets navigated to standby upgrade list screen
And I verify "<Flight_Number>" flight number "standbyUpgradeList_standbyList_flightDate" and date is displayed
And I verify standby header "standbyUpgradeList_standbyList_header" is displayed
And I verify economy "standbyUpgradeList_standbyList_shortCabinName" premium economy and business cabins are displayed
And I verify available label "standbyUpgradeList_standbyList_cabinAvailableLabel" is displayed
And I verify economy "standbyUpgradeList_standbyList_shortCabinName" is displayed
And I verify list of passenger are displayed in economy cabin
And I verify that only first 3 letter of the last is dispalyed as lastname in the list
And I verify that only first one letter of the first name is dispalyed as frist name in the list
And I verify confrimed passengers are displayed in the top of the list
And I verify that confrimed passengers are displayed in alphabetic order in the list
And I verfiy confirmed passengers are displayed with green tickmark for confrim indication
And I verify waitlisted passenger are displayed under the confirmed passenger
And I verfiy waitlisted passenger priority is displayed as priority number
And I swipe left and verify upgrade "standbyUpgradeList_upgradeList_standbyListPeak" is available
And I verify the availability of premium economy cabin is displayed
And I verify the availability of business class cabin is displayed

Examples:
|origin|destination|DatePicker|Flight_Number|


