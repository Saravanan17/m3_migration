Feature: Migration - Pod2 EndtoEnd Scenarios

	@TEST_M3-13387 @TESTSET_M3-13386 @P1 @Pod2e2e @data=M3-E2E_01
	Scenario Outline: M3-E2E_01_flightnumber Reset button and Cancel button fields verification in FS Landing screen
		Given I launch mobile application
		When user taps on the flight status tab
		Then I verify a sheet is displayed with header "flightStatus_home_header"
		And I verify that buttons "flightStatus_sheet_searchRouteButton" "flightStatus_sheet_searchFlightButton" are displayed
		When I Tap on Flight Number in the FS_Landing screen
		Then I verify a sheet is displayed with header "flightStatus_flightSearch_header" for FlightNumber
		And I verify that an input field with a hint is displayed "flightStatus_flightSearch_searchFieldHint" for entering flight number
		And I verify the Marketing code with logo "flightStatus_flightSearch_marketingCode" and Search_icon is displayed
		And I verify aircanada logo is displayed
		When I enter the "<flightNumber>" on flightNumber search field
		Then I verify the flightStatus flightSearch clearButton button is displayed
		And I verify that flightStatus flightSearch searchFlightButton and reset button are displayed for flightnumber
		When I tap on reset button "flightStatus_flightSearch_resetButton" for flightnumber 
		Then I verify a sheet is displayed with header "flightStatus_home_header"
		When I Tap on Flight Number in the FS_Landing screen
		When I enter the "<flightNumber>" on flightNumber search field
		And I tap on cancel icon "flightStatus_flightSearch_clearButton" for flight number
		When I enter the "<flightNumber>" on flightNumber search field
		#And I select "<DatePicker>" from date picker 
		And I tap on search button "flightStatus_flightSearch_searchFlightButton" for flightnumber 
		#Then I verify flight status details header "flightStatus_flightDetails_header" is displayed
		
		Examples:
		|flightNumber|
