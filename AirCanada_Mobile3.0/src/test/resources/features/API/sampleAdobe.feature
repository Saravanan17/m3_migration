 Feature: Sample Adobe Application 

    @data=TC004 @adobeTest
	Scenario Outline: To Run Web Application Test
		Given I open application in  "<Browser>" browser
		When I launch application in "<Edition_Name>" Edition
		And User enter "<Origin>" and "<Destination>" place on home page
		And User select "<OriginDate>" and "<DestinationDate>" from calender
		And I compare with Browser logs "<BookingMagent>" "<CurrencyCode>" "<Odpair>" "<Origin>" "<Destination>"
		
			Examples:
				| Origin | Destination | OriginDate | DestinationDate | URL | Edition_Name | BookingMagent | CurrencyCode | Odpair |